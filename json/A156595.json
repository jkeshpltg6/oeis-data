{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A156595",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 156595,
			"data": "0,1,1,0,1,0,0,1,0,0,1,1,0,1,0,0,1,1,0,1,1,0,1,0,0,1,1,0,1,1,0,1,0,0,1,0,0,1,1,0,1,0,0,1,1,0,1,1,0,1,0,0,1,0,0,1,1,0,1,0,0,1,0,0,1,1,0,1,0,0,1,1,0,1,1,0,1,0,0,1,0,0,1,1,0,1,0,0,1,0,0,1,1,0,1,0,0,1,1,0,1,1,0,1,0",
			"name": "Fixed point of the morphism 0-\u003e011, 1-\u003e010.",
			"comment": [
				"This sequence draws the Sierpinski gasket, when iterating the following odd-even drawing rule: If \"1\" then draw a segment forward, if \"0\" then draw a segment forward and turn 120 degrees right if in odd position or left if in even position.",
				"From _Dimitri Hendriks_, Jun 29 2010: (Start)",
				"This sequence is the first difference of the Mephisto Waltz A064990, i.e., a(n) = A064990(n) + A064990(n+1), where '+' is addition modulo 2.",
				"This sequence can also be generated as a Toeplitz word: First consider the periodic word 0,1,$,0,1,$,0,1,$,... and then fill the gaps $ by the bitwise negation of the sequence itself: 0,1,_1_,0,1,_0_,0,1,_0_,.... See the Allouche/Bacher reference for a precise definition of Toeplitz sequences. (End)",
				"From _Joerg Arndt_, Jan 21 2013: (Start)",
				"Identical to the morphism 0-\u003e 011010010, 1-\u003e011010011 given on p.100 of the Fxtbook (see link), because 0 -\u003e 011 -\u003e 011010010 and 1 -\u003e 010 -\u003e 011010011.",
				"This sequence gives the turns (by 120 degrees) of the R9-dragon curve (displayed on p.101) which can be rendered as follows:",
				"  [Init] Set n=0 and direction=0.",
				"  [Draw] Draw a unit line (in the current direction). Turn left/right if a(n) is zero/nonzero respectively.",
				"  [Next] Set n=n+1 and goto (draw).",
				"(End)"
			],
			"reference": [
				"M. Lothaire, Combinatorics on words."
			],
			"link": [
				"J.-P. Allouche and R. Bacher, \u003ca href=\"http://dx.doi.org/10.5169/seals-59494\"\u003eToeplitz Sequences, Paperfolding, Towers of Hanoi, and Progression-Free Sequences of Integers\u003c/a\u003e, L'Enseignement Mathématique, volume 38, pages 315-327, 1992.",
				"Joerg Arndt, \u003ca href=\"http://www.jjj.de/fxt/#fxtbook\"\u003eMatters Computational (The Fxtbook)\u003c/a\u003e (section 1.31.5 \"Dragon curves based on radix-R counting\", pp. 95-101, image on p. 101).",
				"Gabriele Fici and Jeffrey Shallit, \u003ca href=\"https://arxiv.org/abs/2112.12125\"\u003eProperties of a Class of Toeplitz Words\u003c/a\u003e, arXiv:2112.12125 [cs.FL], 2021.",
				"Kevin Ryde, \u003ca href=\"http://user42.tuxfamily.org/terdragon/index.html\"\u003eIterations of the Terdragon Curve\u003c/a\u003e, see index \"AltTurnRpred\" with AltTurnRpred(n) = a(n-1).",
				"\u003ca href=\"/index/Fi#FIXEDPOINTS\"\u003eIndex entries for sequences that are fixed points of mappings\u003c/a\u003e"
			],
			"formula": [
				"Start with 0 and apply the morphism 0-\u003e011 and 1-\u003e010 repeatedly.",
				"a(3k-2)=0, a(3k-1)=1, a(3k)=1-a(k) for k\u003e=1, a(0)=0. - _Clark Kimberling_, Apr 28 2011"
			],
			"example": [
				"0 -\u003e 0,1,1 -\u003e 0,1,1,0,1,0,0,1,0 -\u003e ..."
			],
			"mathematica": [
				"Nest[ Flatten[ # /. {0 -\u003e {0, 1, 1}, 1 -\u003e {0, 1, 0}}] \u0026, {0}, 10]",
				"SubstitutionSystem[{0-\u003e{0,1,1},1-\u003e{0,1,0}},0,{5}][[1]] (* _Harvey P. Dale_, Jan 15 2022 *)"
			],
			"xref": [
				"Cf. A278996 (indices of 0's), A278997 (indices of 1's), A189717 (partial sums).",
				"Cf. A189628 (morphisms guide).",
				"Cf. A307672 (draws curves that align with the Sierpinski gasket)."
			],
			"keyword": "easy,nice,nonn,changed",
			"offset": "0,1",
			"author": "Alexis Monnerot-Dumaine (alexis.monnerotdumaine(AT)gmail.com), Feb 10 2009",
			"references": 7,
			"revision": 42,
			"time": "2022-01-15T15:20:14-05:00",
			"created": "2009-02-27T03:00:00-05:00"
		}
	]
}