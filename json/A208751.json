{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A208751",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 208751,
			"data": "1,1,2,1,6,2,1,12,12,2,1,20,40,18,2,1,30,100,86,24,2,1,42,210,294,150,30,2,1,56,392,812,656,232,36,2,1,72,672,1932,2268,1240,332,42,2,1,90,1080,4116,6624,5172,2100,450,48,2,1,110,1650,8052,17028,17996",
			"name": "Triangle of coefficients of polynomials u(n,x) jointly generated with A208752; see the Formula section.",
			"comment": [
				"For a discussion and guide to related arrays, see A208510.",
				"Subtriangle of the triangle T(n,k) given by (1, 0, 1, 0, 0, 0, 0, 0, 0, 0, ...) DELTA (0, 2, -1, 0, 0, 0, 0, 0, 0, 0, ...) where DELTA is the operator defined in A084938. - _Philippe Deléham_, Mar 17 2012"
			],
			"formula": [
				"u(n,x) = u(n-1,x) + 2x*v(n-1,x),",
				"v(n,x) = (x+1)*u(n-1,x) + (x+1)*v(n-1,x),",
				"where u(1,x)=1, v(1,x)=1.",
				"From _Philippe Deléham_, Mar 17 2012: (Start)",
				"As DELTA-triangle with 0 \u003c= k \u003c= n:",
				"G.f.: (1-x-y*x)/(1-2*x-y*x+x^2-y*x^2).",
				"T(n,k) = 2*T(n-1,k) + T(n-1,k-1) - T(n-2,k) + T(n-2,k-1), T(0,0) = T(1,0) = T(2,0) = 1, T(1,1) = T(2,2) = 0, T(2,1) = 2 and T(n,k) = 0 if k \u003c 0 or if k \u003e n. (End)"
			],
			"example": [
				"First five rows:",
				"  1;",
				"  1,  2;",
				"  1,  6,  2;",
				"  1, 12, 12,  2;",
				"  1, 20, 40, 18,  2;",
				"First five polynomials u(n,x):",
				"  1",
				"  1 +  2x",
				"  1 +  6x +  2x^2",
				"  1 + 12x + 12x^2 +  2x^3",
				"  1 + 20x + 40x^2 + 18x^3 + 2x^4",
				"From _Philippe Deléham_, Mar 17 2012: (Start)",
				"(1, 0, 1, 0, 0, ...) DELTA (0, 2, -1, 0, 0, ...) begins:",
				"  1;",
				"  1,   0;",
				"  1,   2,   0;",
				"  1,   6,   2,   0;",
				"  1,  12,  12,   2,   0;",
				"  1,  20,  40,  18,   2,   0;",
				"  1,  30, 100,  86,  24,   2,   0; (End)"
			],
			"mathematica": [
				"u[1, x_] := 1; v[1, x_] := 1; z = 16;",
				"u[n_, x_] := u[n - 1, x] + 2 x*v[n - 1, x];",
				"v[n_, x_] := u[n - 1, x] + (x + 1) v[n - 1, x];",
				"Table[Expand[u[n, x]], {n, 1, z/2}]",
				"Table[Expand[v[n, x]], {n, 1, z/2}]",
				"cu = Table[CoefficientList[u[n, x], x], {n, 1, z}];",
				"TableForm[cu]",
				"Flatten[%]    (* A208751 *)",
				"Table[Expand[v[n, x]], {n, 1, z}]",
				"cv = Table[CoefficientList[v[n, x], x], {n, 1, z}];",
				"TableForm[cv]",
				"Flatten[%]    (* A208752 *)"
			],
			"xref": [
				"Cf. A208752, A208510."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Clark Kimberling_, Mar 01 2012",
			"references": 3,
			"revision": 14,
			"time": "2020-01-24T03:30:02-05:00",
			"created": "2012-03-02T12:08:02-05:00"
		}
	]
}