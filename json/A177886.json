{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A177886",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 177886,
			"data": "1,0,1,1,3,0,5,2,8,0,9,1,11,0,5,9,15,0,17,3,7,0,21,2,34,0,62,7,27,0,29,8,11,0,15,9,35,0,13,6,39,0,41,9,36,0,45",
			"name": "The number of isomorphism classes of Latin quandles (a.k.a. left distributive quasigroups) of order n.",
			"comment": [
				"A quandle is Latin if its multiplication table is a Latin square.  A Latin quandle may be described as a left (or right) distributive quasigroup. Sherman Stein (see reference below) proved that a left distributive quasigroup of order n exists if and only if n is not of the form 4k + 2."
			],
			"link": [
				"W. E. Clark, M. Elhamdadi, M. Saito, T. Yeatman, \u003ca href=\"http://arxiv.org/abs/1312.3307\"\u003eQuandle Colorings of Knots and Applications\u003c/a\u003e, arXiv preprint arXiv:1312.3307, 2013",
				"G. Ehrman, A. Gurpinar, M. Thibault, D. Yetter, \u003ca href=\"http://www.math.ksu.edu/main/events/KSU-REU/REUquandle.pdf\"\u003eSome Sharp Ideas on Quandle Construction\u003c/a\u003e",
				"A. Hulpke, D. Stanovský, P. Vojtěchovský, \u003ca href=\"http://arxiv.org/abs/1409.2249\"\u003eConnected quandles and transitive groups\u003c/a\u003e, arXiv:1409.2249 [math.GR], 2014.",
				"S. Nelson, \u003ca href=\"http://arxiv.org/abs/math/0702038\"\u003eA polynomial invariant of finite quandles\u003c/a\u003e, arXiv:math/0702038 [math.QA], 2007.",
				"S. K. Stein, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-1957-0094404-6\"\u003eOn the Foundations of Quasigroups\u003c/a\u003e, Transactions of American Mathematical Society, 85 (1957), 228-256.",
				"Leandro Vendramin, \u003ca href=\"http://arxiv.org/abs/1105.5341\"\u003eOn the classification of quandles of low order\u003c/a\u003e, arXiv:1105.5341v1 [math.GT].",
				"Leandro Vendramin and Matías Graña, \u003ca href=\"http://code.google.com/p/rig/\"\u003eRig, a GAP package for racks and quandles\u003c/a\u003e."
			],
			"example": [
				"a(2) = 0 since the only quandle of order 2 has multiplication table with rows [1,1] and [2,2]."
			],
			"program": [
				"(GAP) (using the Rig package)",
				"LoadPackage(\"rig\");",
				"a:=[1,0];;",
				"Print(1,\",\");",
				"Print(0,\",\");",
				"for n in [3..35] do",
				"  a[n]:=0;",
				"  for i in [1..NrSmallQuandles(n)] do",
				"    if IsLatin(SmallQuandle(n,i)) then",
				"      a[n]:=a[n]+1;",
				"    fi;",
				"  od;",
				"  Print(a[n],\", \");",
				"od; # _W. Edwin Clark_, Nov 26 2011"
			],
			"xref": [
				"Cf. A181769, A176077, A181771.",
				"See also Index to OEIS under quandles."
			],
			"keyword": "nonn,more",
			"offset": "1,5",
			"author": "_W. Edwin Clark_, Dec 14 2010",
			"ext": [
				"Added fact due to S. K. Stein that a(4k+2) = 0 and a reference to Stein's paper.",
				"a(11)-a(35) from _W. Edwin Clark_, Nov 26 2011",
				"Links to the rig Gap package by _W. Edwin Clark_, Nov 26 2011",
				"a(36)-a(47) by _David Stanovsky_, Oct 01 2014"
			],
			"references": 1,
			"revision": 36,
			"time": "2021-12-26T21:09:00-05:00",
			"created": "2010-11-12T14:26:18-05:00"
		}
	]
}