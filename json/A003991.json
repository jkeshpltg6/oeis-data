{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A003991",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 3991,
			"data": "1,2,2,3,4,3,4,6,6,4,5,8,9,8,5,6,10,12,12,10,6,7,12,15,16,15,12,7,8,14,18,20,20,18,14,8,9,16,21,24,25,24,21,16,9,10,18,24,28,30,30,28,24,18,10,11,20,27,32,35,36,35,32,27,20,11,12,22,30,36,40,42,42,40,36,30,22,12",
			"name": "Multiplication table read by antidiagonals: T(i,j) = i*j, i\u003e=1, j\u003e=1.",
			"comment": [
				"Or, triangle X(n,m) = T(n-m+1,m) read by rows, in which row n gives the numbers n*1, (n-1)*2, (n-2)*3, ..., 2*(n-1), 1*n.",
				"Radius of incircle of Pythagorean triangle with sides a=(n+1)^2-m^2, b=2*(n+1)*m and c=(n+1)^2+m^2. - _Floor van Lamoen_, Aug 16 2001",
				"A permutation of A061017. - _Matthew Vandermast_, Feb 28 2003",
				"In the proof of countability of rational numbers they are arranged in a square array. a(n) = p*q where p/q is the corresponding rational number as read from the array. - _Amarnath Murthy_, May 29 2003",
				"Permanent of upper right n X n corner is A000442. - _Marc LeBrun_, Dec 11 2003",
				"Row 12 gives total number of partridges, turtle doves, ... and drummers drumming that you have received at the end of the Twelve Days of Christmas song. - _Alonso del Arte_, Jun 17 2005",
				"Consider a particle with spin S (a half-integer) and 2S+1 quantum states |m\u003e, m = -S,-S+1,...,S-1,S. Then the matrix element \u003cm+1|S_+|m\u003e = sqrt((S+m+1)(S-m)) of the spin-raising operator is the square-root of the triangular (tabl) element T(r,o) of this sequence in row r = 2S, and at offset o=2(S+m). T(r,o) is also the intensity |\u003cm+1|S_+|m\u003e\u003cm|S_-|m+1\u003e| of the transition between the states |m\u003e and |m+1\u003e. For example, the five transitions between the 6 states of a spin S=5/2 particle have relative intensities 5,8,9,8,5. The total intensity of all spin 5/2 transitions (relative to spin 1/2) is 35, which is the tetrahedral number A000292(5). - _Stanislav Sykora_, May 26 2012",
				"Sum_{k=0..2n-2} (-1)^k*a(A000124(2n-2)+k) = n. See A098359. - _Charlie Marion_, Apr 22 2013",
				"T(n, k) is also the (k-1)-superdiagonal sum of an n X n Toeplitz matrix M(n) whose first row consists of successive positive integer numbers 1, ..., n. - _Stefano Spezia_, Jul 12 2019"
			],
			"reference": [
				"J. H. Conway and R. K. Guy, The Book of Numbers, Copernicus Press, NY, 1996, p. 46."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A003991/b003991.txt\"\u003eRows n = 1..100 of triangle, flattened\u003c/a\u003e",
				"G. W. Leibniz, \u003ca href=\"/A003991/a003991.pdf\"\u003eDissertatio de arte combinatoria\u003c/a\u003e, 1666, Leipzig. (in Latin. This triangle appears on p. 208, page 44 of the PDF file).",
				"A. Necer, \u003ca href=\"http://dx.doi.org/10.5802/jtnb.205\"\u003eSéries formelles et produit de Hadamard\u003c/a\u003e, Journal de théorie des nombres de Bordeaux, 9 no. 2 (1997), p. 319-335.",
				"Boris Putievskiy, \u003ca href=\"http://arxiv.org/abs/1212.2732\"\u003eTransformations Integer Sequences And Pairing Functions\u003c/a\u003e arXiv:1212.2732 [math.CO], 2012."
			],
			"formula": [
				"Rectangular array: T(n, m) = n*m, n\u003e=1, m\u003e= 1.",
				"Triangle X(n, m) = T(n-m+1, m) = (n-m+1)*m.",
				"Sum_{i=1..n} Sum_{j=1..n} a(n) = A000537(n) [Sum of first n cubes; or n-th triangular number squared.] Determinant of all n X n contiguous subarrays of A003991 is 0. - _Gerald McGarvey_, Sep 26 2004",
				"G.f. as rectangular array: x * y / [ (1-x)^2 * (1-y)^2 ].",
				"a(n) = i*j, where i=floor((1+sqrt(8n-7))/2), j=n-i*(i-1)/2. - _Hieronymus Fischer_, Aug 08 2007",
				"As an infinite lower triangular matrix equals A000012 * A002260; where A000012 = (1; 1,1; 1,1,1; ...) and A002260 = (1; 1,2; 1,2,3; ...). - _Gary W. Adamson_, Oct 23 2007",
				"As a linear array, the sequence is a(n) = A002260(n)*A004736(n) or a(n) = ((t*t+3*t+4)/2-n)*(n-(t*(t+1)/2)), where t=floor((-1+sqrt(8*n-7))/2). - _Boris Putievskiy_, Dec 17 2012",
				"G.f. as linear array: (x - 3*x^2 + Sum_{k \u003e= 0} ((k+2-x-(k+1)*x^2)*x^((k^2+3*k+4)/2)))/(1-x)^3. - _Robert Israel_, Dec 14 2015",
				"E.g.f. as triangle: exp(x+y)*(1 + x - y + x*y - y^2). - _Stefano Spezia_, Jul 12 2019",
				"a(n) = (1/2)*t + (n - 1/4)*t^2 - (1/4)*t^4 - n^2 + n, where t = floor(sqrt(2*n) + 1/2). - _Ridouane Oudra_, Nov 21 2020",
				"a(n) = A003989(n) * A003990(n) = A059895(n) * A059896(n) = A059895(n)^2 * A059897(n). - _Antti Karttunen_, Dec 13 2021"
			],
			"example": [
				"The array T starts in row n=1 with columns m\u003e=1 as:",
				"   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15",
				"   2   4   6   8  10  12  14  16  18  20  22  24  26  28  30",
				"   3   6   9  12  15  18  21  24  27  30  33  36  39  42  45",
				"   4   8  12  16  20  24  28  32  36  40  44  48  52  56  60",
				"   5  10  15  20  25  30  35  40  45  50  55  60  65  70  75",
				"   6  12  18  24  30  36  42  48  54  60  66  72  78  84  90",
				"   7  14  21  28  35  42  49  56  63  70  77  84  91  98 105",
				"   8  16  24  32  40  48  56  64  72  80  88  96 104 112 120",
				"   9  18  27  36  45  54  63  72  81  90  99 108 117 126 135",
				"  10  20  30  40  50  60  70  80  90 100 110 120 130 140 150",
				"The triangle X(n, m) begins",
				"   n\\m  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 ...",
				"   1:   1",
				"   2:   2  2",
				"   3:   3  4  3",
				"   4:   4  6  6  4",
				"   5:   5  8  9  8  5",
				"   6:   6 10 12 12 10  6",
				"   7:   7 12 15 16 15 12  7",
				"   8:   8 14 18 20 20 18 14  8",
				"   9:   9 16 21 24 25 24 21 16  9",
				"  10:  10 18 24 28 30 30 28 24 18 10",
				"  11:  11 20 27 32 35 36 35 32 27 20 11",
				"  12:  12 22 30 36 40 42 42 40 36 30 22 12",
				"  13:  13 24 33 40 45 48 49 48 45 40 33 24 13",
				"  14:  14 26 36 44 50 54 56 56 54 50 44 36 26 14",
				"  15:  15 28 39 48 55 60 63 64 63 60 55 48 39 28 15",
				"  ... Formatted by _Wolfdieter Lang_, Dec 02 2014"
			],
			"maple": [
				"seq(seq(i*(n-i),i=1..n-1),n=2..10); # _Robert Israel_, Dec 14 2015"
			],
			"mathematica": [
				"Table[(x + 1 - y) y, {x, 13}, {y, x}] // Flatten (* _Robert G. Wilson v_, Oct 06 2007 *)",
				"f[n_] := Table[SeriesCoefficient[E^(x + y) (1+ x - y +x*y-y^2), {x, 0, i}, {y, 0, j}]*i!*j!, {i, n, n}, {j, 0, n}]; Flatten[Array[f, 11,0]] (* _Stefano Spezia_, Jul 12 2019 *)"
			],
			"program": [
				"(PARI) A003991(n,k) = if(k\u003c1 || n\u003c1,0,k*n)",
				"(MAGMA) /* As triangle */ [[k*(n-k+1): k in [1..n]]: n in [1..15]]; // _Vincenzo Librandi_, Jul 12 2019"
			],
			"xref": [
				"Main diagonal gives squares A000290. Antidiagonal sums are tetrahedral numbers A000292. See A004247 for another version.",
				"Cf. A003989, A003990, A003056, A049581, A000442, A027424, A002260, A033638, A059895, A059896, A059897.",
				"Cf. also A051776, A067138, A091257, A325821, A331590, A341520, A350066."
			],
			"keyword": "tabl,nonn,nice,easy,look",
			"offset": "1,2",
			"author": "_Marc LeBrun_",
			"ext": [
				"More terms from _Michael Somos_"
			],
			"references": 93,
			"revision": 157,
			"time": "2021-12-14T12:09:46-05:00",
			"created": "1996-03-15T03:00:00-05:00"
		}
	]
}