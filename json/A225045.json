{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A225045",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 225045,
			"data": "1,0,1,0,1,1,1,2,1,3,1,4,3,5,5,5,7,7,10,10,13,13,16,18,21,25,27,32,33,41,44,53,57,65,73,81,93,102,118,128,145,159,181,200,224,246,275,304,337,375,413,460,503,559,614,679,749,821,907,991,1096,1197,1319,1442,1582,1733,1893,2076,2265,2482,2702,2956,3220",
			"name": "Number of partitions of n into distinct non-triangular numbers, cf. A014132.",
			"link": [
				"Alois P. Heinz and Vaclav Kotesovec, \u003ca href=\"/A225045/b225045.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e (terms 0..1000 from Alois P. Heinz)"
			],
			"formula": [
				"G.f.: prod(n\u003e=1, 1 + q^n ) / prod(n\u003e=1, 1 + q^(n*(n+1)/2) ). [_Joerg Arndt_, Apr 01 2014]",
				"a(n) ~ exp(Pi*sqrt(n/3) - 3^(1/4) * Zeta(3/2) * n^(1/4) / (2+sqrt(2)) - 3*(3-2*sqrt(2)) * Zeta(3/2)^2 / (16*Pi)) / (2*3^(1/4)*n^(3/4)). - _Vaclav Kotesovec_, Jan 02 2017"
			],
			"example": [
				"a(10) = #{8+2} = 1;",
				"a(11) = #{11, 9+2, 7+4, 5+4+2} = 4;",
				"a(12) = #{12, 8+4, 7+5} = 3;",
				"a(13) = #{13, 11+2, 9+4, 8+5, 7+4+2} = 5."
			],
			"maple": [
				"b:= proc(n, i) option remember; `if`(n\u003ei*(i+1)/2, 0,",
				"      `if`(n=0, 1, add(b(n-i*j, i-1), j=0..min(n/i,",
				"      `if`(issqr(8*i+1), 0, 1)))))",
				"    end:",
				"a:= n-\u003e b(n$2):",
				"seq(a(n), n=0..80);  # _Alois P. Heinz_, Apr 01 2014"
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = If[n \u003e i*(i+1)/2, 0, If[n==0, 1, Sum[b[n-i*j, i-1], {j, 0, Min[n/i, If[IntegerQ[Sqrt[8*i+1]], 0, 1]]}]]]; a[n_] := b[n, n]; Table[a[n], {n, 0, 80}] (* _Jean-François Alcover_, Jan 15 2016, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(Haskell)",
				"a225045 = p a014132_list where",
				"   p _      0 = 1",
				"   p (k:ks) m = if m \u003c k then 0 else p ks (m - k) + p ks m",
				"(PARI) N=66; q='q+O('q^N); Vec( prod(n=1,N, 1 + q^n) / prod(n=1,N, 1 + q^(n*(n+1)/2)) ) \\\\ _Joerg Arndt_, Apr 01 2014"
			],
			"xref": [
				"Cf. A000009, A024940, A225044, A087154."
			],
			"keyword": "nonn",
			"offset": "0,8",
			"author": "_Reinhard Zumkeller_, Apr 25 2013",
			"references": 3,
			"revision": 18,
			"time": "2017-01-02T17:45:48-05:00",
			"created": "2013-04-25T16:04:59-04:00"
		}
	]
}