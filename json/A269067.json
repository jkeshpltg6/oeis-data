{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A269067",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 269067,
			"data": "2,3,16,115,88,5887,19328,259723,124952,381773117,41931328,20646903199,866732192,467168310097,2386873693184,75920439315929441,97261697912,5278968781483042969,2387693641959232,9093099984535515162569,10872995484706511008,168702835448329388944396777,38650653745373963289088",
			"name": "Numerator of the volume of d dimensional symmetric convex cuboid with constraints |x1 + x2 + ... xd| \u003c= 1 and |x1|, |x2|, ..., |xd| \u003c= 1.",
			"comment": [
				"Reference A. Dubickas shows that all the volume integrals are rational with V[d] \u003c= 2^d."
			],
			"link": [
				"R. Chela, \u003ca href=\"http://jlms.oxfordjournals.org/content/s1-38/1/183.extract\"\u003eReducible Polynomials\u003c/a\u003e, Journal London Math. Soc. 38 (1963), pp 183-188 Eq. 7.",
				"Arturas Dubickas, \u003ca href=\"http://dx.doi.org/10.1007/s00229-014-0657-y\"\u003eOn the number of reducible polynomials of bounded naive height\u003c/a\u003e, Manuscripta Math. 144 (2014), pp 439-456, Eq. 4, 5 \u0026 Section 5.",
				"Mathematica Stack Exchange, \u003ca href=\"https://mathematica.stackexchange.com/questions/172777/how-to-improve-or-optimize-a-volume-integration-over-a-cuboid\"\u003eHow to improve or optimize a volume integration over a cuboid\u003c/a\u003e"
			],
			"example": [
				"For d = 3 the volume is 16/3, for each volume we have V[1] = 2, V[2] = 3, V[3] = 16/3, V[4] = 115/12, V[5] = 88/5, V[6] = 5887/180, V[7] = 19328/315, V[8] = 259723/2240, V[9] = 124952/567, V[10] = 381773117/907200, etc."
			],
			"mathematica": [
				"V[d_] := Integrate[Boole[Abs[Sum[x[i], {i, 1, d}]] \u003c= 1],",
				"  Table[x[i], {i, 1, d}] \\[Element]",
				"   Cuboid[Table[-1, {i, 1, d}], Table[+1, {i, 1, d}]] (* Lorenz H. Menke, Jr. *)",
				"v[d_] := With[{a = Array[x,d]}, RegionMeasure @ ImplicitRegion[ a ∈ Cuboid[-Table[1, d], Table[1, d]] \u0026\u0026 -1 \u003c= Total[a] \u003c= 1, a]] (* Carl Woll *)",
				"v[d_] := 2^(d+1)/(Pi) Integrate[Sin[t]^(d+1)/t^(n+1), {t, 0, Infinity}] (* Carl Woll *)"
			],
			"xref": [
				"The denominator sequence is given by A266913.",
				"Cf. A199832 The rational coefficient of the leading coefficient of the empirical rows duplicates these volume integrals in sequence.  This is not a proof."
			],
			"keyword": "nonn,frac",
			"offset": "1,1",
			"author": "_Lorenz H. Menke, Jr._, Feb 19 2016",
			"ext": [
				"a(11)-a(23) from _Lorenz H. Menke, Jr._, May 10 2018"
			],
			"references": 1,
			"revision": 31,
			"time": "2018-05-28T09:17:04-04:00",
			"created": "2016-03-21T21:36:21-04:00"
		}
	]
}