{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A117967",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 117967,
			"data": "0,1,5,3,4,17,15,16,11,9,10,14,12,13,53,51,52,47,45,46,50,48,49,35,33,34,29,27,28,32,30,31,44,42,43,38,36,37,41,39,40,161,159,160,155,153,154,158,156,157,143,141,142,137,135,136,140,138,139,152,150,151,146",
			"name": "Positive part of inverse of A117966; write n in balanced ternary and then replace (-1)'s with 2's.",
			"reference": [
				"D. E. Knuth, The Art of Computer Programming. Addison-Wesley, Reading, MA, Vol. 2, pp. 173-175"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A117967/b117967.txt\"\u003eTable of n, a(n) for n = 0..9841\u003c/a\u003e",
				"Ken Levasseur, \u003ca href=\"http://discretemath.org/ternary_number_system.html\"\u003eThe Balanced Ternary Number System\u003c/a\u003e"
			],
			"formula": [
				"a(0) = 0, a(3n) = 3a(n), a(3n+1) = 3a(n)+1, a(3n-1) = 3a(n)+2.",
				"If one adds this clause, then the function is defined on the whole Z: If n\u003c0, then a(n) = A004488(a(-n)) (or equivalently: a(n) = A117968(-n)) and then it holds that a(A117966(n)) = n. - _Antti Karttunen_, May 19 2008"
			],
			"example": [
				"7 in balanced ternary is 1(-1)1, changing to 121 ternary is 16, so a(7)=16."
			],
			"maple": [
				"a:= proc(n) local d, i, m, r; m:=n; r:=0;",
				"      for i from 0 while m\u003e0 do",
				"         d:= irem(m, 3, 'm');",
				"         if d=2 then m:=m+1 fi;",
				"         r:= r+d*3^i",
				"      od; r",
				"    end:",
				"seq(a(n), n=0..100);  # _Alois P. Heinz_, May 11 2015"
			],
			"mathematica": [
				"a[n_] := Module[{d, i, m = n, r = 0}, For[i = 0, m \u003e 0, i++, {m, d} = QuotientRemainder[m, 3]; If[d == 2, m++]; r = r + d*3^i]; r];",
				"a /@ Range[0, 100] (* _Jean-François Alcover_, Jan 05 2021, after _Alois P. Heinz_ *)"
			],
			"program": [
				"(Scheme)",
				"(Two alternative definitions in MIT/GNU Scheme, defined for whole Z:)",
				"(define (A117967 z) (cond ((zero? z) 0) ((negative? z) (A004488 (A117967 (- z)))) (else (let* ((lp3 (expt 3 (A062153 z))) (np3 (* 3 lp3))) (if (\u003c (* 2 z) np3) (+ lp3 (A117967 (- z lp3))) (+ np3 (A117967 (- z np3))))))))",
				"(define (A117967v2 z) (cond ((zero? z) 0) ((negative? z) (A004488 (A117967v2 (- z)))) ((zero? (modulo z 3)) (* 3 (A117967v2 (/ z 3)))) ((= 1 (modulo z 3)) (+ (* 3 (A117967v2 (/ (- z 1) 3))) 1)) (else (+ (* 3 (A117967v2 (/ (+ z 1) 3))) 2))))",
				";; _Antti Karttunen_, May 19 2008",
				"(Python)",
				"from sympy.ntheory.factor_ import digits",
				"def a004488(n): return int(\"\".join([str((3 - i)%3) for i in digits(n, 3)[1:]]), 3)",
				"def a117968(n):",
				"    if n==1: return 2",
				"    if n%3==0: return 3*a117968(n/3)",
				"    elif n%3==1: return 3*a117968((n - 1)/3) + 2",
				"    else: return 3*a117968((n + 1)/3) + 1",
				"def a(n): return 0 if n==0 else a004488(a117968(n)) # _Indranil Ghosh_, Jun 06 2017"
			],
			"xref": [
				"Cf. A117966. a(n) = A004488(A117968(n)). Bisection of A140263. A140267 gives the same sequence in ternary."
			],
			"keyword": "base,nonn,look",
			"offset": "0,3",
			"author": "_Franklin T. Adams-Watters_, Apr 05 2006",
			"references": 22,
			"revision": 24,
			"time": "2021-01-05T11:11:32-05:00",
			"created": "2006-02-24T03:00:00-05:00"
		}
	]
}