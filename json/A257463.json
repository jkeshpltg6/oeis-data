{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A257463",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 257463,
			"data": "1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,2,5,1,1,1,1,3,10,17,1,1,1,1,3,23,93,73,1,1,1,1,4,40,465,1417,388,1,1,1,1,4,73,1746,19834,32152,2461,1,1,1,1,5,114,5741,190131,1532489,1016489,18155,1,1",
			"name": "Number A(n,k) of factorizations of m^k into n factors, where m is a product of exactly n distinct primes and each factor is a product of k primes (counted with multiplicity); square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"comment": [
				"Also number of ways to partition the multiset consisting of k copies each of 1, 2, ..., n into n multisets of size k."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A257463/b257463.txt\"\u003eAntidiagonals n = 0..27, flattened\u003c/a\u003e (antidiagonals 0..12 from Alois P. Heinz)",
				"P. A. MacMahon, \u003ca href=\"http://plms.oxfordjournals.org/content/s2-17/1/25.extract\"\u003eCombinations derived from m identical sets of n different letters and their connexion with general magic squares\u003c/a\u003e, Proc. London Math. Soc., 17 (1917), 25-41. The array is on page 40.",
				"Math StackExchange, \u003ca href=\"http://math.stackexchange.com/questions/1641433/\"\u003eNumber of ways to partition 40 balls with 4 colors into 4 baskets\u003c/a\u003e",
				"Marko Riedel, \u003ca href=\"/A257463/a257463.txt\"\u003eMaple program to compute array from cycle indices\u003c/a\u003e"
			],
			"example": [
				"A(4,2) = 17: (2*3*5*7)^2 = 44100 = 15*15*14*14 = 21*15*14*10 = 21*21*10*10 = 25*14*14*9 = 25*21*14*6 = 25*21*21*4 = 35*14*10*9 = 35*15*14*6 = 35*21*10*6 = 35*21*15*4 = 35*35*6*6 = 35*35*9*4 = 49*10*10*9 = 49*15*10*6 = 49*15*15*4 = 49*25*6*6 = 49*25*9*4.",
				"A(3,3) = 10: (2*3*5)^3 = 2700 = 30*30*30 = 45*30*20 = 50*27*20 = 50*30*18 = 50*45*12 = 75*20*18 = 75*30*12 = 75*45*8 = 125*18*12 = 125*27*8.",
				"A(2,4) = 3: (2*3)^4 = 1296 = 36*36 = 54*24 = 81*16.",
				"Square array A(n,k) begins:",
				"  1, 1,   1,     1,       1,        1,         1, ...",
				"  1, 1,   1,     1,       1,        1,         1, ...",
				"  1, 1,   2,     2,       3,        3,         4, ...",
				"  1, 1,   5,    10,      23,       40,        73, ...",
				"  1, 1,  17,    93,     465,     1746,      5741, ...",
				"  1, 1,  73,  1417,   19834,   190131,   1398547, ...",
				"  1, 1, 388, 32152, 1532489, 43816115, 848597563, ..."
			],
			"maple": [
				"with(numtheory):",
				"b:= proc(n, i, k) option remember; `if`(n=1, 1,",
				"      add(`if`(d\u003ei or bigomega(d)\u003c\u003ek, 0,",
				"      b(n/d, d, k)), d=divisors(n)))",
				"    end:",
				"A:= (n, k)-\u003e b(mul(ithprime(i), i=1..n)^k$2, k):",
				"seq(seq(A(n, d-n), n=0..d), d=0..8);"
			],
			"mathematica": [
				"b[n_, i_, k_] := b[n, i, k] = If[n==1, 1, DivisorSum[n, If[#\u003ei || PrimeOmega[#] != k, 0, b[n/#, #, k]]\u0026]];",
				"A[n_, k_] := b[p = Product[Prime[i], {i, 1, n}]^k, p, k];",
				"Table[A[n, d-n], {d, 0, 10}, {n, 0, d}] // Flatten (* _Jean-François Alcover_, Mar 20 2017, translated from Maple *)"
			],
			"xref": [
				"Columns k=0+1, 2-4 give: A000012, A002135, A254243, A268668.",
				"Rows n=0+1, 2-5 give: A000012, A008619, A257464, A253259, A253263.",
				"Main diagonal gives A334286.",
				"Cf. A257462, A257493 (ordered factorizations)."
			],
			"keyword": "nonn,tabl",
			"offset": "0,13",
			"author": "_Alois P. Heinz_, Apr 24 2015",
			"references": 11,
			"revision": 37,
			"time": "2020-04-21T17:31:24-04:00",
			"created": "2015-04-24T18:33:30-04:00"
		}
	]
}