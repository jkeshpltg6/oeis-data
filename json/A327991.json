{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327991",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327991,
			"data": "1,2,2,6,2,6,30,2,6,30,10,210,2,6,30,10,210,42,70,2310,2,6,30,10,210,42,70,2310,14,330,462,770,30030,2,6,30,10,210,42,70,2310,14,330,462,770,30030,66,110,2730,154,4290,6006,10010,510510",
			"name": "The complementary Fibonacci codes. Irregular triangle T(n, k) with n \u003e= 0 and 0 \u003c= k \u003c A000045(n+1).",
			"comment": [
				"The complementary Fibonacci codes are binary strings enumerated in an irregular triangle CF(n, k). The first few are shown below in the Example section. The complementary Fibonacci codes are the bitwise complements of the Fibonacci codes described in A327990, in ascending order.",
				"The complementary Fibonacci codes are represented here through",
				"    T(n, k) = Product_{j=0..m} p(j)^c(j),",
				"where p(j) is the j-th prime number, c = CF(n, k) and m = length(CF(n, k))."
			],
			"example": [
				"The complementary Fibonacci codes start:",
				"[0] [[]]",
				"[1] [[1]]",
				"[2] [[1][11]]",
				"[3] [[1][11][111]]",
				"[4] [[1][11][111][101][1111]]",
				"[5] [[1][11][111][101][1111][1101][1011][11111]]",
				"[6] [[1][11][111][101][1111][1101][1011][11111][1001][11101][11011][10111][111111]]",
				"[7] [[1][11][111][101][1111][1101][1011][11111][1001][11101][11011][10111][111111] [11001][10101][111101][10011][111011][110111][101111][1111111]]",
				"The representation of the complementary Fibonacci codes starts:",
				"[0] [1]",
				"[1] [2]",
				"[2] [2, 6]",
				"[3] [2, 6, 30]",
				"[4] [2, 6, 30, 10, 210]",
				"[5] [2, 6, 30, 10, 210, 42, 70, 2310]",
				"[6] [2, 6, 30, 10, 210, 42, 70, 2310, 14, 330, 462, 770, 30030]",
				"[7] [2, 6, 30, 10, 210, 42, 70, 2310, 14, 330, 462, 770, 30030, 66, 110, 2730, 154, 4290, 6006, 10010, 510510]"
			],
			"program": [
				"(SageMath)",
				"@cached_function",
				"def FibonacciCodes(n):",
				"    if n == 0 : return [[]]",
				"    if n == 1 : return [[1]]",
				"    A = [c.conjugate() for c in Compositions(n) if not(1 in c)]",
				"    return FibonacciCodes(n-1) + [[2-i for i in a] for a in A]",
				"def A327991row(n):",
				"    P = Primes()",
				"    M = lambda C: mul(P[i]^c for (i, c) in enumerate(C))",
				"    return [M(c) for c in FibonacciCodes(n)]",
				"for n in (0..7): print(A327991row(n))"
			],
			"xref": [
				"The diagonal is A002110 (primorial numbers).",
				"Cf. A309896, A327990, A000045, A001924."
			],
			"keyword": "nonn,tabf",
			"offset": "0,2",
			"author": "_Peter Luschny_, Oct 09 2019",
			"references": 0,
			"revision": 10,
			"time": "2019-10-09T02:31:35-04:00",
			"created": "2019-10-09T02:22:33-04:00"
		}
	]
}