{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A094816",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 94816,
			"data": "1,1,1,1,3,1,1,8,6,1,1,24,29,10,1,1,89,145,75,15,1,1,415,814,545,160,21,1,1,2372,5243,4179,1575,301,28,1,1,16072,38618,34860,15659,3836,518,36,1,1,125673,321690,318926,163191,47775,8274,834,45,1,1,1112083,2995011",
			"name": "Triangle read by rows: T(n,k) are the coefficients of Charlier polynomials: A046716 transposed, for 0 \u003c= k \u003c= n.",
			"comment": [
				"The a-sequence for this Sheffer matrix is A027641(n)/A027642(n) (Bernoulli numbers) and the z-sequence is A130189(n)/ A130190(n). See the W. Lang link.",
				"Take the lower triangular matrix in A049020 and invert it, then read by rows! - _N. J. A. Sloane_, Feb 07 2009",
				"Exponential Riordan array [exp(x), log(1/(1-x))]. Equal to A007318*A132393. - _Paul Barry_, Apr 23 2009",
				"A signed version of the triangle appears in [Gessel]. - _Peter Bala_, Aug 31 2012",
				"T(n,k) is the number of permutations over all subsets of {1,2,...,n} (Cf. A000522) that have exactly k cycles.  T(3,2) = 6: We permute the elements of the subsets {1,2}, {1,3}, {2,3}. Each has one permutation with 2 cycles.  We permute the elements of {1,2,3} and there are three permutations that have 2 cycles. 3*1 + 1*3 = 6. - _Geoffrey Critzer_, Feb 24 2013",
				"From _Wolfdieter Lang_, Jul 28 2017: (Start)",
				"In Chihara's book the row polynomials (with rising powers) are the Charlier polynomials (-1)^n*C^(a)_n(-x), with a = -1, n \u003e= 0. See  p. 170, eq. (1.4).",
				"In Ismail's book the present Charlier polynomials are denoted by C_n(-x;a=1) on p. 177, eq. (6.1.25). (End)"
			],
			"reference": [
				"T. S. Chihara, An Introduction to Orthogonal Polynomials, Gordon and Breach, New York, London, Paris, 1978, Ch. VI, 1., pp. 170-172.",
				"Classical and Quantum Orthogonal Polynomials in One Variable, Cambridge University Press, 2005, EMA, Vol. 98, p. 177."
			],
			"link": [
				"Paul Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Barry3/barry100r.html\"\u003eThe Restricted Toda Chain, Exponential Riordan Arrays, and Hankel Transforms\u003c/a\u003e, J. Int. Seq. 13 (2010) # 10.8.4, example 6.",
				"Paul Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Barry4/barry122.html\"\u003eExponential Riordan Arrays and Permutation Enumeration\u003c/a\u003e, J. Int. Seq. 13 (2010) # 10.9.1, example 8.",
				"Paul Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL14/Barry1/barry97r2.html\"\u003eRiordan Arrays, Orthogonal Polynomials as Moments, and Hankel Transforms\u003c/a\u003e, J. Int. Seq. 14 (2011) # 11.2.2, example 22.",
				"Paul Barry, \u003ca href=\"http://arxiv.org/abs/1105.3044\"\u003eCombinatorial polynomials as moments, Hankel transforms and exponential Riordan arrays\u003c/a\u003e, arXiv preprint arXiv:1105.3044 [math.CO], 2011, also \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL14/Barry5/barry112.html\"\u003eJ. Int. Seq. 14 (2011)  11.6.7\u003c/a\u003e.",
				"Ira Gessel, \u003ca href=\"https://www.fq.math.ca/Scanned/19-2/gessel.pdf\"\u003eCongruences for Bell and Tangent numbers\u003c/a\u003e, The Fibonacci Quarterly, Vol. 19, Number 2, 1981.",
				"Aoife Hennessy, Paul Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL14/Barry6/barry161.html\"\u003eGeneralized Stirling Numbers, Exponential Riordan Arrays, and Orthogonal Polynomials\u003c/a\u003e, J. Int. Seq. 14 (2011) # 11.8.2.",
				"Marin Knežević, Vedran Krčadinac, and Lucija Relić, \u003ca href=\"https://arxiv.org/abs/2012.15307\"\u003eMatrix products of binomial coefficients and unsigned Stirling numbers\u003c/a\u003e, arXiv:2012.15307 [math.CO], 2020.",
				"Wolfdieter Lang, \u003ca href=\"/A094816/a094816.txt\"\u003eFirst 10 rows and more.\u003c/a\u003e",
				"Wolfdieter Lang, \u003ca href=\"https://arxiv.org/abs/1708.01421\"\u003eOn Generating functions of Diagonals Sequences of Sheffer and Riordan Number Triangles\u003c/a\u003e, arXiv:1708.01421 [math.NT], August 2017.",
				"W. F. Lunnon, P. A. B. Pleasants, and N. M. Stephens, \u003ca href=\"http://matwbn.icm.edu.pl/ksiazki/aa/aa35/aa3511.pdf\"\u003eArithmetic properties of Bell numbers to a composite modulus I\u003c/a\u003e, Acta Arithmetica 35 (1979), pp. 1-16."
			],
			"formula": [
				"E.g.f.: exp(t)/(1-t)^(-x) = Sum_{n\u003e=0} C(-x,n)*t^n/n!.",
				"Sum_{k = 0..n} T(n, k)*x^k = C(x, n), Charlier polynomials; C(x, n)= A024000(n), A000012(n), A000522(n), A001339(n), A082030(n), A095000(n), A095177(n), A096307(n), A096341(n), A095722(n), A095740(n) for x = -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 respectively. - _Philippe Deléham_, Feb 27 2013",
				"T(n+1, k) = (n+1)*T(n, k) + T(n, k-1) - n*T(n-1, k) with T(0, 0) = 1, T(0, k) = 0 if k\u003e0, T(n, k) = 0 if k\u003c0.",
				"PS*A008275*PS as infinite lower triangular matrices, where PS is a triangle with PS(n, k) = (-1)^k*A007318(n, k). PS = 1/PS. - _Gerald McGarvey_, Aug 20 2009",
				"T(n,k) = (-1)^(n-k)*Sum_{j=0..n} C(-j-1, -n-1)*S1(j, k) where S1 are the signed Stirling numbers of the first kind. - _Peter Luschny_, Apr 10 2016",
				"Absolute values T(n,k) of triangle (-1)^(n+k) T(n,k) where row n gives coefficients of x^k, 0 \u003c= k \u003c= n, in expansion of Sum_{k=0..n} binomial(n,k) (-1)^(n-k) x^{(k)}, where x^{(k)} := Product_{i=0..k-1} (x-i), k \u003e= 1, and x^{(0)} := 1, the falling factorial powers. - _Daniel Forgues_, Oct 13 2019",
				"From _Peter Bala_, Oct 23 2019: (Start)",
				"The n-th row polynomial is",
				"   R(n, x) = Sum_{k = 0..n} (-1)^k*binomial(n, k)*k! * binomial(-x, k).",
				"These polynomials occur in series acceleration formulas for the constant",
				"   1/e = n! * Sum_{k \u003e= 0} (-1)^k/(k!*R(n,k)*R(n,k+1)), n \u003e= 0. (cf. A068985, A094816 and A137346). (End)",
				"R(n, x) = KummerU[-n, 1 - n - x, 1]. - _Peter Luschny_, Oct 27 2019",
				"Sum_{j=0..m} (-1)^(m-j) * Bell(n+j) * T(m,j) = m! * Sum_{k=0..n} binomial(k,m) * stirling2(n,k). - _Vaclav Kotesovec_, Aug 06 2021"
			],
			"example": [
				"From _Paul Barry_, Apr 23 2009: (Start)",
				"Triangle begins",
				"  1;",
				"  1,     1;",
				"  1,     3,     1;",
				"  1,     8,     6,     1;",
				"  1,    24,    29,    10,     1;",
				"  1,    89,   145,    75,    15,    1;",
				"  1,   415,   814,   545,   160,   21,   1;",
				"  1,  2372,  5243,  4179,  1575,  301,  28,  1;",
				"  1, 16072, 38618, 34860, 15659, 3836, 518, 36, 1;",
				"Production matrix is",
				"  1, 1;",
				"  0, 2, 1;",
				"  0, 1, 3,  1;",
				"  0, 1, 3,  4,  1;",
				"  0, 1, 4,  6,  5,  1;",
				"  0, 1, 5, 10, 10,  6,  1;",
				"  0, 1, 6, 15, 20, 15,  7,  1;",
				"  0, 1, 7, 21, 35, 35, 21,  8, 1;",
				"  0, 1, 8, 28, 56, 70, 56, 28, 9, 1; (End)"
			],
			"maple": [
				"A094816 := (n,k) -\u003e (-1)^(n-k)*add(binomial(-j-1,-n-1)*Stirling1(j,k), j=0..n):",
				"seq(seq(A094816(n, k), k=0..n), n=0..9); # _Peter Luschny_, Apr 10 2016"
			],
			"mathematica": [
				"nn=10;f[list_]:=Select[list,#\u003e0\u0026];Map[f,Range[0,nn]!CoefficientList[Series[ Exp[x]/(1-x)^y,{x,0,nn}],{x,y}]]//Grid  (* _Geoffrey Critzer_, Feb 24 2013 *)",
				"Flatten[Table[(-1)^(n-k) Sum[Binomial[-j-1,-n-1] StirlingS1[j,k],{j,0,n}], {n,0,9},{k,0,n}]] (* _Peter Luschny_, Apr 10 2016 *)",
				"p[n_] := HypergeometricU[-n, 1 - n - x, 1];",
				"Table[CoefficientList[p[n], x], {n,0,9}] // Flatten (* _Peter Luschny_, Oct 27 2019 *)"
			],
			"program": [
				"(PARI) {T(n, k)= local(A); if( k\u003c0 || k\u003en, 0, A = x * O(x^n); polcoeff( n! * polcoeff( exp(x + A) / (1 - x + A)^y, n), k))} /* _Michael Somos_, Nov 19 2006 */",
				"(Sage)",
				"def a_row(n):",
				"    s = sum(binomial(n,k)*rising_factorial(x,k) for k in (0..n))",
				"    return expand(s).list()",
				"[a_row(n) for n in (0..9)] # _Peter Luschny_, Jun 28 2019"
			],
			"xref": [
				"Columns: A000012, A002104.",
				"Diagonals: A000012, A000217.",
				"Row sums A000522, alternating row sums A024000.",
				"KummerU(-n,1-n-x,z): this sequence (z=1), |A137346| (z=2), A327997 (z=3)."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "0,5",
			"author": "_Philippe Deléham_, Jun 12 2004",
			"references": 23,
			"revision": 123,
			"time": "2021-08-06T04:56:40-04:00",
			"created": "2004-09-22T03:00:00-04:00"
		}
	]
}