{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A053846",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 53846,
			"data": "1,2,14,236,12692,1783784,811523288,995733306992,3988947598331024,43581058503809001248,1559669026899267564563936,152805492791495918971070907584,49094725258525117931062810300451648,43237014297639482582550110281347475757696,124920254287369111633119733942816364074145497472",
			"name": "Number of n X n matrices over GF(3) of order dividing 2 (i.e., number of solutions of X^2=I in GL(n,3)).",
			"comment": [
				"Or, number of n X n invertible diagonalizable matrices over GF(3)."
			],
			"reference": [
				"V. Jovovic, The cycle index polynomials of some classical groups, Belgrade, 1995, unpublished."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A053846/b053846.txt\"\u003eTable of n, a(n) for n = 0..60\u003c/a\u003e",
				"Geoffrey Critzer, \u003ca href=\"https://esirc.emporia.edu/handle/123456789/3595\"\u003eCombinatorics of Vector Spaces over Finite Fields\u003c/a\u003e, Master's thesis, Emporia State University, 2018.",
				"Kent E. Morrison, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL9/Morrison/morrison37.html\"\u003eInteger Sequences and Matrices Over Finite Fields\u003c/a\u003e, Journal of Integer Sequences, Vol. 9 (2006), Article 06.2.1."
			],
			"formula": [
				"a(n)/A053290(n) is the coefficient of x^n in (Sum_{n\u003e=0} x^n/A053290(n))^2. - _Geoffrey Critzer_, Aug 05 2017"
			],
			"example": [
				"a(2) = 14 because we have: {{0, 1}, {1, 0}}, {{0, 2}, {2, 0}}, {{1, 0}, {0, 1}}, {{1, 0}, {0,2}}, {{1, 0}, {1, 2}}, {{1, 0}, {2, 2}}, {{1, 1}, {0, 2}}, {{1,2}, {0, 2}}, {{2, 0}, {0, 1}}, {{2, 0}, {0, 2}}, {{2, 0}, {1,1}}, {{2, 0}, {2, 1}}, {{2, 1}, {0, 1}}, {{2, 2}, {0, 1}}. -  _Geoffrey Critzer_, Aug 05 2017"
			],
			"maple": [
				"T:= proc(n, k) option remember; `if`(k\u003c0 or k\u003en, 0,",
				"      `if`(n=0, 1, T(n-1, k-1)+3^k*T(n-1, k)))",
				"    end:",
				"a:= n-\u003e add(3^(k*(n-k))*T(n, k), k=0...n):",
				"seq(a(n), n=0..15);  # _Alois P. Heinz_, Aug 06 2017"
			],
			"mathematica": [
				"nn = 12; g[ n_] := (q - 1)^n q^Binomial[n, 2] FunctionExpand[",
				"QFactorial[n, q]] /. q -\u003e 3; G[z_] := Sum[z^k/g[k], {k, 0, nn}];Table[g[n], {n, 0, nn}] CoefficientList[Series[G[z]^2, {z, 0, nn}], z] /* _Geoffrey Critzer_, Aug 05 2017"
			],
			"program": [
				"(PARI) a(n)={my(v=[1]); for(n=1,n,v=vector(#v+1,k,if(k\u003e1, v[k-1]) + if(k\u003c=#v, 3^(k-1)*v[k]))); sum(k=0,n,3^(k*(n-k))*v[k+1])} \\\\ _Andrew Howroyd_, Mar 02 2018",
				"(Python)",
				"from sympy.core.cache import cacheit",
				"@cacheit",
				"def T(n, k): return 0 if k\u003c0 or k\u003en else 1 if n==0 else T(n - 1, k - 1) + 3**k*T(n - 1, k)",
				"def a(n): return sum(3**(k*(n - k))*T(n, k) for k in range(n + 1))",
				"print([a(n) for n in range(15)]) # _Indranil Ghosh_, Aug 06 2017, after Maple code"
			],
			"xref": [
				"Cf. A053722, A053290, A290516."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Vladeta Jovovic_, Mar 28 2000",
			"ext": [
				"More terms from _Geoffrey Critzer_, Aug 05 2017"
			],
			"references": 8,
			"revision": 36,
			"time": "2020-05-04T19:17:31-04:00",
			"created": "2000-05-08T03:00:00-04:00"
		}
	]
}