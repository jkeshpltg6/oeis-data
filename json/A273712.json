{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A273712",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 273712,
			"data": "1,1,1,1,1,0,1,1,1,0,1,1,2,1,0,1,1,6,80,1,0,1,1,24,7484400,21964800,1,0,1,1,120,3892643213082624,35417271278873496315860673177600000000,74836825861835980800000,1,0",
			"name": "Number A(n,k) of k-ary heaps on n levels; square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A273712/b273712.txt\"\u003eAntidiagonals n = 0..9, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/D-ary_heap\"\u003eD-ary heap\u003c/a\u003e"
			],
			"example": [
				"Square array A(n,k) begins:",
				"  1, 1,        1,                                      1, ...",
				"  1, 1,        1,                                      1, ...",
				"  0, 1,        2,                                      6, ...",
				"  0, 1,       80,                                7484400, ...",
				"  0, 1, 21964800, 35417271278873496315860673177600000000, ..."
			],
			"maple": [
				"with(combinat):",
				"b:= proc(n, k) option remember; local h, i, x, y, z;",
				"      if n\u003c2 then 1 elif k\u003c2 then k",
				"    else h:= ilog[k]((k-1)*n+1);",
				"         if k^h=(k-1)*n+1 then b((n-1)/k, k)^k*",
				"            multinomial(n-1, ((n-1)/k)$k)",
				"       else x, y:=(k^h-1)/(k-1), (k^(h-1)-1)/(k-1);",
				"            for i from 0 do z:= (n-1)-(k-1-i)*y-i*x;",
				"              if y\u003c=z and z\u003c=x then b(y, k)^(k-1-i)*",
				"                 multinomial(n-1, y$(k-1-i), x$i, z)*",
				"                 b(x, k)^i*b(z, k); break fi",
				"            od",
				"      fi fi",
				"    end:",
				"A:= (n, k)-\u003e `if`(n\u003c2, 1, `if`(k\u003c2, k, b((k^n-1)/(k-1), k))):",
				"seq(seq(A(n,d-n), n=0..d), d=0..7);"
			],
			"mathematica": [
				"multinomial[n_, k_List] := n!/Times @@ (k!);",
				"b[n_, k_] := b[n, k] = Module[{h, i, x, y, z}, Which[n\u003c2, 1, k\u003c2, k, True, h = Log[k, (k-1)*n+1] // Floor; If[k^h == (k-1)*n+1, b[(n-1)/k, k]^k*multinomial[n-1, Array[(n-1)/k\u0026, k]], {x, y} := {(k^h-1)/(k-1), (k^(h-1)-1)/(k-1)}; For[i = 0, True, i++, z = (n-1) - (k-1-i)*y - i*x; If[y \u003c= z \u0026\u0026 z \u003c= x, b[y, k]^(k-1-i) * multinomial[n-1, Join[Array[y\u0026, k-1-i], Array[x\u0026, i], {z}]]*b[x, k]^i * b[z, k]; Break[]]]]]];",
				"A[n_, k_] := If[n\u003c2, 1, If[k\u003c2, k, b[(k^n-1) / (k-1), k]]];",
				"Table[A[n, d-n], {d, 0, 7}, {n, 0, d}] // Flatten (* _Jean-François Alcover_, Jan 24 2017, after _Alois P. Heinz_ *)"
			],
			"xref": [
				"Columns k=0-4 give: A019590(n+1), A000012, A056972, A273723, A273725.",
				"Main diagonal gives A273729.",
				"Cf. A273693."
			],
			"keyword": "nonn,tabl",
			"offset": "0,13",
			"author": "_Alois P. Heinz_, May 28 2016",
			"references": 6,
			"revision": 22,
			"time": "2022-01-02T09:47:40-05:00",
			"created": "2016-05-28T11:55:32-04:00"
		}
	]
}