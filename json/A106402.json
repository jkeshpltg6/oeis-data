{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A106402",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 106402,
			"data": "1,3,9,13,24,27,50,51,81,72,120,117,170,150,216,205,288,243,362,312,450,360,528,459,601,510,729,650,840,648,962,819,1080,864,1200,1053,1370,1086,1530,1224,1680,1350,1850,1560,1944,1584,2208,1845,2451,1803,2592",
			"name": "Expansion of eta(q^3)^9 / eta(q)^3 in powers of q.",
			"comment": [
				"Cubic AGM theta functions: a(q) (see A004016), b(q) (A005928), c(q) (A005882).",
				"Number 3 of the 74 eta-quotients listed in Table I of Martin (1996).",
				"a(n+1) is the number of partition triples of n where each partition is 3-core (see Theorem 3.1 of Wang link)."
			],
			"reference": [
				"G. E. Andrews, B. C. Berndt, Ramanujan's lost notebook, Part I, Springer, New York, 2005, MR2135178 (2005m:11001) See p. 314, Equ. (14.2.14)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A106402/b106402.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (terms 1..1000 from Vaclav Kotesovec)",
				"J. M. Borwein and P. B. Borwein, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-1991-1010408-0\"\u003eA cubic counterpart of Jacobi's identity and the AGM\u003c/a\u003e, Trans. Amer. Math. Soc., 323 (1991), no. 2, 691-701. MR1010408 (91e:33012) see page 697.",
				"Y. Martin, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-96-01743-6\"\u003eMultiplicative eta-quotients\u003c/a\u003e, Trans. Amer. Math. Soc. 348 (1996), no. 12, 4825-4856, see page 4852 Table I.",
				"H Movasati, Y Nikdelan, \u003ca href=\"http://arxiv.org/abs/1603.09411\"\u003eGauss-Manin Connection in Disguise: Dwork Family\u003c/a\u003e, arXiv preprint arXiv:1603.09411, 2016.",
				"Michael Somos, \u003ca href=\"/A030203/a030203.txt\"\u003eIndex to Yves Martin's list of 74 multiplicative eta-quotients and their A-numbers\u003c/a\u003e",
				"Liuquan Wang, \u003ca href=\"http://arxiv.org/abs/1507.03099\"\u003eExplicit Formulas for Partition Pairs and Triples with 3-Cores\u003c/a\u003e, arXiv:1507.03099 [math.NT], 2015."
			],
			"formula": [
				"Expansion of (c(q) / 3)^3 in powers of q where c(q) is a cubic AGM theta function.",
				"Euler transform of period 3 sequence [ 3, 3, -6, ...].",
				"G.f. A(x) satisfies 0 = f(A(x), A(x^2), A(x^4)) where f(u, v, w) = v^3 + 6*u*v*w + 8*u*w^2 - u^2*w.",
				"G.f.: Sum_{k\u003e0} k^2 * x^k / (1 + x^k + x^(2*k)) = x * Product_{k\u003e0} (1 - x^(3*k))^9 / (1 - x^k)^3.",
				"a(n) is multiplicative and a(p^e) = ((p^2)^(e+1) - u^(e+1)) / (p^2 - u) where u = 0, 1, -1 when p == 0, 1, 2 (mod 3). - _Michael Somos_, Oct 19 2005",
				"G.f. is a period 1 Fourier series which satisfies f(-1 / (3 t)) = 27^(-1/2) (t/i)^3 g(t) where q = exp(2 Pi i t) and g() is the g.f. for A109041.",
				"Convolution cube of A033687.",
				"a(3*n) = 9 * a(n). a(3*n + 1) = A231947(n). -  _Michael Somos_, May 18 2015",
				"Convolution square is A198958. - _Michael Somos_, Dec 26 2015"
			],
			"example": [
				"G.f. = q + 3*q^2 + 9*q^3 + 13*q^4 + 24*q^5 + 27*q^6 + 50*q^7 + 51*q^8 + ..."
			],
			"mathematica": [
				"a[ n_] := If[ n \u003c 1, 0, DivisorSum[ n, #^2 KroneckerSymbol[ n/#, 3] \u0026]]; (* _Michael Somos_, Jul 19 2012 *)",
				"a[ n_] := SeriesCoefficient[ q (QPochhammer[ q^3]^3 / QPochhammer[ q])^3, {q, 0, n}]; (* _Michael Somos_, Jul 19 2012 *)",
				"nmax = 40; Rest[CoefficientList[Series[x * Product[(1 - x^(3*k))^9 / (1 - x^k)^3, {k, 1, nmax}], {x, 0, nmax}], x]] (* _Vaclav Kotesovec_, Sep 07 2015 *)"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c1, 0, n--; A = x * O(x^n); polcoeff( eta(x^3 + A)^9 / eta(x + A)^3, n))};",
				"(PARI) {a(n) = if( n\u003c1, 0, sumdiv( n, d, d^2 * kronecker( n/d, 3)))};",
				"(PARI) {a(n) = my(A, p, e, u); if( n\u003c1, 0, A = factor(n); prod( k=1, matsize(A)[1], [p, e] = A[k, ]; u = kronecker(-3, p); ((p^2)^(e+1) - u^(e+1)) / (p^2 - u)))};",
				"(PARI) a(n) = sumdiv(n, d, ((d % 3) == 1)*(n/d)^2) - sumdiv(n, d, ((d % 3)== 2)*(n/d)^2); \\\\ _Michel Marcus_, Jul 14 2015",
				"(MAGMA) A := Basis( ModularForms( Gamma1(3), 3), 52); A[2]; /* _Michael Somos_, May 18 2015 */"
			],
			"xref": [
				"Cf. A033687, A109041, A198958, A231947."
			],
			"keyword": "nonn,mult",
			"offset": "1,2",
			"author": "_Michael Somos_, May 02 2005",
			"references": 6,
			"revision": 55,
			"time": "2018-11-22T21:50:11-05:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}