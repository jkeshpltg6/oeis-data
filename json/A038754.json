{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A038754",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 38754,
			"data": "1,2,3,6,9,18,27,54,81,162,243,486,729,1458,2187,4374,6561,13122,19683,39366,59049,118098,177147,354294,531441,1062882,1594323,3188646,4782969,9565938,14348907,28697814,43046721,86093442,129140163,258280326,387420489",
			"name": "a(2n) = 3^n, a(2n+1) = 2*3^n.",
			"comment": [
				"In general, for the recurrence a(n)=a(n-1)*a(n-2)/a(n-3), all terms are integers iff a(0) divides a(2) and first three terms are positive integers, since a(2n+k)=a(k)*(a(2)/a(0))^n for all nonnegative integers n and k.",
				"a(n) = A140740(n+2,2). - _Reinhard Zumkeller_, May 26 2008",
				"Equals eigensequence of triangle A070909; (1, 1, 2, 3, 6, 9, 18, ...) shifts to the left with multiplication by triangle A070909. - _Gary W. Adamson_, May 15 2010",
				"The a(n) represent all paths of length (n+1), n\u003e=0, starting at the initial node on the path graph P_5, see the second Maple program. - _Johannes W. Meijer_, May 29 2010",
				"a(n) is the difference between numbers of multiple of 3 evil (A001969) and odious (A000069) numbers in interval [0, 2^(n+1)). - _Vladimir Shevelev_, May 16 2012",
				"A \"half-geometric progression\": to obtain a term (beginning with the third one) we multiply the before previous one by 3. - _Vladimir Shevelev_, May 21 2012",
				"Pisano period lengths: 1, 2, 1, 4, 8, 2, 12, 4, 1, 8, 10, 4, 6, 12, 8, 8, 32, 2, 36, 8, ... . - _R. J. Mathar_, Aug 10 2012",
				"Sum_(n\u003e=0) 1/a(n) = 9/4. - _Alexander R. Povolotsky_, Aug 24 2012",
				"Numbers n such that the n-th cyclotomic polynomial has a root mod 3. - _Eric M. Schmidt_, Jul 31 2013",
				"Range of row n of the circular Pascal array of order 6. - _Shaun V. Ault_, Jun 05 2014",
				"a(2*n) = A000244(n), a(2*n+1) = A008776(n). - _Reinhard Zumkeller_, Oct 19 2015"
			],
			"link": [
				"T. D. Noe and Indranil Ghosh, \u003ca href=\"/A038754/b038754.txt\"\u003eTable of n, a(n) for n = 0..1500\u003c/a\u003e, (first 401 terms from T. D. Noe)",
				"S. V. Ault and C. Kicey, \u003ca href=\"http://dx.doi.org/10.1016/j.disc.2014.05.020\"\u003eCounting paths in corridors using circular Pascal arrays\u003c/a\u003e, Discrete Mathematics (2014).",
				"Paul Barry, \u003ca href=\"https://arxiv.org/abs/1803.06408\"\u003eThree Études on a sequence transformation pipeline\u003c/a\u003e, arXiv:1803.06408 [math.CO], 2018.",
				"Nachum Dershowitz, \u003ca href=\"https://arxiv.org/abs/2006.06516\"\u003eBetween Broadway and the Hudson\u003c/a\u003e, arXiv:2006.06516 [math.CO], 2020.",
				"Richard L. Ollerton and Anthony G. Shannon, \u003ca href=\"http://www.fq.math.ca/Scanned/36-2/ollerton.pdf\"\u003eSome properties of generalized Pascal squares and triangles\u003c/a\u003e, Fib. Q., 36 (1998), 98-109. See pages 106-7.",
				"Vladimir Shevelev, \u003ca href=\"http://arxiv.org/abs/0710.3177\"\u003eOn monotonic strengthening of Newman-like phenomenon on (2m+1)-multiples in base 2m\u003c/a\u003e, arXiv:0710.3177 [math.NT], 2007.",
				"M. B. Wells, \u003ca href=\"/A000170/a000170.pdf\"\u003eElements of Combinatorial Computing\u003c/a\u003e, Pergamon, Oxford, 1971. [Annotated scanned copy of pages 237-240]",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,3)."
			],
			"formula": [
				"a(n) = a(n-1)*a(n-2)/a(n-3) with a(0)=1, a(1)=2, a(2)=3.",
				"a(2n) = (3/2)*a(2n-1) = 3^n, a(2n+1) = 2*a(2n) = 2*3^n.",
				"a(1)=1, a(n)=2*a(n-1) if a(n-1) is odd, or a(n)=3/2*a(n-1) if a(n-1) is even. - _Benoit Cloitre_, Apr 27 2003",
				"a(n) = (1/6)*(5-(-1)^n)*3^floor(n/2); a(2n) = a(2n-1) + a(2n-2) + a(2n-3); a(2n+1) = a(2n) + a(2n-1). - _Benoit Cloitre_, Apr 27 2003",
				"G.f.: (1+2x)/(1-3x^2). - _Paul Barry_, Aug 25 2003",
				"a(n) = (1 + n mod 2) * 3^floor(n/2). a(n) = A087503(n) - A087503(n-1). - _Reinhard Zumkeller_, Sep 11 2003",
				"a(n) = sqrt(3)(2+sqrt(3))(sqrt(3))^n/6-sqrt(3)(2-sqrt(3))(-sqrt(3))^n/6. - _Paul Barry_, Sep 16 2003",
				"a(n+1) = a(n) + a(n - n mod 2). - _Reinhard Zumkeller_, May 26 2008",
				"If p(i) = Fibonacci(i-3) and if A is the Hessenberg matrix of order n defined by A(i,j) = p(j-i+1), (i\u003c=j), A(i,j)=-1, (i=j+1), and A(i,j)=0 otherwise. Then, for n\u003e=1, a(n-1) = (-1)^n det A. - _Milan Janjic_, May 08 2010",
				"a(n) = A182751(n) for n \u003e= 2. - _Jaroslav Krizek_, Nov 27 2010",
				"a(n) = Sum_{i=0..2^(n+1), i==0 mod 3} (-1)^A000120(i). - _Vladimir Shevelev_, May 16 2012",
				"a(0)=1, a(1)=2, for n\u003e=3, a(n)=3*a(n-2). - _Vladimir Shevelev_, May 21 2012",
				"a(n) = sqrt(3*a(n-1)^2 + (-3)^(n-1)). - _Richard R. Forberg_, Sep 04 2013",
				"a(n) = 2^((1-(-1)^n)/2)*3^((2*n-1+(-1)^n)/4). - _Luce ETIENNE_, Aug 11 2014",
				"For n \u003e 0: a(n+1) = a(n) + if a(n) odd then min{a(n), a(n-1)} else max{a(n), a(n-1)}, see also A128588. - _Reinhard Zumkeller_, Oct 19 2015"
			],
			"example": [
				"In the interval [0,2^5) we have 11 multiples of 3 numbers, from which 10 are evil and only one (21) is odious. Thus a(4) = 10 - 1 = 9. - _Vladimir Shevelev_, May 16 2012"
			],
			"maple": [
				"a[0]:=0:a[1]:=1:for n from 2 to 50 do a[n]:=3*a[n-2]+2 od: seq(a[n]+1, n=0..34); # _Zerinvary Lajos_, Mar 20 2008",
				"with(GraphTheory): P:=5: G:=PathGraph(P): A:= AdjacencyMatrix(G): nmax:=35; for n from 1 to nmax do B(n):=A^n; a(n):=add(B(n)[1,k],k=1..P) od: seq(a(n),n=1..nmax); # _Johannes W. Meijer_, May 29 2010"
			],
			"mathematica": [
				"LinearRecurrence[{0,3},{1,2},40] (* _Harvey P. Dale_, Jan 26 2014 *)",
				"CoefficientList[Series[(1 + 2 x) / (1 - 3 x^2), {x, 0, 40}], x] (* _Vincenzo Librandi_, Aug 18 2016 *)",
				"Module[{nn=20,c},c=3^Range[0,nn];Riffle[c,2c]] (* _Harvey P. Dale_, Aug 21 2021 *)"
			],
			"program": [
				"(PARI) a(n)=(1/6)*(5-(-1)^n)*3^floor(n/2)",
				"(PARI) a(n)=3^(n\u003e\u003e1)\u003c\u003cbittest(n,0)",
				"(Haskell)",
				"import Data.List (transpose)",
				"a038754 n = a038754_list !! n",
				"a038754_list = concat $ transpose [a000244_list, a008776_list]",
				"-- _Reinhard Zumkeller_, Oct 19 2015",
				"(MAGMA) [n le 2 select n else 3*Self(n-2): n in [1..40]]; // _Vincenzo Librandi_, Aug 18 2016"
			],
			"xref": [
				"Cf. Somos sequences A006720, A006721, A006722, A006723.",
				"a(n) = A094718(5, n).",
				"Cf. A000079, A133464, A140730, A037124, A070909, A048328, A068911, A124302, A000045, A038754, A028495, A030436, A061551, A178381, A182751-A182757.",
				"Cf. A000244, A008776, A128588."
			],
			"keyword": "easy,nice,nonn",
			"offset": "0,2",
			"author": "_Henry Bottomley_, May 03 2000",
			"references": 83,
			"revision": 114,
			"time": "2021-08-21T10:53:17-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}