{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A109822",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 109822,
			"data": "1,1,2,1,4,6,1,7,18,24,1,11,46,96,120,1,16,101,326,600,720,1,22,197,932,2556,4320,5040,1,29,351,2311,9080,22212,35280,40320,1,37,583,5119,27568,94852,212976,322560,362880,1,46,916,10366,73639,342964,1066644",
			"name": "Triangle read by rows: T(n,1)=1, T(n,k) = T(n-1,k) + (n-1)T(n-1, k-1) for 1 \u003c= k \u003c= n.",
			"comment": [
				"T(n,n) = n!. Sum of row n is the signless Stirling number of the first kind s(n,2)(A000254). T(n,k) = A096747(n,k) for 1 \u003c= k \u003c= n."
			],
			"formula": [
				"T(n, k) = Sum_{i=0..k-1} |stirling1(n, n-i)| for 1 \u003c= k \u003c= n.",
				"From _Peter Bala_, Jul 08 2012: (Start)",
				"E.g.f.: x/(1-x)*{1/(1-x*z)^(1/x) - 1/(1-x*z)} = x*z + (x + 2*x^2)*z^2/2! + (x + 4*x^2 + 6*x^3)*z^3/3! + ... Cf. the e.g.f. of A059518.",
				"(End)"
			],
			"example": [
				"T(5,3) = 46 because 18 + 4*7 = 46.",
				"Triangle begins:",
				"Row 1:                    1",
				"Row 2:                 1     2",
				"Row 3:              1     4     6",
				"Row 4:           1     7    18    24",
				"Row 5:        1    11    46    96   120",
				"Row 6:     1    16   101   326   600   720",
				"Row 7:  1    22   197   932  2556  4320  5040"
			],
			"maple": [
				"with(combinat): T:=(n,k)-\u003eadd(abs(stirling1(n,n-i)),i=0..k-1): for n from 1 to 11 do seq(T(n,k),k=1..n) od; # yields sequence in triangular form T:=proc(n,k) if k=1 then 1 elif k=n then n! else T(n-1,k)+(n-1)*T(n-1,k-1) fi end: for n from 1 to 11 do seq(T(n,k),k=1..n) od; # yields sequence in triangular form. - _Emeric Deutsch_, Jul 03 2005",
				"A109822_row := proc(n) local k,i;",
				"add(add(abs(combinat[stirling1](n, n-i)), i=0..k)*x^(n-k-1),k=0..n-1);",
				"seq(coeff(%,x,n-k),k=1..n) end:",
				"seq(print(A109822_row(n)),n=1..7); # _Peter Luschny_, Sep 18 2011"
			],
			"mathematica": [
				"Table[Sum[Abs@ StirlingS1[n, n - i], {i, 0, k - 1}], {n, 10}, {k, n}] // Flatten (* _Michael De Vlieger_, Aug 17 2017 *)"
			],
			"xref": [
				"Cf. A000254, A049444, A059518, A096747, A137650."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Emeric Deutsch_, Jul 03 2005",
			"references": 2,
			"revision": 15,
			"time": "2017-08-17T22:32:27-04:00",
			"created": "2005-07-19T03:00:00-04:00"
		}
	]
}