{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A036275",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 36275,
			"data": "0,0,3,0,0,6,142857,0,1,0,90,3,769230,714285,6,0,5882352941176470,5,526315789473684210,0,476190,45,4347826086956521739130,6,0,384615,370,571428,3448275862068965517241379310,3,322580645161290,0,30,2941176470588235,285714,7",
			"name": "The periodic part of the decimal expansion of 1/n. Any initial 0's are to be placed at end of cycle.",
			"comment": [
				"a(n) = 0 iff n = 2^i*5^j (A003592). - _Jon Perry_, Nov 19 2014",
				"a(n) = n iff n = 3 or 6 (see De Koninck \u0026 Mercier reference). - _Bernard Schott_, Dec 02 2020"
			],
			"reference": [
				"Jean-Marie De Koninck \u0026 Armel Mercier, 1001 Problèmes en Théorie Classique des Nombres, Problème 347 pp. 50 and 205, Ellipses, Paris, 2004."
			],
			"link": [
				"Philippe Guglielmetti, \u003ca href=\"/A036275/b036275.txt\"\u003eTable of n, a(n) for n = 1..1001\u003c/a\u003e (first 500 terms from T. D. Noe)",
				"\u003ca href=\"/index/1#1overn\"\u003eIndex entries for sequences related to decimal expansion of 1/n\u003c/a\u003e"
			],
			"example": [
				"1/28 = .03571428571428571428571428571428571428571... and digit-cycle is 571428, so a(28)=571428."
			],
			"maple": [
				"isCycl := proc(n) local ifa,i ; if n \u003c= 2 then RETURN(false) ; fi ; ifa := ifactors(n)[2] ; for i from 1 to nops(ifa) do if op(1,op(i,ifa)) \u003c\u003e 2 and op(1,op(i,ifa)) \u003c\u003e 5 then RETURN(true) ; fi ; od ; RETURN(false) ; end: A036275 := proc(n) local ifa,sh,lpow,mpow,r ; if not isCycl(n) then RETURN(0) ; else lpow:=1 ; while true do for mpow from lpow-1 to 0 by -1 do if (10^lpow-10^mpow) mod n =0 then r := (10^lpow-10^mpow)/n ; r := r mod (10^(lpow-mpow)-1) ; while r*10 \u003c 10^(lpow-mpow) do r := 10*r ; od ; RETURN(r) ; fi ; od ; lpow := lpow+1 ; od ; fi ; end: for n from 1 to 60 do printf(\"%d %d \",n,A036275(n)) ; od ; # _R. J. Mathar_, Oct 19 2006"
			],
			"mathematica": [
				"fc[n_]:=Block[{q=RealDigits[1/n][[1,-1]]},If[IntegerQ[q],0,While[First[q]==0,q=RotateLeft[q]];FromDigits[q]]];",
				"Table[fc[n],{n,36}] (* _Ray Chandler_, Nov 19 2014, corrected Jun 27 2017 *)",
				"Table[FromDigits[FindTransientRepeat[RealDigits[1/n,10,120][[1]],3] [[2]]],{n,40}] (* Requires Mathematica version 10 or later *) (* _Harvey P. Dale_, Mar 12 2019 *)"
			],
			"xref": [
				"Cf. A007732, A051626, A002371, A048595, A006883, A007498, A007615, A040017, A051627.",
				"See also A060282, A060283, A060251.",
				"A051628 is length of preamble."
			],
			"keyword": "base,nonn,easy,nice",
			"offset": "1,3",
			"author": "_Floor van Lamoen_",
			"ext": [
				"Corrected and extended by _N. J. A. Sloane_",
				"Corrected a(92), a(208), a(248), a(328), a(352) and a(488) which missed a trailing zero (see the table). - _Philippe Guglielmetti_, Jun 20 2017"
			],
			"references": 19,
			"revision": 63,
			"time": "2021-02-12T18:15:57-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}