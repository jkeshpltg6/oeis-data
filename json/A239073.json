{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A239073",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 239073,
			"data": "1,2,10,48,208,874,3600,14622,58976,236824,949094,3800126,15207812,60846154,243414326,973716670,3894985588,15580180122,62321195992,249285735518,997144844044,3988583179554,15954340324098,63817376508852,255269536476262",
			"name": "Given a circle of radius R into which small circles of radius R/2^n are packed in a \"hexagonal pattern\" (see Comments), a(n) is the maximum number of small circles.",
			"comment": [
				"The construction rule is: (1) Start with a unit circle (big circle). (2) Pack circles at radius 1/2^n (small circles) on the diameter line of the big circle. (3) Maintain hexagonal packing pattern of small circles on the rows above and below that of the previous step. The number of small circles in any row is limited so that the circumference of the last small circle does not cross the circumference of the big circle (but is allowed to contact it). (4) Repeat process to the top and bottom rows.",
				"See illustration in links.",
				"From _Jon E. Schoenfield_, Mar 23 2014: (Start)",
				"There can never be exactly one small circle on the top row iMax (and, symmetrically, exactly one on the bottom row) that contacts the large circle, since the upper edge of that small circle would be at a radius of (iMax*sqrt(3)+1)/2^n from the center of the unit circle (which cannot equal 1 unless iMax=0, i.e., there are no rows above or below the center).",
				"The number of small circles on the top row seems to be on the order of 2^(n/2). It seems nearly certain that, for n\u003e0, there is never exactly one small circle on the top row. (End)",
				"After _Wolfdieter Lang_'s suggestions of an alternative definition, I found later that a(n) = A053417(2^n-1), n \u003e= 1."
			],
			"link": [
				"Kival Ngaokrajang, \u003ca href=\"/A239073/a239073.pdf\"\u003eIllustration of initial terms\u003c/a\u003e",
				"Kival Ngaokrajang, \u003ca href=\"/A239073/a239073.txt\"\u003eSmall Basic program\u003c/a\u003e"
			],
			"formula": [
				"a(n) = 2^n + 2 * Sum_{i odd, \u003c= iMax} (2*floor(sqrt(x - 3 * i^2) / 2) + 1) + 2 * Sum_{i even, 0 \u003c i \u003c= iMax} 2*floor(sqrt(x - 3 * i^2) / 2 + 1/2) where x = (2^n-1)^2 and iMax = floor(sqrt(x/3)). - _Jon E. Schoenfield_, Mar 17, Mar 23 2014"
			],
			"program": [
				"(Small Basic)",
				"See links.",
				"(MAGMA)",
				"for n in [0..25] do",
				"   x:=(2^n - 1)^2;",
				"   c:=2^n; // for row i=0",
				"   for i in [1..Isqrt(x div 3)] do",
				"      t:=x-3*i*i;",
				"      if IsOdd(i) then",
				"         c:=c+2*(2*Floor(Sqrt(t)/2)+1);",
				"      else",
				"         c:=c+2*(2*Floor(Sqrt(t)/2+1/2));",
				"      end if;",
				"   end for;",
				"   n, c;",
				"end for; // _Jon E. Schoenfield_, Mar 17 2014"
			],
			"xref": [
				"Cf. A239074, A239206, A053417, A053416."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Kival Ngaokrajang_, Mar 10 2014",
			"ext": [
				"a(20) - a(24) and corrections to a(15), a(17), and a(18) from _Jon E. Schoenfield_, Mar 17 2014"
			],
			"references": 4,
			"revision": 32,
			"time": "2014-04-08T12:50:36-04:00",
			"created": "2014-04-06T08:34:00-04:00"
		}
	]
}