{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A103447",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 103447,
			"data": "1,1,1,1,-1,1,1,-1,-1,1,1,0,1,0,1,1,-1,1,1,-1,1,1,1,1,0,1,1,1,1,-1,1,1,1,1,-1,1,1,0,0,0,-1,0,0,0,1,1,0,0,0,0,0,0,0,0,1,1,1,0,0,1,0,1,0,0,1,1,1,-1,1,-1,1,1,1,1,-1,1,-1,1,1,0,-1,0,0,0,0,0,0,0,-1,0,1,1,-1,-1,-1,-1,0,0,0,0,-1,-1,-1,-1,1",
			"name": "Triangle read by rows: T(n,k) = Moebius(binomial(n,k)) (0 \u003c= k \u003c= n).",
			"comment": [
				"T(2*n, n) = 0 for all n except n=0, 1, 2 and 4 (Granville and Ramare)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A103447/b103447.txt\"\u003eRows n = 0..100 of the triangle, flattened\u003c/a\u003e",
				"A. Granville and O. Ramaré, \u003ca href=\"http://www.dms.umontreal.ca/~andrew/PDF/ramare.pdf\"\u003eExplicit bounds on exponential sums and the scarcity of squarefree binomial coefficients\u003c/a\u003e, Mathematika 43 (1996), 73-107, \u003ca href=\"http://dx.doi.org/10.1112/S0025579300011608\"\u003e[DOI]\u003c/a\u003e."
			],
			"formula": [
				"T(n, k) = Moebius(binomial(n, k)) (0 \u003c= k \u003c= n).",
				"T(n, k) = A008683(A007318(n, k)).",
				"Sum_{k=0..n} T(n, k) = A103448(n)."
			],
			"example": [
				"T(3,2)=-1 because binomial(3,2)=3 and Moebius(3)=-1.",
				"Triangle begins:",
				"  1;",
				"  1,  1;",
				"  1, -1,  1;",
				"  1, -1, -1,  1;",
				"  1,  0,  1,  0,  1;",
				"  1, -1,  1,  1, -1,  1;"
			],
			"maple": [
				"with(numtheory):T:=proc(n,k) if k\u003c=n then mobius(binomial(n,k)) else 0 fi end: for n from 0 to 15 do seq(T(n,k),k=0..n) od; # yields sequence in triangular form"
			],
			"mathematica": [
				"T[n_, k_]:= MoebiusMu[Binomial[n, k]]; Table[T[n, k], {n,0,15}, {k,0,n}]//Flatten (* _G. C. Greubel_, Jun 16 2021 *)"
			],
			"program": [
				"(PARI) T(n,k) = moebius(binomial(n,k))",
				"for(n=0, 15, for(k=0, n, print1(T(n,k)\", \"))) \\\\ _Charles R Greathouse IV_, Nov 03 2014",
				"(MAGMA) [MoebiusMu(Binomial(n, k)): k in [0..n], n in [0..15]]; // _G. C. Greubel_, Jun 16 2021",
				"(Sage)",
				"def T(n, k): return moebius(binomial(n, k))",
				"flatten([[T(n, k) for k in (0..n)] for n in (0..15)]) # _G. C. Greubel_, Jun 16 2021"
			],
			"xref": [
				"Cf. A007318, A008683, A103448 (row sums), A103449."
			],
			"keyword": "sign,tabl",
			"offset": "0,1",
			"author": "_Emeric Deutsch_, Feb 06 2005",
			"references": 5,
			"revision": 19,
			"time": "2021-06-18T07:25:01-04:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}