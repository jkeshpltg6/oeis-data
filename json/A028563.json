{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A028563",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 28563,
			"data": "0,8,18,30,44,60,78,98,120,144,170,198,228,260,294,330,368,408,450,494,540,588,638,690,744,800,858,918,980,1044,1110,1178,1248,1320,1394,1470,1548,1628,1710,1794,1880,1968,2058,2150,2244,2340,2438,2538,2640,2744,2850,2958,3068,3180,3294",
			"name": "a(n) = n*(n+7).",
			"comment": [
				"a(m), for m \u003e= 1, are the only positive integer values of t for which the Binet-de Moivre formula of the recurrence b(n) = 7*b(n-1)+t*b(n-2) has a square root whose radicand is a square. In particular, sqrt(7^2+4*t) is a positive integer since 7^2+4*t = 7^2+4*a(m) = (2*m+7)^2. Thus the characteristic roots are r1 = 7+m and r2 = -m. - _Felix P. Muga II_, Mar 28 2014 (edited - _Wolfdieter Lang_, Apr 17 2014)"
			],
			"link": [
				"Vincenzo Librandi, \u003ca href=\"/A028563/b028563.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Patrick De Geest, \u003ca href=\"http://www.worldofnumbers.com/consemor.htm\"\u003ePalindromic Quasipronics of the form n(n+x)\u003c/a\u003e.",
				"Felix P. Muga II, \u003ca href=\"https://www.researchgate.net/publication/267327689_Extending_the_Golden_Ratio_and_the_Binet-de_Moivre_Formula\"\u003eExtending the Golden Ratio and the Binet-de Moivre Formula\u003c/a\u003e, Preprint on ResearchGate, March 2014.",
				"\u003ca href=\"/index/Rec#order_03\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (3,-3,1)."
			],
			"formula": [
				"Equals 2*A055999. - _Zerinvary Lajos_, Feb 12 2007",
				"a(n) = 2*n+a(n-1)+6. - _Vincenzo Librandi_, Aug 05 2010",
				"Sum_{n\u003e=1} 1/a(n) = 363/980 = 0.37040816... - _R. J. Mathar_, Mar 22 2011",
				"G.f.: 2*x*(4-3*x)/(1-x)^3. - _Colin Barker_, Feb 17 2012",
				"Sum_{n\u003e=1} (-1)^(n+1)/a(n) = 2*log(2)/7 - 319/2940. - _Amiram Eldar_, Jan 15 2021"
			],
			"maple": [
				"A028563:=n-\u003en*(n + 7); seq(A028563(n), n=0..50); # _Wesley Ivan Hurt_, Mar 30 2014"
			],
			"mathematica": [
				"CoefficientList[Series[2 x (4 - 3 x)/(1 - x)^3, {x, 0, 50}], x] (* _Vincenzo Librandi_, Oct 18 2013 *)",
				"LinearRecurrence[{3,-3,1},{0,8,18},60] (* _Harvey P. Dale_, Oct 07 2015 *)"
			],
			"program": [
				"(MAGMA) [n*(n+7): n in [0..60]]; // _Vincenzo Librandi_, Oct 18 2013",
				"(PARI) a(n)=n*(n+7) \\\\ _Charles R Greathouse IV_, Jun 17 2017"
			],
			"xref": [
				"Cf. A002522, A055999."
			],
			"keyword": "nonn,easy",
			"offset": "0,2",
			"author": "_Patrick De Geest_",
			"references": 18,
			"revision": 51,
			"time": "2021-01-15T07:39:38-05:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}