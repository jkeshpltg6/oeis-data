{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A342984",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 342984,
			"data": "1,1,1,0,2,0,0,3,3,0,0,4,20,4,0,0,5,75,75,5,0,0,6,210,604,210,6,0,0,7,490,3150,3150,490,7,0,0,8,1008,12480,27556,12480,1008,8,0,0,9,1890,40788,170793,170793,40788,1890,9,0,0,10,3300,115500,829920,1565844,829920,115500,3300,10,0",
			"name": "Triangle read by rows: T(n,k) is the number of nonseparable tree-rooted planar maps with n edges and k faces, n \u003e= 0, k = 1..n+1.",
			"comment": [
				"The number of vertices is n + 2 - k.",
				"A tree-rooted planar map is a planar map with a distinguished spanning tree.",
				"For k \u003e= 2, column k is a polynomial of degree 4*(k-2)+1."
			],
			"link": [
				"Andrew Howroyd, \u003ca href=\"/A342984/b342984.txt\"\u003eTable of n, a(n) for n = 0..1325\u003c/a\u003e (rows 0..50)",
				"T. R. S. Walsh and A. B. Lehman, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(75)90050-7\"\u003eCounting rooted maps by genus. III: Nonseparable maps\u003c/a\u003e, J. Combinatorial Theory Ser. B 18 (1975), 222-259, Table IVa."
			],
			"formula": [
				"T(n,n+2-k) = T(n,k).",
				"G.f. A(x,y) satisfies F(x,y) = A(x*F(x,y)^2,y) where F(x,y) is the g.f. of A342982."
			],
			"example": [
				"Triangle begins:",
				"  1;",
				"  1, 1;",
				"  0, 2,    0;",
				"  0, 3,    3,     0;",
				"  0, 4,   20,     4,     0;",
				"  0, 5,   75,    75,     5,     0;",
				"  0, 6,  210,   604,   210,     6,    0;",
				"  0, 7,  490,  3150,  3150,   490,    7, 0;",
				"  0, 8, 1008, 12480, 27556, 12480, 1008, 8, 0;",
				"  ..."
			],
			"program": [
				"(PARI) \\\\ here F(n,y) gives A342982 as g.f.",
				"F(n,y)={sum(n=0, n, x^n*sum(i=0, n, my(j=n-i); y^i*(2*i+2*j)!/(i!*(i+1)!*j!*(j+1)!))) + O(x*x^n)}",
				"H(n)={my(g=F(n,y), v=Vec(subst(g, x, serreverse(x*g^2)))); vector(#v, n, Vecrev(v[n], n))}",
				"{ my(T=H(8)); for(n=1, #T, print(T[n])) }"
			],
			"xref": [
				"Columns (and diagonals) 3..5 are A006411, A006412, A006413.",
				"Row sums are A004304.",
				"Cf. A342982, A342985, A342987."
			],
			"keyword": "nonn,tabl,changed",
			"offset": "0,5",
			"author": "_Andrew Howroyd_, Apr 03 2021",
			"references": 7,
			"revision": 12,
			"time": "2022-01-02T16:09:36-05:00",
			"created": "2021-04-03T19:01:41-04:00"
		}
	]
}