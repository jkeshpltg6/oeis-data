{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A101330",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 101330,
			"data": "3,5,5,8,8,8,11,13,13,11,13,18,21,18,13,16,21,29,29,21,16,18,26,34,40,34,26,18,21,29,42,47,47,42,29,21,24,34,47,58,55,58,47,34,24,26,39,55,65,68,68,65,55,39,26,29,42,63,76,76,84,76,76,63,42,29,32,47",
			"name": "Array read by antidiagonals: T(n,k) = Knuth's Fibonacci (or circle) product of n and k (\"n o k\"), n \u003e= 1, k \u003e= 1.",
			"comment": [
				"Let n = Sum_{i \u003e= 2} eps(i) Fib_i and k = Sum_{j \u003e= 2} eps(j) Fib_j be the Zeckendorf expansions of n and k, respectively (cf. A035517, A014417). (The eps(i) are 0 or 1 and no two consecutive eps(i) are both 1.) Then the Fibonacci (or circle) product of n and k is n o k = Sum_{i,j} eps(i)*eps(j) Fib_{i+j} (= T(n,k)).",
				"The Zeckendorf expansion can be written n=Sum_{i=1..k} F(a_i), where a_{i+1} \u003e= a_i + 2. In this formulation, the product becomes: if n = Sum_{i=1..k} F(a_i) and m = Sum_{j=1..l} F(b_j) then n o m = Sum_{i=1..k} Sum_{j=1..l} F(a_i + b_j).",
				"Knuth shows that this multiplication is associative. This is not true if we change the product to n x k = Sum_{i,j} eps(i)*eps(j) Fib_{i+j-2}, see A101646. Of course 1 is not a multiplicative identity here, whereas it is in A101646.",
				"The papers by Arnoux, Grabner et al. and Messaoudi discuss this sequence and generalizations."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A101330/b101330.txt\"\u003eRows n=1..100 of array, flattened\u003c/a\u003e",
				"P. Arnoux, \u003ca href=\"http://dx.doi.org/10.1016/0893-9659(89)90078-5\"\u003eSome remarks about Fibonacci multiplication\u003c/a\u003e, Appl. Math. Lett. 2 (1989), 319-320.",
				"P. Arnoux, \u003ca href=\"/A101858/a101858.pdf\"\u003eSome remarks about Fibonacci multiplication\u003c/a\u003e, Appl. Math. Lett. 2 (No. 4, 1989), 319-320. [Annotated scanned copy]",
				"Vincent Canterini and Anne Siegel, \u003ca href=\"http://dx.doi.org/10.1090/S0002-9947-01-02797-0\"\u003eGeometric representation of substitutions of Pisot type\u003c/a\u003e, Trans. Amer. Math. Soc. 353 (2001), 5121-5144.",
				"P. Grabner et al., \u003ca href=\"http://dx.doi.org/10.1016/0893-9659(94)90017-5\"\u003eAssociativity of recurrence multiplication\u003c/a\u003e, Appl. Math. Lett. 7 (1994), 85-90.",
				"D. E. Knuth, \u003ca href=\"http://dx.doi.org/10.1016/0893-9659(88)90176-0\"\u003eFibonacci multiplication\u003c/a\u003e, Appl. Math. Lett. 1 (1988), 57-60.",
				"W. F. Lunnon, \u003ca href=\"/A101330/a101330.txt\"\u003eProof of formula\u003c/a\u003e",
				"A. Messaoudi, \u003ca href=\"http://www.numdam.org/item?id=JTNB_1998__10_1_135_0\"\u003ePropriétés arithmétiques et dynamiques du fractal de Rauzy\u003c/a\u003e, Journal de théorie des nombres de Bordeaux, 10 no. 1 (1998), p. 135-162.",
				"A. Messaoudi, \u003ca href=\"http://almira.math.u-bordeaux.fr/jtnb/1998-1/messaoudi.ps\"\u003ePropriétés arithmétiques et dynamiques du fractal de Rauzy\u003c/a\u003e [alternative copy]",
				"A. Messaoudi, \u003ca href=\"http://dml.cz/handle/10338.dmlcz/136773\"\u003eGénéralisation de la multiplication de Fibonacci\u003c/a\u003e, Math. Slovaca, 50 (2) (2000), 135-148.",
				"A. Messaoudi, \u003ca href=\"http://dx.doi.org/10.1016/S0893-9659(02)00073-3\"\u003eTribonacci multiplication\u003c/a\u003e, Appl. Math. Lett. 15 (2002), 981-985."
			],
			"formula": [
				"x o y = 3 x y - x [(y+1)/phi^2] - y [(x+1)/phi^2]. For proof see link. - _Fred Lunnon_, May 19 2008"
			],
			"example": [
				"Array begins:",
				"   3   5   8  11   13   16   18   21   24 ...",
				"   5   8  13  18   21   26   29   34   39 ...",
				"   8  13  21  29   34   42   47   55   63 ...",
				"  11  18  29  40   47   58   65   76   87 ...",
				"  13  21  34  47   55   68   76   89  102 ...",
				"  16  26  42  58   68   84   94  110  126 ...",
				"  18  29  47  65   76   94  105  123  141 ...",
				"  21  34  55  76   89  110  123  144  165 ...",
				"  24  39  63  87  102  126  141  165  189 ...",
				"  ..........................................."
			],
			"mathematica": [
				"zeck[n_Integer] := Block[{k = Ceiling[ Log[ GoldenRatio, n*Sqrt[5]]], t = n, fr = {}}, While[k \u003e 1, If[t \u003e= Fibonacci[k], AppendTo[fr, 1]; t = t - Fibonacci[k], AppendTo[fr, 0]]; k-- ]; FromDigits[fr]]; kfp[n_, m_] := Block[{y = Reverse[ IntegerDigits[ zeck[ n]]], z = Reverse[ IntegerDigits[ zeck[ m]]]}, Sum[ y[[i]]*z[[j]]*Fibonacci[i + j + 2], {i, Length[y]}, {j, Length[z]}]]; (* _Robert G. Wilson v_, Feb 09 2005 *)",
				"Flatten[ Table[ kfp[i, n - i], {n, 2, 13}, {i, n - 1, 1, -1}]] (* _Robert G. Wilson v_, Feb 09 2005 *)"
			],
			"xref": [
				"See A101646 and A135090 for other versions.",
				"Cf. A035517, A014417. See A101385, A101633, A101858 for related definitions of product.",
				"Main diagonal is A101332. First row equals A026274. Second row is A101345. Third row is A101642."
			],
			"keyword": "nonn,tabl,easy,nice",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_, Jan 25 2005",
			"ext": [
				"More terms from _David Applegate_, Jan 26 2005"
			],
			"references": 10,
			"revision": 28,
			"time": "2017-12-03T17:02:52-05:00",
			"created": "2005-02-20T03:00:00-05:00"
		}
	]
}