{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A307446",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 307446,
			"data": "1,3,3,3,5,7,7,9,11,11,11,13,13,13,11,9,9,9,11,11,11,13,15,15,17,19,19,19,21,23,23,25,27,27,27,25,25,25,27,29,29,31,33,33,33,35,37,37,39,41,41,41,43,43,43,41,39,39,39,41,41,41,43,45,45,47,49,49",
			"name": "First coordinate of a contiguous path on the odd-numbered points of a square lattice leading to a distance-limited bijection with the even-numbered points of a square lattice rotated by Pi/6 around the common point (0,0). A307447 gives the second coordinate.",
			"comment": [
				"In a private communication in 2009, Klaus Nagel described a conjectured bijection between the grid points of two quadratic lattices G and H such that the second lattice H has an offset of 1/2 in both coordinate directions and is rotated by an angle of Pi/6 against the first lattice G. If all straight-line connections between points of G and H not exceeding the Euclidean distance d=sqrt(1/2) are drawn (see Nagel link), several types of connected components in the infinite bipartite graph of the vertices of G U H containing equal numbers of grid points from G and H arise.",
				"One of them is an \"octopus\" with 4 contiguous arms identical up to symmetry. To achieve a bijection between the points of G and H, the four arms must not be finite. Otherwise the central grid point of H, which is equidistant to its 4 closest neighbors in G, but can only be used once, would lead to a violation of the condition of equal number of points from G and H in any connected component.",
				"The coordinates (i,j) of the grid points of lattice G in one of the arms are provided in this sequence (i) and in A307447 (j). Instead of applying the offset of (1/2,1/2) between lattices G and H, all distances are scaled by a factor of 2. Only grid points with odd coordinates are used from G and grid points with even coordinates from H.",
				"The construction of the selected one arm proceeds as follows: Start with the edge (1,1)-\u003e(3,1) in G. Determine the point with even coordinates in H (rotated) that is closest to the midpoint (2,1) of the considered edge of G. After rotation by Pi/6 the coordinates of point (2,0) in H become (1.732051,1.000). A grid point from H after rotation is only accepted if its distances from both end points of the edge in G are less than sqrt(2). The distance from (1,1) is 0.732051 and from (3,1) 1.267949, both \u003c sqrt(2).",
				"To determine the continuation of the path, excluding U-turns, the rotated grid points of H closest to the midpoints of the 3 candidate edges starting at the end point of the edge selected in the previous step are subjected to the tests of not exceeding distance sqrt(2) from both ends of the respective candidate edge of G. If more than one of the 3 candidate edges passes the checks, the one with least distance of its midpoint to the closest grid point in H (rotated) is selected.",
				"A visualization of the selection process is provided in the plot \"Construction of A307446 and A307447\", see link. The selected path on the grid points of G is marked in red. Grid points of H (rotated) are marked in blue. The checked candidate edges are in cyan. A distance of the closest point in H from the start point of the candidate edge exceeding sqrt(2) is marked in orange, and a distance from the end point exceeding sqrt(2) is marked in brown. Vectors in purple are used for the distance between midpoints of edges in G to the closest point in H (rotated).",
				"The infinite length of the 4 arms has yet to be proved."
			],
			"link": [
				"Hugo Pfoertner, \u003ca href=\"/A307446/b307446.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Klaus Nagel, \u003ca href=\"/A307446/a307446.png\"\u003eMapping of 2 grids with distance limit sqrt(1/2)\u003c/a\u003e, 4 symmetric paths starting in lower left corner (2009).",
				"Hugo Pfoertner, \u003ca href=\"/A307446/a307446_2.png\"\u003eConstruction of A307446 and A307447\u003c/a\u003e.",
				"Hugo Pfoertner, \u003ca href=\"/A307446/a307446_3.png\"\u003ePaths in lattices G and H superimposed\u003c/a\u003e.",
				"Hugo Pfoertner, \u003ca href=\"/A307446/a307446_1.png\"\u003eA307447 vs A307446\u003c/a\u003e, n = 1...1562.",
				"Hugo Pfoertner, \u003ca href=\"/A307446/a307446.pdf\"\u003eA307447 vs A307446\u003c/a\u003e, n = 1...50000. Zoom in to see details.",
				"\u003ca href=\"/index/Con#coordinates_2D_curves\"\u003eIndex entries for sequences related to coordinates of 2D curves\u003c/a\u003e"
			],
			"program": [
				"(PARI) continuation(ii,jj,iprev,jprev)=",
				"{my(i,j,ix,iy,x,y,d,C=sqrt(3)/2,S=1/2,",
				"id=[1,0,-1,0],jd=[0,1,0,-1],v=[0,0],dmin=oo);",
				"for(nd=1,4,i=ii+id[nd];j=jj+jd[nd];",
				"if(i!=iprev||j!=jprev,",
				"x=C*i+S*j;ix=round(x);if(ix%2!=0,if(ix\u003ex,ix--,ix++));",
				"y=C*j-S*i;iy=round(y);if(iy%2!=0,if(iy\u003ey,iy--,iy++));",
				"x=C*ix-S*iy;y=C*iy+S*ix;",
				"if((i+id[nd]-x)^2+(j+jd[nd]-y)^2\u003c2\u0026\u0026(x-ii)^2+(y-jj)^2\u003c2,d=(i-x)^2+(j-y)^2;",
				"if(d\u003cdmin,v[1]=i;v[2]=j;dmin=d;))));v}",
				"iprev=0;jprev=1;ii=1;jj=1; \\\\ use print1(jj,...) to get A307447",
				"for(k=1,68,print1(ii,\", \");w=continuation(ii,jj,iprev,jprev);iprev=w[1];jprev=w[2];ii+=2*(w[1]-ii);jj+=2*(w[2]-jj))"
			],
			"xref": [
				"Cf. A307110, A307447.",
				"Coordinates of corresponding points on rotated lattice: A307482, A307483."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_Hugo Pfoertner_, Apr 10 2019",
			"references": 4,
			"revision": 28,
			"time": "2020-02-13T06:53:25-05:00",
			"created": "2019-04-13T22:14:54-04:00"
		}
	]
}