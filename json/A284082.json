{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A284082",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 284082,
			"data": "1,1,1,2,1,1,1,3,2,2,1,1,1,3,2,4,1,1,1,1,0,5,1,1,2,6,3,1,1,2,1,5,0,4,2,2,1,9,0,1,1,3,1,8,5,11,1,2,2,2,8,4,1,5,0,1,0,14,1,2,1,5,0,6,2,5,1,0,7,9,1,0,1,18,10,2,0,0,1,0,4,10,1,2,8",
			"name": "Smallest positive m such that n divides sigma_m(n) - j where j is some divisor of n, or 0 if no such m exists.",
			"comment": [
				"If p is a prime, we have a(p) = 1. In general, if n = p^q with p prime, then a(n) \u003c= q. For every prime power p^q \u003c 10^13 it actually holds a(p^q) = q. Is this true for every prime power? - _Giovanni Resta_, Mar 20 2017",
				"Yes, this is true: sigma[m](p^q) == 1/(1-p^m) (mod p^q); this is never divisible by p, and == 1 (mod p^q) iff m \u003e= q. - _Robert Israel_, Apr 27 2017",
				"First occurrence of k: 21, 1, 4, 8, 16, 22, 26, 69, 44, 38, 75, 46, 148, 316, 58, 186, ..., . - _Robert G. Wilson v_, Apr 14 2017"
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A284082/b284082.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"maple": [
				"f:= proc(n) local m,mm,F,S,P,D,M0,M1;",
				"      F:= ifactors(n)[2];",
				"      if nops(F) = 1 then return F[1][2] fi;",
				"      P:= map(t -\u003e t[1]^t[2], F);",
				"      S:= mul(add(t[1]^(i*m),i=0..t[2]),t=F);",
				"      D:= subs(n=0,numtheory:-divisors(n));",
				"      for mm from 1 to ilcm(op(map(numtheory:-phi, P)))+max(seq(t[2],t=F)) do",
				"        if member(subs(m=mm,S) mod n, D) then return mm fi;",
				"      od;",
				"      0",
				"end proc:",
				"map(f, [$1..100]); # _Robert Israel_, Apr 27 2017"
			],
			"mathematica": [
				"a[n_] := Block[{ds, d=Divisors[n], m=0}, While[m \u003c= 2*n, m++; ds = DivisorSigma[m, n]; If[ Select[d, Mod[ds-#, n] == 0 \u0026, 1] != {}, Break[]]]; If[m \u003e 2*n, 0, m]]; Array[a, 85] (* assuming that sigma(m,n) mod n has a period \u003c= 2*n, _Giovanni Resta_, Mar 20 2017 *)"
			],
			"xref": [
				"Integers n such that n divides sigma_k(n) - i, where i is some divisor of n; A205523 (k = 1), A283970 (k = 2).",
				"Cf. A033950."
			],
			"keyword": "nonn",
			"offset": "1,4",
			"author": "_Juri-Stepan Gerasimov_, Mar 19 2017",
			"ext": [
				"a(56) from _Giovanni Resta_, Mar 20 2017"
			],
			"references": 2,
			"revision": 24,
			"time": "2017-04-27T23:03:54-04:00",
			"created": "2017-04-27T12:52:53-04:00"
		}
	]
}