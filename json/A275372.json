{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A275372",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 275372,
			"data": "1,-1,-7,6,20,-13,-34,15,53,-25,-91,52,135,-65,-180,82,253,-133,-343,160,449,-207,-603,306,780,-348,-979,438,1241,-600,-1557,703,1924,-890,-2375,1115,2910,-1300,-3535,1620,4318,-1993,-5198,2335,6180,-2783,-7420",
			"name": "Expansion of f(-x) * f(-x^2)^4 / phi(x^2) in powers of x where phi(), f() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A275372/b275372.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of f(-x^2)^7 / (f(x) * f(x^2)^2) in powers of x where f() is a Ramanujan theta function.",
				"Expansion of q^(-3/8) * eta(q) * eta(q^2)^6 * eta(q^8)^2 / eta(q^4)^5 in powers of q.",
				"Euler transform of period 8 sequence [ -1, -7, -1, -2, -1, -7, -1, -4, ...].",
				"G.f.: Product_{k\u003e0} (1 - x^k)^4 * (1 + x^k)^3 * (1 + x^(4*k))^2 / (1 + x^(2*k))^3.",
				"2 * a(n) = - A279955(2*n + 1)."
			],
			"example": [
				"G.f. = 1 - x - 7*x^2 + 6*x^3 + 20*x^4 - 13*x^5 - 34*x^6 + 15*x^7 + 53*x^8 + ...",
				"G.f. = q^3 - q^11 - 7*q^19 + 6*q^27 + 20*q^35 - 13*q^43 - 34*q^51 + 15*q^59 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x] QPochhammer[ x^2]^4 / EllipticTheta[ 3, 0, x^2], {x, 0, n}];",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ x^2]^7 / (QPochhammer[ -x] QPochhammer[ -x^2]^2), {x, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(A); if( n\u003c0, 0, A = x * O(x^n); polcoeff( eta(x + A) * eta(x^2 + A)^6 * eta(x^8 + A)^2 / eta(x^4 + A)^5, n))};"
			],
			"xref": [
				"Cf. A279955."
			],
			"keyword": "sign",
			"offset": "0,3",
			"author": "_Michael Somos_, Dec 25 2016",
			"references": 1,
			"revision": 20,
			"time": "2021-03-12T22:24:48-05:00",
			"created": "2016-12-25T14:02:16-05:00"
		}
	]
}