{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A002129",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 2129,
			"id": "M3236 N1307",
			"data": "1,-1,4,-5,6,-4,8,-13,13,-6,12,-20,14,-8,24,-29,18,-13,20,-30,32,-12,24,-52,31,-14,40,-40,30,-24,32,-61,48,-18,48,-65,38,-20,56,-78,42,-32,44,-60,78,-24,48,-116,57,-31,72,-70,54,-40,72,-104,80,-30,60,-120,62,-32,104,-125",
			"name": "Generalized sum of divisors function: excess of sum of odd divisors of n over sum of even divisors of n.",
			"comment": [
				"Glaisher calls this zeta(n) or zeta_1(n). - _N. J. A. Sloane_, Nov 24 2018",
				"Coefficients in expansion of Sum_{n \u003e= 1} x^n/(1+x^n)^2 = Sum_{n \u003e= 1} (-1)^(n-1)*n*x^n/(1-x^n).",
				"Unsigned sequence is A113184. - _Peter Bala_, Dec 14 2020"
			],
			"reference": [
				"L. Comtet, Advanced Combinatorics, Reidel, 1974, p. 162, #16, (6), 3rd formula.",
				"Steven R. Finch, Mathematical Constants, Cambridge, 2003, pp. 259-262.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A002129/b002129.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (terms 1..1000 from T. D. Noe)",
				"Steven R. Finch, \u003ca href=\"http://www.people.fas.harvard.edu/~sfinch/constant/onenin/onenin.html\"\u003eThe \"One-Ninth\" Constant\u003c/a\u003e [Broken link]",
				"Steven R. Finch, \u003ca href=\"http://web.archive.org/web/20010603070928/http://www.mathsoft.com/asolve/constant/onenin/onenin.html\"\u003eThe \"One-Ninth\" Constant\u003c/a\u003e [From the Wayback machine]",
				"J. W. L. Glaisher, \u003ca href=\"https://books.google.com/books?id=bLs9AQAAMAAJ\u0026amp;pg=RA1-PA1\"\u003eOn the representations of a number as the sum of two, four, six, eight, ten, and twelve squares\u003c/a\u003e, Quart. J. Math. 38 (1907), 1-62 (see p. 4 and p. 8).",
				"Heekyoung Hahn, \u003ca href=\"http://arxiv.org/abs/1507.04426\"\u003eConvolution sums of some functions on divisors\u003c/a\u003e, arXiv:1507.04426 [math.NT], 2015.",
				"P. A. MacMahon, \u003ca href=\"http://plms.oxfordjournals.org/content/s2-19/1/75.extract\"\u003eDivisors of numbers and their continuations in the theory of partitions\u003c/a\u003e, Proc. London Math. Soc., (2) 19 (1919), 75-113; Coll. Papers II, pp. 303-341.",
				"\u003ca href=\"/index/Ge#Glaisher\"\u003eIndex entries for sequences mentioned by Glaisher\u003c/a\u003e"
			],
			"formula": [
				"Multiplicative with a(p^e) = 3-2^(e+1) if p = 2; (p^(e+1)-1)/(p-1) if p \u003e 2. - _David W. Wilson_, Sep 01 2001",
				"G.f.: Sum_{n\u003e=1} n*x^n*(1-3*x^n)/(1-x^(2*n)). - _Vladeta Jovovic_, Oct 15 2002",
				"L.g.f.: Sum_{n\u003e=1} a(n)*x^n/n = log[ Sum_{n\u003e=0} x^(n(n+1)/2) ], the log of the g.f. of A010054. - _Paul D. Hanna_, Jun 28 2008",
				"Dirichlet g.f. zeta(s)*zeta(s-1)*(1-4/2^s). Dirichlet convolution of A000203 and the quasi-finite (1,-4,0,0,0,...). - _R. J. Mathar_, Mar 04 2011",
				"a(n) = A000593(n)-A146076(n). - _R. J. Mathar_, Mar 05 2011"
			],
			"example": [
				"a(28) = 40 because the sum of the even divisors of 28 (2, 4, 14 and 28) = 48 and the sum of the odd divisors of 28 (1 and 7) = 8, their absolute difference being 40."
			],
			"maple": [
				"A002129 := proc(n) -add((-1)^d*d,d=numtheory[divisors](n)) ; end proc: # _R. J. Mathar_, Mar 05 2011"
			],
			"mathematica": [
				"f[n_] := Block[{c = Divisors@ n}, Plus @@ Select[c, EvenQ] - Plus @@ Select[c, OddQ]]; Array[f, 64] (* _Robert G. Wilson v_, Mar 04 2011 *)",
				"a[n_] := DivisorSum[n, -(-1)^#*#\u0026]; Array[a, 80] (* _Jean-François Alcover_, Dec 01 2015 *)",
				"f[p_, e_] := If[p == 2, 3 - 2^(e + 1), (p^(e + 1) - 1)/(p - 1)]; a[1] = 1; a[n_] := Times @@ (f @@@ FactorInteger[n]);  Array[a, 64] (* _Amiram Eldar_, Jul 20 2019 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c1,0,-sumdiv(n,d,(-1)^d*d))",
				"(PARI) {a(n)=n*polcoeff(log(sum(k=0,(sqrtint(8*n+1)-1)\\2,x^(k*(k+1)/2))+x*O(x^n)),n)} \\\\ _Paul D. Hanna_, Jun 28 2008"
			],
			"xref": [
				"A diagonal of A060044.",
				"a(2^n) = -A036563(n+1). a(3^n) = A003462(n+1).",
				"First differences of -A024919(n).",
				"Cf. A010054, A113184."
			],
			"keyword": "sign,easy,nice,mult",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Better description and more terms from _Robert G. Wilson v_, Dec 14 2000",
				"More terms from _N. J. A. Sloane_, Mar 19 2001"
			],
			"references": 74,
			"revision": 67,
			"time": "2020-12-14T10:33:29-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}