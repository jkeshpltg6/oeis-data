{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007253",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7253,
			"id": "M4131",
			"data": "1,0,-6,20,15,36,0,-84,195,100,240,0,-461,1020,540,1144,0,-1980,4170,2040,4275,0,-6984,14340,6940,14076,0,-21936,44025,20760,41476,0,-62484,123620,57630,113244,0,-166056,324120,148900,289578,0",
			"name": "McKay-Thompson series of class 5a for Monster.",
			"reference": [
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A007253/b007253.txt\"\u003eTable of n, a(n) for n = -1..1000\u003c/a\u003e (terms -1..100 from G. A. Edgar)",
				"J. H. Conway and S. P. Norton, \u003ca href=\"http://blms.oxfordjournals.org/content/11/3/308.extract\"\u003eMonstrous Moonshine\u003c/a\u003e, Bull. Lond. Math. Soc. 11 (1979) 308-339.",
				"D. Ford, J. McKay and S. P. Norton, \u003ca href=\"http://dx.doi.org/10.1080/00927879408825127\"\u003eMore on replicable functions\u003c/a\u003e, Commun. Algebra 22, No. 13, 5175-5193 (1994).",
				"J. McKay and H. Strauss, \u003ca href=\"http://dx.doi.org/10.1080/00927879008823911\"\u003eThe q-series of monstrous moonshine and the decomposition of the head characters\u003c/a\u003e, Comm. Algebra 18 (1990), no. 1, 253-278.",
				"\u003ca href=\"/index/Mat#McKay_Thompson\"\u003eIndex entries for McKay-Thompson series for Monster simple group\u003c/a\u003e"
			],
			"formula": [
				"G.f.: T5a(q) satisfies functional equation P5(T5a(q)) = j(q^5) - 744, where we used modular function j(q) from A000521 and polynomial P5(t) = t^5+30*t^3-100*t^2+105*t-780. _G. A. Edgar_, Mar 10 2017"
			],
			"example": [
				"T5a = 1/q - 6*q + 20*q^2 + 15*q^3 + 36*q^4 - 84*q^6 + 195*q^7 + 100*q^8 + ..."
			],
			"maple": [
				"with(numtheory): TOP := 23;",
				"Order:=101;",
				"g2 := (4/3) * (1 + 240 * add(sigma[ 3 ](n)*q^n, n=1..TOP-1));",
				"g3 := (8/27) * (1 - 504 * add(sigma[ 5 ](n)*q^n, n=1..TOP-1));",
				"delta := series(g2^3 - 27*g3^2, q=0, TOP);",
				"j := series(1728 * g2^3 / delta, q=0, TOP);",
				"# computation above of j is from A000521",
				"P5 := t^5 + 30*t^3 - 100*t^2 + 105*t - 780;",
				"subs(t = q^(-1) + x, P5) - subs(q=q^5, j - 744);",
				"solve(%, x);",
				"T5a := series(q^(-1)+%, q=0) assuming q \u003e 0;",
				"# _G. A. Edgar_, Mar 10 2017"
			],
			"mathematica": [
				"eta[q_]:= q^(1/24)*QPochhammer[q]; e5B:= (eta[q]/eta[q^5])^6; e25a:= (eta[q]/eta[q^25]);  a[n_]:= SeriesCoefficient[(1 + 5/e25a)*(1 + e5B) + 5*(e25a - 5/e25a)*(e5B/(e25a)^3), {q, 0, n}]; Table[a[n], {n, -1, 50}] (* _G. C. Greubel_, Jan 25 2018 *)"
			],
			"program": [
				"(PARI) q='q+O('q^30);  F=(1 + 5*q*eta(q^25)/eta(q))*(1 + (eta(q)/eta(q^5) )^6/q) + 5*(eta(q)/(q*eta(q^25)) - 5*q*eta(q^25)/eta(q))*(q^2* eta(q^25)^3 *eta(q)^3/eta(q^5)^6); Vec(F)  \\\\ _G. C. Greubel_, Jun 12 2018"
			],
			"keyword": "sign",
			"offset": "-1,3",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"More terms from _G. A. Edgar_, Mar 10 2017"
			],
			"references": 2,
			"revision": 33,
			"time": "2018-06-13T03:38:53-04:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}