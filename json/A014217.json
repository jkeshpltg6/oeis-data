{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A014217",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 14217,
			"data": "1,1,2,4,6,11,17,29,46,76,122,199,321,521,842,1364,2206,3571,5777,9349,15126,24476,39602,64079,103681,167761,271442,439204,710646,1149851,1860497,3010349,4870846,7881196,12752042,20633239,33385281,54018521,87403802",
			"name": "a(n) = floor(phi^n), where phi = (1+sqrt(5))/2 is the golden ratio.",
			"comment": [
				"Floor{lim k-\u003eoo {Fibonacci(k)/Fibonacci(k-n)}}. - _Jon Perry_, Jun 10 2003",
				"For n\u003e1 a(n) is the maximum element in the continued fraction for A000045(n)*phi. - _Benoit Cloitre_, Jun 19 2005",
				"a(n) is also the number of circles curvature (rounded down) inscribed in kite arranged as spiral form, starting with a unit circle. See illustration in links. - _Kival Ngaokrajang_, Aug 29 2013",
				"a(n) is the n-th Lucas number (A000032) if n is odd, and a(n) is the n-th Lucas number minus 1 if n is even. (Mario Catalani's formula below expresses this fact.) This is related to the fact that the powers of phi approach the values of the Lucas numbers, the odd powers from above and the even powers from below. - _Geoffrey Caveney_, Apr 18 2014"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A014217/b014217.txt\"\u003eTable of n, a(n) for n = 0..4784\u003c/a\u003e (first 301 terms from T. D. Noe)",
				"Mohammad K. Azarian, \u003ca href=\"http://www.math-cs.ucmo.edu/~mjms/1998.3/prob.ps\"\u003eProblem 123\u003c/a\u003e, Missouri Journal of Mathematical Sciences, Vol. 10, No. 3, Fall 1998, p. 176.  \u003ca href=\"http://www.math-cs.ucmo.edu/~mjms/2000.1/soln.ps\"\u003eSolution\u003c/a\u003e published in Vol. 12, No. 1, Winter 2000, pp. 61-62.",
				"Ayman A. El-Okaby, \u003ca href=\"http://arxiv.org/abs/0709.2394\"\u003e Exceptional Lie Groups, E-infinity Theory and Higgs Boson\u003c/a\u003e, arXiv:0709.2394 [physics.gen-ph], 2007.",
				"G. Harman, \u003ca href=\"http://at.yorku.ca/cgi-bin/amca/cadx-39\"\u003eOne hundred years of normal numbers\u003c/a\u003e",
				"Kival Ngaokrajang, \u003ca href=\"/A014217/a014217.pdf\"\u003eIllustration for n = 0..7\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,2,-1,-1)."
			],
			"formula": [
				"a(n) = a(n-1) + 2*a(n-2) - a(n-3) - a(n-4).",
				"a(n) = a(n-1) + a(n-2) + (1-(-1)^n)/2 = a(n-1) + a(n-2) + A000035(n).",
				"a(n) = A000032(n)-(1+(-1)^n)/2. - Mario Catalani (mario.catalani(AT)unito.it), Jan 17 2003",
				"G.f.: (1-x^2+x^3)/((1+x)(1-x)(1-x-x^2)). - _R. J. Mathar_, Sep 06 2008",
				"a(2n-1) = (Fibonacci(4n+1)-2)/Fibonacci(2n+2). - _Gary Detlefs_, Feb 16 2011",
				"a(n) = floor(Fibonacci(2n+3)/Fibonacci(n+3)). - _Gary Detlefs_, Feb 28 2011",
				"a(2n) = Fibonacci(2*n-1)+Fibonacci(2*n+1)-1. - _Gary Detlefs_, Mar 10 2011",
				"a(n+2*k)-a(n) = A203976(k)*A000032(n+k) if k odd; a(n+2*k)-a(n) = A203976(k)*A000045(n+k) if k even; for k\u003e0. - _Paul Curtz_, Jun 05 2013",
				"a(n) = A052952(n) - A052952(n-2) + A052952(n-3). - _R. J. Mathar_, Jun 13 2013",
				"a(n+6) - a(n-6) = 40*A000045(n), case k=6 of my formula above. - _Paul Curtz_, Jun 13 2013",
				"a(n-3) + a(n+3) = A153382(n). - _Paul Curtz_, Jun 17 2013",
				"a(n-1) + a(n+2) = A022319(n). - _Paul Curtz_, Jun 17 2013",
				"For k\u003e0, a(2k) = A169985(2k)-1 and a(2k+1) = A169985(2k+1) (which is equivalent to Catalani's 2003 formula). - _Danny Rorabaugh_, Apr 15 2015",
				"a(n) = ((-1)^(1+n)-1)/2 + ((1-sqrt(5))/2)^n + ((1+sqrt(5))/2)^n. - _Colin Barker_, Nov 05 2017"
			],
			"maple": [
				"A014217 := proc(n)",
				"    option remember;",
				"    if n \u003c= 3 then",
				"        op(n+1,[1,1,2,4]) ;",
				"    else",
				"        procname(n-1)+2*procname(n-2)-procname(n-3)-procname(n-4) ;",
				"    end if;",
				"end proc: # _R. J. Mathar_, Jun 23 2013",
				"#",
				"a:= n-\u003e (\u003c\u003c0|1|0|0\u003e, \u003c0|0|1|0\u003e, \u003c0|0|0|1\u003e, \u003c-1|-1|2|1\u003e\u003e^n. \u003c\u003c1, 1, 2, 4\u003e\u003e)[1, 1]:",
				"seq(a(n), n=0..40);  # _Alois P. Heinz_, Oct 12 2017"
			],
			"mathematica": [
				"Table[Floor[GoldenRatio^n], {n, 0, 36}] (* _Vladimir Joseph Stephan Orlovsky_, Dec 12 2008 *)",
				"LinearRecurrence[{1, 2, -1, -1}, {1, 1, 2, 4}, 40] (* _Jean-François Alcover_, Nov 05 2017 *)"
			],
			"program": [
				"(PARI) for (n=0,20,print1(fibonacci(1000)\\fibonacci(1000-n), \",\"))",
				"(MAGMA) [Floor( ((1+Sqrt(5))/2)^n ): n in [0..100]]; // _Vincenzo Librandi_, Apr 16 2011",
				"(Haskell)",
				"a014217 n = a014217_list !! n",
				"a014217_list = 1 : 1 : zipWith (+)",
				"   a000035_list (zipWith (+) a014217_list $ tail a014217_list)",
				"-- _Reinhard Zumkeller_, Jan 06 2012",
				"(Sage) [floor(golden_ratio^n) for n in range(37)] # _Danny Rorabaugh_, Apr 19 2015",
				"(Python)",
				"from sympy import floor, sqrt",
				"def A014217(n): return floor(((1+sqrt(5))/2)**n) # _Chai Wah Wu_, Dec 17 2021"
			],
			"xref": [
				"Cf. A000045, A020956, A052952, A057146, A062114, A169985, A169986, A226328."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,3",
			"author": "_Clark Kimberling_",
			"ext": [
				"Corrected by _T. D. Noe_, Nov 09 2006",
				"Edited by _N. J. A. Sloane_, Aug 29 2008 at the suggestion of _R. J. Mathar_"
			],
			"references": 39,
			"revision": 125,
			"time": "2021-12-18T14:58:26-05:00",
			"created": "1998-06-14T03:00:00-04:00"
		}
	]
}