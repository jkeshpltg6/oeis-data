{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A126350",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 126350,
			"data": "1,1,2,1,5,5,1,9,22,15,1,14,61,99,52,1,20,135,385,471,203,1,27,260,1140,2416,2386,877,1,35,455,2835,9156,15470,12867,4140,1,44,742,6230,28441,72590,102215,73681,21147",
			"name": "Triangle read by rows: matrix product of the binomial coefficients with the Stirling numbers of the second kind.",
			"comment": [
				"Many well-known integer sequences arise from such a matrix product of combinatorial coefficients. In the present case we have as the first row (not surprisingly) A000110 = Bell or exponential numbers: ways of placing n labeled balls into n indistinguishable boxes . As second row we have A033452 = \"STIRLING\" transform of squares A000290. As the column sums we have 1, 3, 11, 47, 227, 1215, 7107, 44959, 305091 which is A035009 = STIRLING transform of [1,1,2,4,8,16,32, ...]."
			],
			"formula": [
				"(In Maple notation:) Matrix product A.B of matrix A[i,j]:=binomial(j-1,i-1) with i = 1 to p+1, j = 1 to p+1, p=8 and of matrix B[i,j]:=stirling2(j,i) with i from 1 to d, j from 1 to d, d=9."
			],
			"example": [
				"Matrix begins:",
				"1 2 5 15 52 203  877  4140  21147",
				"0 1 5 22 99 471 2386 12867  73681",
				"0 0 1  9 61 385 2416 15470 102215",
				"0 0 0  1 14 135 1140  9156  72590",
				"0 0 0  0 1   20  260  2835  28441",
				"0 0 0  0 0    1   27   455   6230",
				"0 0 0  0 0    0    1    35    742",
				"0 0 0  0 0    0    0     1     44",
				"0 0 0  0 0    0    0     0      1"
			],
			"maple": [
				"T:= (n, k)-\u003e add(Stirling2(n, j)*binomial(j-1, n-k), j=n-k+1..n):",
				"seq(seq(T(n, k), k=1..n), n=1..10);  # _Alois P. Heinz_, Sep 03 2019"
			],
			"mathematica": [
				"T[dim_] := T[dim] = Module[{M}, M[n_, n_] = 1; M[_, _] = 0; Do[M[n, k] = M[n-1, k-1] + (k+2) M[n-1, k] + (k+1) M[n-1, k+1], {n, 0, dim-1}, {k, 0, n-1}]; Array[M, {dim, dim}, {0, 0}]];",
				"dim = 9;",
				"Table[T[dim][[n]][[1 ;; n]] // Reverse, {n, 1, dim}] (* _Jean-François Alcover_, Jun 27 2019, from Sage *)"
			],
			"program": [
				"(Sage)",
				"def A126350_triangle(dim): # rows in reversed order",
				"    M = matrix(ZZ,dim,dim)",
				"    for n in (0..dim-1): M[n,n] = 1",
				"    for n in (1..dim-1):",
				"        for k in (0..n-1):",
				"            M[n,k] = M[n-1,k-1]+(k+2)*M[n-1,k]+(k+1)*M[n-1,k+1]",
				"    return M",
				"A126350_triangle(9) # _Peter Luschny_, Sep 19 2012"
			],
			"xref": [
				"Cf. A039810, A039814, A126351, A054654, A126353.",
				"Cf. A137597."
			],
			"keyword": "nonn,tabl",
			"offset": "1,3",
			"author": "_Thomas Wieder_, Dec 29 2006",
			"references": 4,
			"revision": 19,
			"time": "2020-03-17T13:50:02-04:00",
			"created": "2007-01-12T03:00:00-05:00"
		}
	]
}