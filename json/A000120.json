{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A000120",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 120,
			"id": "M0105 N0041",
			"data": "0,1,1,2,1,2,2,3,1,2,2,3,2,3,3,4,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,2,3,3,4,3,4,4,5,3",
			"name": "1's-counting sequence: number of 1's in binary expansion of n (or the binary weight of n).",
			"comment": [
				"The binary weight of n is also called Hamming weight of n. [The term \"Hamming weight\" was named after the American mathematician Richard Wesley Hamming (1915-1998). - _Amiram Eldar_, Jun 16 2021]",
				"a(n) is also the largest integer such that 2^a(n) divides binomial(2n, n) = A000984(n). - _Benoit Cloitre_, Mar 27 2002",
				"To construct the sequence, start with 0 and use the rule: If k \u003e= 0 and a(0), a(1), ..., a(2^k-1) are the first 2^k terms, then the next 2^k terms are a(0) + 1, a(1) + 1, ..., a(2^k-1) + 1. - _Benoit Cloitre_, Jan 30 2003",
				"An example of a fractal sequence. That is, if you omit every other number in the sequence, you get the original sequence. And of course this can be repeated. So if you form the sequence a(0 * 2^n), a(1 * 2^n), a(2 * 2^n), a(3 * 2^n), ... (for any integer n \u003e 0), you get the original sequence. - Christopher.Hills(AT)sepura.co.uk, May 14 2003",
				"The n-th row of Pascal's triangle has 2^k distinct odd binomial coefficients where k = a(n) - 1. - _Lekraj Beedassy_, May 15 2003",
				"Fixed point of the morphism 0 -\u003e 01, 1 -\u003e 12, 2 -\u003e 23, 3 -\u003e 34, 4 -\u003e 45, etc., starting from a(0) = 0. - _Robert G. Wilson v_, Jan 24 2006",
				"a(n) is the number of times n appears among the mystery calculator sequences: A005408, A042964, A047566, A115419, A115420, A115421. - _Jeremy Gardiner_, Jan 25 2006",
				"a(n) is the number of solutions of the Diophantine equation 2^m*k + 2^(m-1) + i = n, where m \u003e= 1, k \u003e= 0, 0 \u003c= i \u003c 2^(m-1); a(5) = 2 because only (m, k, i) = (1, 2, 0) [2^1*2 + 2^0 + 0 = 5] and (m, k, i) = (3, 0, 1) [2^3*0 + 2^2 + 1 = 5] are solutions. - _Hieronymus Fischer_, Jan 31 2006",
				"The first appearance of k, k \u003e= 0, is at a(2^k-1). - _Robert G. Wilson v_, Jul 27 2006",
				"Sequence is given by T^(infinity)(0) where T is the operator transforming any word w = w(1)w(2)...w(m) into T(w) = w(1)(w(1)+1)w(2)(w(2)+1)...w(m)(w(m)+1). I.e., T(0) = 01, T(01) = 0112, T(0112) = 01121223. - _Benoit Cloitre_, Mar 04 2009",
				"For n \u003e= 2, the minimal k for which a(k(2^n-1)) is not multiple of n is 2^n + 3. - _Vladimir Shevelev_, Jun 05 2009",
				"Triangle inequality: a(k+m) \u003c= a(k) + a(m). Equality holds if and only if C(k+m, m) is odd. - _Vladimir Shevelev_, Jul 19 2009",
				"The number of occurrences of value k in the first 2^n terms of the sequence is equal to binomial(n, k), and also equal to the sum of the first n - k + 1 terms of column k in the array A071919. Example with k = 2, n = 7: there are 21 = binomial(7,2) = 1 + 2 + 3 + 4 + 5 + 6 2's in a(0) to a(2^7-1). - Brent Spillner (spillner(AT)acm.org), Sep 01 2010, simplified by _R. J. Mathar_, Jan 13 2017",
				"Let m be the number of parts in the listing of the compositions of n as lists of parts in lexicographic order, a(k) = n - length(composition(k)) for all k \u003c 2^n and all n (see example); A007895 gives the equivalent for compositions into odd parts. - _Joerg Arndt_, Nov 09 2012",
				"From _Daniel Forgues_, Mar 13 2015: (Start)",
				"Just tally up row k (binary weight equal k) from 0 to 2^n - 1 to get the binomial coefficient C(n,k). (See A007318.)",
				"     0   1       3               7                              15",
				"  0: O | . | .   . | .   .   .   . | .   .   .   .   .   .   .   . |",
				"  1:   | O | O   . | O   .   .   . | O   .   .   .   .   .   .   . |",
				"  2:   |   |     O |     O   O   . |     O   O   .   O   .   .   . |",
				"  3:   |   |       |             O |             O       O   O   . |",
				"  4:   |   |       |               |                             O |",
				"Due to its fractal nature, the sequence is quite interesting to listen to.",
				"(End)",
				"The binary weight of n is a particular case of the digit sum (base b) of n. - _Daniel Forgues_, Mar 13 2015",
				"The mean of the first n terms is 1 less than the mean of [a(n+1),...,a(2n)], which is also the mean of [a(n+2),...,a(2n+1)]. - _Christian Perfect_, Apr 02 2015",
				"a(n) is also the largest part of the integer partition having viabin number n. The viabin number of an integer partition is defined in the following way. Consider the southeast border of the Ferrers board of the integer partition and consider the binary number obtained by replacing each east step with 1 and each north step, except the last one, with 0. The corresponding decimal form is, by definition, the viabin number of the given integer partition. \"Viabin\" is coined from \"via binary\". For example, consider the integer partition [2, 2, 2, 1]. The southeast border of its Ferrers board yields 10100, leading to the viabin number 20. - _Emeric Deutsch_, Jul 20 2017",
				"a(n) is also known as the population count of the binary representation of n. - _Chai Wah Wu_, May 19 2020"
			],
			"reference": [
				"Jean-Paul Allouche and Jeffrey Shallit, Automatic Sequences, Cambridge Univ. Press, 2003, p. 119.",
				"Donald E. Knuth, The Art of Computer Programming, vol. 4A, Combinatorial Algorithms, Section 7.1.3, Problem 41, p. 589. - _N. J. A. Sloane_, Aug 03 2012",
				"Manfred R. Schroeder, Fractals, Chaos, Power Laws. W.H. Freeman, 1991, p. 383.",
				"N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"N. J. A. Sloane, \u003ca href=\"/A000120/b000120.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Franklin T. Adams-Watters, and Frank Ruskey, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL12/Ruskey2/ruskey14.html\"\u003eGenerating Functions for the Digital Sum and Other Digit Counting Sequences\u003c/a\u003e, JIS, Vol. 12 (2009), Article 09.5.6.",
				"J.-P. Allouche, \u003ca href=\"http://math.colgate.edu/~integers/graham2/graham2.Abstract.html\"\u003eOn an Inequality in a 1970 Paper of R. L. Graham\u003c/a\u003e, INTEGERS 21A (2021), #A2.",
				"Jean-Paul Allouche and Jeffrey Shallit, \u003ca href=\"http://dx.doi.org/10.1016/0304-3975(92)90001-V\"\u003eThe ring of k-regular sequences\u003c/a\u003e, Theoretical Computer Sci., Vol. 98 (1992), pp. 163-197. (\u003ca href=\"http://www.cs.uwaterloo.ca/~shallit/Papers/as0.ps\"\u003ePS file on author's web page.\u003c/a\u003e)",
				"Jean-Paul Allouche, Jeffrey Shallit and Jonathan Sondow, \u003ca href=\"http://arxiv.org/abs/math/0512399\"\u003eSummation of Series Defined by Counting Blocks of Digits\u003c/a\u003e, arXiv:math/0512399 [math.NT], 2005-2006.",
				"Jean-Paul Allouche, Jeffrey Shallit and Jonathan Sondow, \u003ca href=\"http://dx.doi.org/10.1016/j.jnt.2006.06.001\"\u003eSummation of series defined by counting blocks of digits\u003c/a\u003e, J. Number Theory, Vol. 123 (2007), pp. 133-143.",
				"Richard Bellman and Harold N. Shapiro, \u003ca href=\"http://www.jstor.org/stable/1969281\"\u003eOn a problem in additive number theory\u003c/a\u003e, Annals Math., Vol. 49, No. 2 (1948), pp. 333-340. - _N. J. A. Sloane_, Mar 12 2009",
				"Jean Coquet, \u003ca href=\"http://dx.doi.org/10.1016/0022-314X(86)90067-3\"\u003ePower sums of digital sums\u003c/a\u003e, J. Number Theory, Vol. 22, No. 2 (1986), pp. 161-176.",
				"Karl Dilcher and Larry Ericksen, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v22i2p24\"\u003eHyperbinary expansions and Stern polynomials\u003c/a\u003e, Elec. J. Combin, Vol. 22 (2015), #P2.24.",
				"Josef Eschgfäller and Andrea Scarpante, \u003ca href=\"http://arxiv.org/abs/1603.08500\"\u003eDichotomic random number generators\u003c/a\u003e, arXiv:1603.08500 [math.CO], 2016.",
				"Emmanuel Ferrand, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL10/Ferrand/ferrand8.html\"\u003eDeformations of the Taylor Formula\u003c/a\u003e, Journal of Integer Sequences, Vol. 10 (2007), Article 07.1.7.",
				"Steven R. Finch, Pascal Sebah and Zai-Qiao Bai, \u003ca href=\"http://arXiv.org/abs/0802.2654\"\u003eOdd Entries in Pascal's Trinomial Triangle\u003c/a\u003e, arXiv:0802.2654 [math.NT], 2008.",
				"Philippe Flajolet, Peter Grabner, Peter Kirschenhofer, Helmut Prodinger and Robert F. Tichy, \u003ca href=\"http://algo.inria.fr/flajolet/Publications/FlGrKiPrTi94.pdf\"\u003eMellin Transforms And Asymptotics: Digital Sums\u003c/a\u003e, Theoret. Computer Sci., Vol. 123, No. 2 (1994), pp. 291-314.",
				"Michael Gilleland, \u003ca href=\"/selfsimilar.html\"\u003eSome Self-Similar Integer Sequences\u003c/a\u003e.",
				"P. J. Grabner, P. Kirschenhofer, H. Prodinger and R. F. Tichy, \u003ca href=\"https://doi.org/10.1007/978-94-011-2058-6_25\"\u003eOn the moments of the sum-of-digits function\u003c/a\u003e, Applications of Fibonacci numbers, Vol. 5 (St. Andrews, 1992), Kluwer Acad. Publ., Dordrecht, 1993, 263-271.",
				"Ronald L. Graham, \u003ca href=\"http://dx.doi.org/10.1111/j.1749-6632.1970.tb56468.x\"\u003eOn primitive graphs and optimal vertex assignments\u003c/a\u003e, Internat. Conf. Combin. Math. (New York, 1970), Annals of the NY Academy of Sciences, Vol. 175, 1970, pp. 170-186.",
				"Khodabakhsh Hessami Pilehrood and Tatiana Hessami Pilehrood, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL13/Pilehrood/pilehrood2.html\"\u003eVacca-Type Series for Values of the Generalized Euler Constant Function and its Derivative\u003c/a\u003e, J. Integer Sequences, Vol. 13 (2010), Article 10.7.3.",
				"Nick Hobson, \u003ca href=\"/A000120/a000120.py.txt\"\u003ePython program for this sequence\u003c/a\u003e.",
				"Kathy Q. Ji and Herbert S. Wilf, \u003ca href=\"https://arxiv.org/abs/math/0611465\"\u003eExtreme Palindromes\u003c/a\u003e, arXiv:math/0611465 [math.CO], 2006.",
				"Guy Louchard and Helmut Prodinger, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Prodinger/prodinger29.html\"\u003eThe Largest Missing Value in a Composition of an Integer and Some Allouche-Shallit-Type Identities\u003c/a\u003e, J. Int. Seq., Vol. 16 (2013), Article 13.2.2.",
				"J.-L. Mauclaire and Leo Murata, \u003ca href=\"http://dx.doi.org/10.3792/pjaa.59.274\"\u003eOn q-additive functions, I\u003c/a\u003e, Proc. Japan Acad. Ser. A Math. Sci., Vol. 59, No. 6 (1983), pp. 274-276.",
				"J.-L. Mauclaire and Leo Murata, \u003ca href=\"http://dx.doi.org/10.3792/pjaa.59.441\"\u003eOn q-additive functions, II\u003c/a\u003e, Proc. Japan Acad. Ser. A Math. Sci., Vol. 59, No. 9 (1983), pp. 441-444.",
				"M. D. McIlroy, \u003ca href=\"http://dx.doi.org/10.1137/0203020\"\u003eThe number of 1's in binary integers: bounds and extremal properties\u003c/a\u003e, SIAM J. Comput., Vol. 3 (1974), pp. 255-261.",
				"Kerry Mitchell, \u003ca href=\"http://kerrymitchellart.com/articles/Spirolateral-Type_Images_from_Integer_Sequences.pdf\"\u003eSpirolateral-Type Images from Integer Sequences\u003c/a\u003e, 2013.",
				"Sam Northshield, \u003ca href=\"http://www.jstor.org/stable/10.4169/000298910X496714\"\u003eStern's Diatomic Sequence 0,1,1,2,1,3,2,3,1,4,...\u003c/a\u003e, Amer. Math. Month., Vol. 117, No. 7 (2010), pp. 581-598.",
				"Theophanes E. Raptis, \u003ca href=\"https://arxiv.org/abs/1805.06341\"\u003eFinite Information Numbers through the Inductive Combinatorial Hierarchy\u003c/a\u003e, arXiv:1805.06301 [physics.gen-ph], 2018.",
				"Carlo Sanna, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL15/Sanna/sanna3.html\"\u003eOn Arithmetic Progressions of Integers with a Distinct Sum of Digits\u003c/a\u003e, Journal of Integer Sequences, Vol. 15 (2012), Article 12.8.1. - _N. J. A. Sloane_, Dec 29 2012",
				"Nanci Smith, \u003ca href=\"http://www.fq.math.ca/Scanned/4-4/elementary4-4.pdf\"\u003eProblem B-82\u003c/a\u003e, Fib. Quart., Vol. 4, No. 4 (1966), pp. 374-375.",
				"Jonathan Sondow, \u003ca href=\"https://doi.org/10.1007/978-0-387-68361-4_23\"\u003eNew Vacca-type rational series for Euler's constant and its \"alternating\" analog ln 4/Pi\u003c/a\u003e, arXiv:\u003ca href=\"https://arxiv.org/abs/math/0508042\"\u003emath/0508042\u003c/a\u003e [math.NT] 2005; Additive Number Theory, D. and G. Chudnovsky, eds., Springer, 2010, pp. 331-340.",
				"Ralf Stephan, \u003ca href=\"/somedcgf.html\"\u003eSome divide-and-conquer sequences with (relatively) simple ordinary generating functions\u003c/a\u003e, 2004.",
				"Ralf Stephan, \u003ca href=\"/A079944/a079944.ps\"\u003eTable of generating functions\u003c/a\u003e.",
				"Ralf Stephan, \u003ca href=\"https://arxiv.org/abs/math/0307027\"\u003eDivide-and-conquer generating functions. I. Elementary sequences\u003c/a\u003e, arXiv:math/0307027 [math.CO], 2003.",
				"Kenneth B. Stolarsky, \u003ca href=\"http://dx.doi.org/10.1137/0132060\"\u003ePower and exponential sums of digital sums related to binomial coefficient parity\u003c/a\u003e, SIAM J. Appl. Math., Vol. 32 (1977), pp. 717-730. See B(n). - _N. J. A. Sloane_, Apr 05 2014",
				"J. R. Trollope, \u003ca href=\"http://www.jstor.org/stable/2687954\"\u003eAn explicit expression for binary digital sums\u003c/a\u003e, Math. Mag., Vol. 41, No. 1 (1968), pp. 21-25.",
				"Robert Walker, \u003ca href=\"http://robertinventor.com/ftswiki/Self_Similar_Sloth_Canon_Number_Sequences\"\u003eSelf Similar Sloth Canon Number Sequences\u003c/a\u003e.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Binary.html\"\u003eBinary\u003c/a\u003e, \u003ca href=\"http://mathworld.wolfram.com/DigitCount.html\"\u003eDigit Count\u003c/a\u003e, \u003ca href=\"http://mathworld.wolfram.com/Stolarsky-HarborthConstant.html\"\u003eStolarsky-Harborth Constant\u003c/a\u003e, \u003ca href=\"http://mathworld.wolfram.com/DigitSum.html\"\u003eDigit Sum\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Hamming_weight\"\u003eHamming weight\u003c/a\u003e.",
				"Wolfram Research, \u003ca href=\"http://functions.wolfram.com/NumberTheoryFunctions/DigitCount/31/01/ShowAll.html\"\u003eNumbers in Pascal's triangle\u003c/a\u003e.",
				"\u003ca href=\"/index/Cor#core\"\u003eIndex entries for \"core\" sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e",
				"\u003ca href=\"/index/Coi#Colombian\"\u003eIndex entries for Colombian or self numbers and related sequences\u003c/a\u003e",
				"\u003ca href=\"/index/Fi#FIXEDPOINTS\"\u003eIndex entries for sequences that are fixed points of mappings\u003c/a\u003e"
			],
			"formula": [
				"a(0) = 0, a(2*n) = a(n), a(2*n+1) = a(n) + 1.",
				"a(0) = 0, a(2^i) = 1; otherwise if n = 2^i + j with 0 \u003c j \u003c 2^i, a(n) = a(j) + 1.",
				"G.f.: Product_{k \u003e= 0} (1 + y*x^(2^k)) = Sum_{n \u003e= 0} y^a(n)*x^n. - _N. J. A. Sloane_, Jun 04 2009",
				"a(n) = a(n-1) + 1 - A007814(n) = log_2(A001316(n)) = 2n - A005187(n) = A070939(n) - A023416(n). - _Henry Bottomley_, Apr 04 2001; corrected by _Ralf Stephan_, Apr 15 2002",
				"a(n) = log_2(A000984(n)/A001790(n)). - _Benoit Cloitre_, Oct 02 2002",
				"For n \u003e 0, a(n) = n - Sum_{k=1..n} A007814(k). - _Benoit Cloitre_, Oct 19 2002",
				"a(n) = n - Sum_{k\u003e=1} floor(n/2^k) = n - A011371(n). - _Benoit Cloitre_, Dec 19 2002",
				"G.f.: (1/(1-x)) * Sum_{k\u003e=0} x^(2^k)/(1+x^(2^k)). - _Ralf Stephan_, Apr 19 2003",
				"a(0) = 0, a(n) = a(n - 2^floor(log_2(n))) + 1. Examples: a(6) = a(6 - 2^2) + 1 = a(2) + 1 = a(2 - 2^1) + 1 + 1 = a(0) + 2 = 2; a(101) = a(101 - 2^6) + 1 = a(37) + 1 = a(37 - 2^5) + 2 = a(5 - 2^2) + 3 = a(1 - 2^0) + 4 = a(0) + 4 = 4; a(6275) = a(6275 - 2^12) + 1 = a(2179 - 2^11) + 2 = a(131 - 2^7) + 3 = a(3 - 2^1) + 4 = a(1 - 2^0) + 5 = 5; a(4129) = a(4129 - 2^12) + 1 = a(33 - 2^5) + 2 = a(1 - 2^0) + 3 = 3. - _Hieronymus Fischer_, Jan 22 2006",
				"A fixed point of the mapping 0 -\u003e 01, 1 -\u003e 12, 2 -\u003e 23, 3 -\u003e 34, 4 -\u003e 45, ... With f(i) = floor(n/2^i), a(n) is the number of odd numbers in the sequence f(0), f(1), f(2), f(3), f(4), f(5), ... - _Philippe Deléham_, Jan 04 2004",
				"When read mod 2 gives the Morse-Thue sequence A010060.",
				"Let floor_pow4(n) denote n rounded down to the next power of four, floor_pow4(n) = 4 ^ floor(log4 n). Then a(0) = 0, a(1) = 1, a(2) = 1, a(3) = 2, a(n) = a(floor(n / floor_pow4(n))) + a(n % floor_pow4(n)). - Stephen K. Touset (stephen(AT)touset.org), Apr 04 2007",
				"a(n) = n - Sum_{k=2..n} Sum_{j|n, j \u003e= 2} (floor(log_2(j)) - floor(log_2(j-1))). - _Hieronymus Fischer_, Jun 18 2007",
				"a(n) = A138530(n, 2) for n \u003e 1. - _Reinhard Zumkeller_, Mar 26 2008",
				"a(A077436(n)) = A159918(A077436(n)); a(A000290(n)) = A159918(n). - _Reinhard Zumkeller_, Apr 25 2009",
				"a(n) = A063787(n) - A007814(n). - _Gary W. Adamson_, Jun 04 2009",
				"a(n) = A007814(C(2n, n)) = 1 + A007814(C(2n-1, n)). - _Vladimir Shevelev_, Jul 20 2009",
				"For odd m \u003e= 1, a((4^m-1)/3) = a((2^m+1)/3) + (m-1)/2 (mod 2). - _Vladimir Shevelev_, Sep 03 2010",
				"a(n) - a(n-1) = { 1 - a(n-1) if and only if A007814(n) = a(n-1), 1 if and only if A007814(n) = 0, -1 for all other A007814(n) }. - Brent Spillner (spillner(AT)acm.org), Sep 01 2010",
				"a(A001317(n)) = 2^a(n). - _Vladimir Shevelev_, Oct 25 2010",
				"a(n) = A139351(n) + A139352(n) = Sum_k {A030308(n, k)}. - _Philippe Deléham_, Oct 14 2011",
				"From _Hieronymus Fischer_, Jun 10 2012: (Start)",
				"  a(n) = Sum_{j = 1..m+1} (floor(n/2^j + 1/2)) - floor(n/2^j)), where m = floor(log_2(n)).",
				"General formulas for the number of digits \u003e= d in the base p representation of n, where 1 \u003c= d \u003c p: a(n) = Sum_{j = 1..m+1} (floor(n/p^j + (p-d)/p) - floor(n/p^j)), where m=floor(log_p(n)); g.f.: g(x) = (1/(1-x))*Sum_{j\u003e=0} (x^(d*p^j) - x^(p*p^j))/(1-x^(p*p^j)). (End)",
				"a(n) = A213629(n, 1) for n \u003e 0. - _Reinhard Zumkeller_, Jul 04 2012",
				"a(n) = A240857(n,n). - _Reinhard Zumkeller_, Apr 14 2014",
				"a(n) = log_2(C(2*n,n) - (C(2*n,n) AND C(2*n,n)-1)). - _Gary Detlefs_, Jul 10 2014",
				"Sum_{n \u003e= 1} a(n)/2n(2n+1) = (gamma + log(4/Pi))/2 = A344716, where gamma is Euler's constant A001620; see Sondow 2005, 2010 and Allouche, Shallit, Sondow 2007. - _Jonathan Sondow_, Mar 21 2015",
				"For any integer base b \u003e= 2, the sum of digits s_b(n) of expansion base b of n is the solution of this recurrence relation: s_b(n) = 0 if n = 0 and s_b(n) = s_b(floor(n/b)) + (n mod b). Thus, a(n) satisfies: a(n) = 0 if n = 0 and a(n) = a(floor(n/2)) + (n mod 2). This easily yields a(n) = Sum_{i = 0..floor(log_2(n))} (floor(n/2^i) mod 2). From that one can compute a(n) = n - Sum_{i = 1..floor(log_2(n))} floor(n/2^i). - _Marek A. Suchenek_, Mar 31 2016",
				"Sum_{k\u003e=1} a(k)/2^k = 2 * Sum_{k \u003e= 0} 1/(2^(2^k)+1) = 2 * A051158. - _Amiram Eldar_, May 15 2020",
				"Sum_{k\u003e=1} a(k)/(k*(k+1)) = A016627 = log(4). - _Bernard Schott_, Sep 16 2020",
				"a(m*(2^n-1)) \u003e= n. Equality holds when 2^n-1 \u003e= A000265(m), but also in some other cases, e.g., a(11*(2^2-1)) = 2 and a(19*(2^3-1)) = 3. - _Pontus von Brömssen_, Dec 13 2020"
			],
			"example": [
				"Using the formula a(n) = a(floor(n / floor_pow4(n))) + a(n % floor_pow4(n)):",
				"  a(4) = a(1) + a(0) = 1",
				"  a(8) = a(2) + a(0) = 1",
				"  a(13) = a(3) + a(1) = 2 + 1 = 3",
				"  a(23) = a(1) + a(7) = 1 + a(1) + a(3) = 1 + 1 + 2 = 4",
				"_Gary W. Adamson_ points out (Jun 03 2009) that this can be written as a triangle:",
				"  0,",
				"  1,",
				"  1,2,",
				"  1,2,2,3,",
				"  1,2,2,3,2,3,3,4,",
				"  1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,",
				"  1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,",
				"  1,2,2,3,2,3,...",
				"where the rows converge to A063787.",
				"From _Omar E. Pol_, Jun 07 2009: (Start)",
				"Also, triangle begins:",
				"  0;",
				"  1,1;",
				"  2,1,2,2;",
				"  3,1,2,2,3,2,3,3;",
				"  4,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4;",
				"  5,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5;",
				"  6,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,2,3,3,4,...",
				"(End)",
				"From _Joerg Arndt_, Nov 09 2012: (Start)",
				"Connection to the compositions of n as lists of parts (see comment):",
				"[ #]:   a(n)  composition",
				"[ 0]:   [0]   1 1 1 1 1",
				"[ 1]:   [1]   1 1 1 2",
				"[ 2]:   [1]   1 1 2 1",
				"[ 3]:   [2]   1 1 3",
				"[ 4]:   [1]   1 2 1 1",
				"[ 5]:   [2]   1 2 2",
				"[ 6]:   [2]   1 3 1",
				"[ 7]:   [3]   1 4",
				"[ 8]:   [1]   2 1 1 1",
				"[ 9]:   [2]   2 1 2",
				"[10]:   [2]   2 2 1",
				"[11]:   [3]   2 3",
				"[12]:   [2]   3 1 1",
				"[13]:   [3]   3 2",
				"[14]:   [3]   4 1",
				"[15]:   [4]   5",
				"(End)"
			],
			"maple": [
				"A000120 := proc(n) local w,m,i; w := 0; m := n; while m \u003e 0 do i := m mod 2; w := w+i; m := (m-i)/2; od; w; end: wt := A000120;",
				"A000120 := n -\u003e add(i, i=convert(n,base,2)): # _Peter Luschny_, Feb 03 2011",
				"with(Bits): p:=n-\u003eilog2(n-And(n,n-1)): seq(p(binomial(2*n,n)),n=0..200) # _Gary Detlefs_, Jan 27 2019"
			],
			"mathematica": [
				"Table[DigitCount[n, 2, 1], {n, 0, 105}]",
				"Nest[Flatten[# /. # -\u003e {#, # + 1}] \u0026, {0}, 7] (* _Robert G. Wilson v_, Sep 27 2011 *)",
				"Table[Plus @@ IntegerDigits[n, 2], {n, 0, 104}]",
				"Nest[Join[#, # + 1] \u0026, {0}, 7] (* _IWABUCHI Yu(u)ki_, Jul 19 2012 *)",
				"Log[2, Nest[Join[#, 2#] \u0026, {1}, 14]] (* gives 2^14 term, _Carlos Alves_, Mar 30 2014 *)"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, 2*n - valuation((2*n)!, 2))};",
				"(PARI) {a(n) = if( n\u003c0, 0, subst(Pol(binary(n)), x ,1))};",
				"(PARI) {a(n) = if( n\u003c1, 0, a(n\\2) + n%2)}; /* _Michael Somos_, Mar 06 2004 */",
				"(PARI) a(n)=my(v=binary(n));sum(i=1,#v,v[i]) \\\\ _Charles R Greathouse IV_, Jun 24 2011",
				"(PARI) a(n)=norml2(binary(n)) \\\\ better use {A000120=hammingweight}. - _M. F. Hasler_, Oct 09 2012, edited Feb 27 2020",
				"(PARI) a(n)=hammingweight(n) \\\\ _Michel Marcus_, Oct 19 2013",
				"(Common LISP) (defun floor-to-power (n pow) (declare (fixnum pow)) (expt pow (floor (log n pow)))) (defun enabled-bits (n) (if (\u003c n 4) (n-th n (list 0 1 1 2)) (+ (enabled-bits (floor (/ n (floor-to-power n 4)))) (enabled-bits (mod n (floor-to-power n 4)))))) ; Stephen K. Touset (stephen(AT)touset.org), Apr 04 2007",
				"See link in A139351 for Fortran program.",
				"(Haskell)",
				"import Data.Bits (Bits, popCount)",
				"a000120 :: (Integral t, Bits t) =\u003e t -\u003e Int",
				"a000120 = popCount",
				"a000120_list = 0 : c [1] where c (x:xs) = x : c (xs ++ [x,x+1])",
				"-- _Reinhard Zumkeller_, Aug 26 2013, Feb 19 2012, Jun 16 2011, Mar 07 2011",
				"(Haskell)",
				"a000120 = concat r",
				"    where r = [0] : (map.map) (+1) (scanl1 (++) r)",
				"-- _Luke Palmer_, Feb 16 2014",
				"(Sage)",
				"def A000120(n):",
				"    if n \u003c= 1: return Integer(n)",
				"    return A000120(n//2) + n%2",
				"[A000120(n) for n in range(105)]  # _Peter Luschny_, Nov 19 2012",
				"(Sage) def A000120(n) : return sum(n.digits(2)) # _Eric M. Schmidt_, Apr 26 2013",
				"(Python) def A000120(n): return bin(n).count('1') # _Chai Wah Wu_, Sep 03 2014",
				"(Scala) (0 to 127).map(Integer.bitCount(_)) // _Alonso del Arte_, Mar 05 2019",
				"(MAGMA) [Multiplicity(Intseq(n, 2), 1): n in [0..104]]; // _Marius A. Burtea_, Jan 22 2020",
				"(MAGMA) [\u0026+Intseq(n, 2):n in [0..104]]; // _Marius A. Burtea_, Jan 22 2020"
			],
			"xref": [
				"The basic sequences concerning the binary expansion of n are this one, A000788, A000069, A001969, A023416, A059015, A007088.",
				"Partial sums see A000788. For run lengths see A131534. See also A001792, A010062.",
				"Number of 0's in n: A023416 and A080791.",
				"a(n) = n - A011371(n).",
				"Sum of digits of n written in bases 2-16: this sequence, A053735, A053737, A053824, A053827, A053828, A053829, A053830, A007953, A053831, A053832, A053833, A053834, A053835, A053836.",
				"Cf. A001620, A344716, A007814.",
				"This is Guy Steele's sequence GS(3, 4) (see A135416).",
				"Cf. A055640, A055641, A102669-A102685, A117804, A122840, A122841, A160093, A160094, A196563, A196564 (for base 10).",
				"Cf. A230952 (boustrophedon transform).",
				"Cf. A070939 (length of binary representation of n)."
			],
			"keyword": "nonn,easy,core,nice,hear,look,base",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"references": 1518,
			"revision": 395,
			"time": "2021-10-30T10:43:55-04:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}