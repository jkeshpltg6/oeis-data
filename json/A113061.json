{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A113061",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 113061,
			"data": "1,1,1,1,1,1,1,9,1,1,1,1,1,1,1,9,1,1,1,1,1,1,1,9,1,1,28,1,1,1,1,9,1,1,1,1,1,1,1,9,1,1,1,1,1,1,1,9,1,1,1,1,1,28,1,9,1,1,1,1,1,1,1,73,1,1,1,1,1,1,1,9,1,1,1,1,1,1,1,9,28,1,1,1,1,1,1,9,1,1,1,1,1,1,1,9,1,1,1,1,1,1,1,9",
			"name": "Sum of cube divisors of n.",
			"comment": [
				"Multiplicative with a(p^e) = (p^(3*(1+floor(e/3)))-1)/(p^3-1). The Dirichlet generating function is zeta(s)*zeta(3s-3). The sequence is the inverse Mobius transform of n*A010057(n). - _R. J. Mathar_, Feb 18 2011"
			],
			"link": [
				"Antti Karttunen, \u003ca href=\"/A113061/b113061.txt\"\u003eTable of n, a(n) for n = 1..19683\u003c/a\u003e",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/1106.4038\"\u003eSurvey of Dirichlet series of multiplicative arithmetic functions\u003c/a\u003e, arXiv:1106.4038 [math.NT] (2011), Remark 15.",
				"\u003ca href=\"/index/Su#sums_of_divisors\"\u003eIndex entries for sequences related to sums of divisors\u003c/a\u003e"
			],
			"formula": [
				"G.f.: Sum_{k\u003e=1} k^3*x^(k^3)/(1 - x^(k^3)). - _Ilya Gutkovskiy_, Dec 24 2016",
				"a(n) = Sum_{d|n} A010057(d)*d. - _Antti Karttunen_, Oct 08 2017",
				"Sum_{k=1..n} a(k) ~ zeta(4/3)*n^(4/3)/4 - n/2. - _Vaclav Kotesovec_, Dec 01 2020"
			],
			"maple": [
				"A113061 := proc(n)",
				"    local a,pe,p,e;",
				"    a := 1;",
				"    for pe in ifactors(n)[2] do",
				"        p := pe[1] ;",
				"        e := pe[2] ;",
				"        e := 3*(1+floor(e/3)) ;",
				"        a := a*(p^e-1)/(p^3-1) ;",
				"    end do:",
				"    a ;",
				"end proc:",
				"seq(A113061(n),n=1..100) ; # _R. J. Mathar_, Oct 08 2017"
			],
			"mathematica": [
				"a[n_] := Sum[If[IntegerQ[d^(1/3)], d, 0], {d, Divisors[n]}];",
				"Array[a, 100] (* _Jean-François Alcover_, Nov 25 2017 *)",
				"f[p_, e_] := (p^(3*(1 + Floor[e/3])) - 1)/(p^3 - 1); a[1] = 1; a[n_] := Times @@ f @@@ FactorInteger[n]; Array[a, 100] (* _Amiram Eldar_, May 01 2020 *)"
			],
			"program": [
				"(PARI) A113061(n) = sumdiv(n,d,ispower(d,3)*d); \\\\ _Antti Karttunen_, Oct 08 2017",
				"(Scheme)",
				";; With memoization-macro definec, after the multiplicative formula of _R. J. Mathar_:",
				"(definec (A113061 n) (if (= 1 n) n (let ((p (A020639 n)) (e (A067029 n))) (* (/ (+ -1 (expt p (* 3 (+ 1 (A002264 e))))) (+ -1 (expt p 3))) (A113061 (A028234 n)))))) ;; _Antti Karttunen_, Oct 08 2017"
			],
			"xref": [
				"Cf. A004709, A010057, A035316."
			],
			"keyword": "nonn,mult",
			"offset": "1,8",
			"author": "_Paul Barry_, Oct 13 2005",
			"references": 9,
			"revision": 31,
			"time": "2020-12-01T02:59:05-05:00",
			"created": "2006-01-24T03:00:00-05:00"
		}
	]
}