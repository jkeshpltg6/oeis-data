{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A225575",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 225575,
			"data": "199,233,257,353,523,653,971,1973,2333,3259,3637,3761,4283,4993,5927,6353,6529,6563,7907,8831,9293,9851,10711,10861,11731,13037,13177,13681,15241,16381,16693,16931,18341,18899,19577,21787,23857,24071,28621,31657,32911",
			"name": "Primes p such that if q is the next prime after p then the concatenation of p with q and the concatenation of q with p are both primes.",
			"comment": [
				"This sequence was suggested by Puzzle 687 at Prime Puzzles (see Links).",
				"If q = p + 2 (that is, p is the lesser of a twin prime, A001359), then p is not in the sequence. - _Alonso del Arte_, May 10 2013",
				"In general, q-p == 0 (mod 6). - _Zak Seidov_, May 11 2013"
			],
			"link": [
				"Marius A. Burtea, \u003ca href=\"/A225575/b225575.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e (terms 1..250 from Paolo P. Lava)",
				"Carlos Rivera, \u003ca href=\"http://www.primepuzzles.net/puzzles/puzz_687.htm\"\u003ePuzzle 687: 17769643\u003c/a\u003e, The Prime Puzzles \u0026 Problems Connection."
			],
			"example": [
				"The prime following 199 is 211 and both 199211 and 211199 are prime."
			],
			"maple": [
				"a:=NULL;",
				"for i from 1 to 2000 do",
				"  p:=ithprime(i);",
				"  q:=nextprime(p);",
				"  s:=convert(p,string);",
				"  t:=convert(q,string);",
				"  if isprime(parse(cat(s,t))) and isprime(parse(cat(t,s))) then a:=a,p; fi;",
				"od:",
				"a;"
			],
			"mathematica": [
				"concatQ[{a_,b_}]:=Module[{idna=IntegerDigits[a],idnb=IntegerDigits[b]},AllTrue[ FromDigits/@ {Join[idna,idnb], Join[idnb,idna]},PrimeQ]]; Transpose[Select[ Partition[ Prime[Range[2000]],2,1],concatQ]][[1]] (* The program uses the AllTrue function from Mathematica version 10 *) (* _Harvey P. Dale_, Jul 16 2015 *)"
			],
			"program": [
				"(MAGMA) conc:=func\u003ca,b|a*10^(#Intseq(b))+b\u003e; [p:p in PrimesUpTo(17000)| IsPrime(conc(p,(NextPrime(p)))) and IsPrime(conc(NextPrime(p),p))]; // _Marius A. Burtea_, Jan 25 2020"
			],
			"xref": [
				"Cf. A030459."
			],
			"keyword": "nonn,base",
			"offset": "1,1",
			"author": "_W. Edwin Clark_, May 10 2013",
			"references": 1,
			"revision": 32,
			"time": "2020-02-19T09:33:16-05:00",
			"created": "2013-05-12T11:02:32-04:00"
		}
	]
}