{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A318191",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 318191,
			"data": "1,1,1,1,1,1,1,1,1,1,1,2,2,1,1,1,6,12,4,1,1,1,24,180,72,8,1,1,1,120,4680,5400,432,16,1,1,1,720,187200,914400,162000,2592,32,1,1,1,5040,10634400,296438400,178660800,4860000,15552,64,1,1,1,40320,813664800,162273628800,469551168000,34907788800,145800000,93312,128,1,1",
			"name": "Number A(n,k) of lattice paths from {n}^k to {0}^k using steps that decrement one component by 1 such that for each point p we have abs(p_{i}-p_{(i mod k)+1}) \u003c= 1 and the first component used is p_1; square array A(n,k), n\u003e=0, k\u003e=0, read by antidiagonals.",
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A318191/b318191.txt\"\u003eAntidiagonals n = 0..20, flattened\u003c/a\u003e",
				"Juan B. Gil, Jordan O. Tirrell, \u003ca href=\"https://arxiv.org/abs/1806.09065\"\u003eA simple bijection for classical and enhanced k-noncrossing partitions\u003c/a\u003e, arXiv:1806.09065 [math.CO], 2018. Also Discrete Mathematics (2019) Article 111705. doi:10.1016/j.disc.2019.111705"
			],
			"example": [
				"A(2,2) = 2^2 = 4:",
				"                    (0,1)",
				"                   /     \\",
				"  (2,2)-(1,2)-(1,1)       (0,0)",
				"                   \\     /",
				"                    (1,0)",
				"Square array A(n,k) begins:",
				"  1, 1,  1,     1,         1,             1,                   1, ...",
				"  1, 1,  1,     2,         6,            24,                 120, ...",
				"  1, 1,  2,    12,       180,          4680,              187200, ...",
				"  1, 1,  4,    72,      5400,        914400,           296438400, ...",
				"  1, 1,  8,   432,    162000,     178660800,        469551168000, ...",
				"  1, 1, 16,  2592,   4860000,   34907788800,     743761386086400, ...",
				"  1, 1, 32, 15552, 145800000, 6820487308800, 1178106009360998400, ..."
			],
			"maple": [
				"b:= proc(l) option remember; (n-\u003e `if`(n\u003c2 or max(l[])=0, 1,",
				"      add(`if`(l[i]=0 or 1\u003cabs(l[`if`(i=1, 0, i)-1]-l[i]+1)",
				"       or 1\u003cabs(l[`if`(i=n, 0, i)+1]-l[i]+1), 0,",
				"      b(subsop(i=l[i]-1, l))), i=1..n)))(nops(l))",
				"    end:",
				"A:= (n, k)-\u003e `if`(k\u003c2 or n=0, 1, b([n-1, n$k-1])):",
				"seq(seq(A(n, d-n), n=0..d), d=0..10);"
			],
			"mathematica": [
				"b[l_] := b[l] = With[{n = Length[l]}, If[n \u003c 2 || Max[l ] == 0, 1, Sum[If[ l[[i]] == 0 ||1 \u003c Abs[l[[If[i == 1, 0, i] - 1]] - l[[i]] + 1] || 1 \u003c Abs[l[[If[i == n, 0, i] + 1]] - l[[i]] + 1], 0, b[ReplacePart[l, i -\u003e l[[i]] - 1]]], {i, n}]]];",
				"A[n_, k_] :=  If[k \u003c 2 || n == 0, 1, b[Join[{n - 1}, Table[n, {k - 1}]]]];",
				"Table[A[n, d - n], {d, 0, 10}, {n, 0, d}] // Flatten (* _Jean-François Alcover_, May 13 2020, after Maple *)"
			],
			"xref": [
				"Columns k=0+1, 2 give: A000012, A011782.",
				"Rows n=0-2 give: A000012, A000142(n-1) for n\u003e0, A322782/n for n\u003e0.",
				"Main diagonal gives A320443.",
				"Cf. A227655."
			],
			"keyword": "nonn,tabl",
			"offset": "0,12",
			"author": "_Alois P. Heinz_, Jan 07 2019",
			"references": 4,
			"revision": 24,
			"time": "2020-05-24T00:13:59-04:00",
			"created": "2019-01-09T15:31:21-05:00"
		}
	]
}