{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A055302",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 55302,
			"data": "1,2,0,6,3,0,24,36,4,0,120,360,140,5,0,720,3600,3000,450,6,0,5040,37800,54600,18900,1302,7,0,40320,423360,940800,588000,101136,3528,8,0,362880,5080320,16087680,15876000,5143824,486864,9144,9,0,3628800",
			"name": "Triangle of number of labeled rooted trees with n nodes and k leaves, n \u003e= 1, 1 \u003c= k \u003c= n.",
			"comment": [
				"Beginning with the second row, dividing each row by n gives the mirror of row n-1 of A141618. Under the exponential transform, the mirror of A141618 is generated, relating the number of connected graphs here to the number of disconnected graphs associated with A141618 (cf. A127671 and A036040). - _Tom Copeland_, Oct 25 2014"
			],
			"reference": [
				"Miklos Bona, editor, Handbook of Enumerative Combinatorics, CRC Press, 2015, page 313."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A055302/b055302.txt\"\u003eRows n = 1..141, flattened\u003c/a\u003e",
				"N. J. A. Sloane, \u003ca href=\"/transforms.txt\"\u003eTransforms\u003c/a\u003e",
				"\u003ca href=\"/index/Ro#rooted\"\u003eIndex entries for sequences related to rooted trees\u003c/a\u003e"
			],
			"formula": [
				"E.g.f. (relative to x) satisfies: A(x,y) = xy + x*exp(A(x,y)) - x. Divides by n and shifts up under exponential transform.",
				"T(n,k) = (n!/k!)*Stirling2(n-1, n-k). - _Vladeta Jovovic_, Jan 28 2004",
				"T(n,k) = A055314(n,k)*(n-k) + A055314(n,k+1)*(k+1). The first term is the number of such trees with root degree \u003e 1 while the second term is the number of such trees with root degree = 1. This simplifies to the above formula by Vladeta Jovovic. - _Geoffrey Critzer_, Dec 01 2012",
				"E.g.f.: G(x,t) = log[1 + t * N(x*t,1/t)], where N(x,t) is the e.g.f. of A141618. Also, G(x*t,1/t)= log[1 + N(x,t)/t] is the comp. inverse in x of x / [1 + t * (e^x - 1)]. - _Tom Copeland_, Oct 26 2014"
			],
			"example": [
				"Triangle begins",
				"     1,",
				"     2,     0;",
				"     6,     3,     0;",
				"    24,    36,     4,     0;",
				"   120,   360,   140,     5,    0;",
				"   720,  3600,  3000,   450,    6, 0;",
				"  5040, 37800, 54600, 18900, 1302, 7, 0;"
			],
			"maple": [
				"T:= (n, k)-\u003e (n!/k!)*Stirling2(n-1, n-k):",
				"seq(seq(T(n, k), k=1..n), n=1..10);  # _Alois P. Heinz_, Nov 13 2013"
			],
			"mathematica": [
				"Table[Table[n!/k! StirlingS2[n-1,n-k], {k,1,n}], {n,0,10}]//Grid  (* _Geoffrey Critzer_, Dec 01 2012 *)"
			],
			"program": [
				"(PARI)",
				"A055302(n,k)=n!/k!*stirling(n-1, n-k,2);",
				"for(n=1,10,for(k=1,n,print1(A055302(n,k),\", \"));print());",
				"\\\\ _Joerg Arndt_, Oct 27 2014"
			],
			"xref": [
				"Row sums give A000169. Columns 1 through 12: A000142, A055303-A055313. Cf. A055314.",
				"Cf. A248120 for a natural refinement."
			],
			"keyword": "nonn,tabl,eigen",
			"offset": "1,2",
			"author": "_Christian G. Bower_, May 11 2000",
			"references": 25,
			"revision": 54,
			"time": "2018-12-25T22:46:17-05:00",
			"created": "2000-06-15T03:00:00-04:00"
		}
	]
}