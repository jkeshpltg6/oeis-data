{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A139382",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 139382,
			"data": "1,1,1,1,4,1,1,13,11,1,1,40,90,26,1,1,121,670,480,57,1,1,364,4811,7870,2247,120,1,1,1093,34041,122861,77527,9807,247,1,1,3280,239380,1876956,2526198,695368,41176,502,1,1,9841,1678940,28393720,80189094,46334382,5924720,169186,1013,1",
			"name": "Triangle read by rows, T(n,k) = (2^k-1) * T(n-1,k) + T(n-1,k-1).",
			"comment": [
				"Row sums = A135922 starting with offset 1: (1, 2, 6, 26, 158, 1330, ...).",
				"This triangle is the q-analog of A008277 (Stirling numbers of the 2nd kind) for q=2 (see Cai et al. link). - _Werner Schulte_, Apr 04 2019"
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A139382/b139382.txt\"\u003eRows n = 1..100 of triangle, flattened\u003c/a\u003e",
				"Yue Cai, Richard Ehrenborg, Margaret A. Readdy, \u003ca href=\"https://arxiv.org/abs/1706.06623\"\u003eq-Stirling numbers revisited\u003c/a\u003e, arXiv:1706.06623 [math.CO], 2017.",
				"Yue Cai, Richard Ehrenborg, Margaret A. Readdy, \u003ca href=\"https://doi.org/10.37236/6829\"\u003eq-Stirling numbers revisited\u003c/a\u003e, Electronic Journal of Combinatorics, 25 Issue 1 (2018), Paper #P1.37."
			],
			"formula": [
				"Triangle read by rows, T(n,k) = (2^k-1) * T(n-1,k) + (T(n-1,k-1). Let X = an infinite bidiagonal matrix with (1,3,7,15,31...) in the main diagonal and (1,1,1,...) in the subdiagonal. n-th row of the triangle = X^n * [1,0,0,0,...].",
				"From _Werner Schulte_, Apr 02 2019: (Start)",
				"G.f. of column k: col(k,t) = Sum_{n\u003e=k} T(n,k)*t^n = t^k/Product_{i=1..k} (1 - (2^i-1)*t) for k \u003e 0.",
				"Sum_{k\u003e0} col(k,t) * (Product_{i=1..k-1} (1 - 2^i)) = t (empty product equals 1).",
				"Sum_{k=1..n} (-1)^k * 2^binomial(k,2) * T(n,k) = (-1)^n for n \u003e 0.",
				"An example for k=3: G.f. of column 3: col(3,t) = Sum_{n\u003e=3} T(n,3) * t^n = 1 * t^3 + 11 * t^4 + 90 * t^5 + 670 * t^6 + . . . = t^3 * ( 1 + 11 * t + 90 * t^2 + 670 * t^3 + . . . ) = t^3 / Product_{i=1..3} ( 1 - ( 2^i - 1 ) * t ) = t^3 / ( ( 1 - t ) * ( 1 - 3 * t ) * ( 1 - 7 * t ) ) = t^3 / ( 1 - 11 * t + 31 * t^2 - 21 * t^3 ). Perhaps the following recurrence formula is useful too: col(k,t) = col(k-1,t) * t / ( 1 - ( 2^k - 1 ) * t ) for k \u003e 1 with initial value col(1,t) = t / ( 1 - t ). Finally: col(k,t) is the g.f. of column k.",
				"With regard to the 2nd formula: We can it replace with the following formula: Sum_{k=1..n} T(n,k) * (Product_{i=1..k-1} (1-2^i)) = A000007(n-1) for n \u003e 0 with empty product 1 (case k=1). Example for n=5: 1*1 + (-1)*40 + (-1)*(-3)*90 + (-1)*(-3)*(-7)*26 + (-1)*(-3)*(-7)*(-15)*1 = 0.",
				"(End)"
			],
			"example": [
				"First few rows of the triangle are:",
				"  1;",
				"  1,   1;",
				"  1,   4,   1;",
				"  1,  13,  11,   1;",
				"  1,  40,  90,  26,   1;",
				"  1, 121, 670, 480,  57,   1;",
				"  ...",
				"a(13) = T(5,3) = 90 = (2^3 - 1)* T(4,3) + T(4,2) = 7*11 + 13."
			],
			"mathematica": [
				"T[1, 1]:= 1; T[n_, k_]:= T[n, k] = If[k \u003e n || k \u003c 1, 0, (2^k-1)*T[n-1, k] + T[n-1, k-1]]; Table[T[n, k], {n, 1, 12}, {k, 1, n}] (* _G. C. Greubel_, Apr 02 2019 *)"
			],
			"program": [
				"(PARI) {T(n,k) = if(k\u003c1 || k\u003en, 0, if(n==1 \u0026\u0026 k==1, 1, (2^k-1)*T(n-1,k) + T(n-1,k-1)))};",
				"for(n=1,12, for(k=1,n, print1(T(n,k), \", \"))) \\\\ _G. C. Greubel_, Apr 02 2019",
				"(Sage)",
				"@CachedFunction",
				"def T(n, k):",
				"   if (k==1): return 1",
				"   elif (k==n): return 1",
				"   else: return (2^k-1)*T(n-1, k) + T(n-1, k-1)",
				"[[T(n, k) for k in (1..n)] for n in (1..12)] # _G. C. Greubel_, Apr 02 2019",
				"# Alternative:",
				"(Sage) # uses[qStirling2 from A333143]",
				"seq(seq(qStirling2(n, k, 2), k=0..n), n=0..9); # _Peter Luschny_, Mar 10 2020"
			],
			"xref": [
				"Cf. A008277, A135922, A333143."
			],
			"keyword": "nonn,tabl",
			"offset": "1,5",
			"author": "_Gary W. Adamson_, Apr 16 2008",
			"ext": [
				"More terms from _G. C. Greubel_, Apr 02 2019"
			],
			"references": 7,
			"revision": 25,
			"time": "2020-03-23T17:30:44-04:00",
			"created": "2008-06-29T03:00:00-04:00"
		}
	]
}