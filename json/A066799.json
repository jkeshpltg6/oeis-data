{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A066799",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 66799,
			"data": "1,1,1,1,1,1,1,1,2,1,1,1,1,1,1,1,1,1,2,4,1,1,1,2,1,4,2,1,1,1,1,1,2,1,3,1,1,1,1,1,1,1,6,1,1,1,1,2,2,1,2,3,2,6,1,1,1,1,1,4,1,6,1,1,4,1,1,1,1,1,4,1,2,2,3,4,10,1,1,1,2,1,2,2,1,1,6,2,5,2,1,1,1,1,2,1,1,1,2,1,1,5,2,12",
			"name": "Square array read by antidiagonals of eventual period of powers of k mod n; period of repeating digits of 1/n in base k.",
			"comment": [
				"The determinant of the n X n matrix made from the northwest corner of this array is 0^(n-1). - _Iain Fox_, Mar 12 2018"
			],
			"link": [
				"Iain Fox, \u003ca href=\"/A066799/b066799.txt\"\u003eAntidiagonals n = 1..141 of array, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k) = T(n, k-n) if k \u003e n.",
				"T(n, n) = T(n, n+1) = 1.",
				"T(n, n-1) = 2."
			],
			"example": [
				"Rows start: 1,1,1,1,1,...; 1,1,1,1,1,...; 1,2,1,1,2,...; 1,1,2,1,1; 1,4,4,2,1,... T(3,2)=2 since the powers of 2 become 1,2,1,2,1,2,... mod 3 with period 2. T(4,2)=1 since the powers of 2 become 1,2,0,0,0,0,... mod 4 with eventual period 1.",
				"Beginning of array:",
				"+-----+--------------------------------------------------------------------",
				"| n\\k |  1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  ...",
				"+-----+--------------------------------------------------------------------",
				"|  1  |  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1, ...",
				"|  2  |  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1, ...",
				"|  3  |  1,  2,  1,  1,  2,  1,  1,  2,  1,  1,  2,  1,  1,  2,  1,  1, ...",
				"|  4  |  1,  1,  2,  1,  1,  1,  2,  1,  1,  1,  2,  1,  1,  1,  2,  1, ...",
				"|  5  |  1,  4,  4,  2,  1,  1,  4,  4,  2,  1,  1,  4,  4,  2,  1,  1, ...",
				"|  6  |  1,  2,  1,  1,  2,  1,  1,  2,  1,  1,  2,  1,  1,  2,  1,  1, ...",
				"|  7  |  1,  3,  6,  3,  6,  2,  1,  1,  3,  6,  3,  6,  2,  1,  1,  3, ...",
				"|  8  |  1,  1,  2,  1,  2,  1,  2,  1,  1,  1,  2,  1,  2,  1,  2,  1, ...",
				"| ... |"
			],
			"mathematica": [
				"t[n_, k_] := For[p = PowerMod[k, n, n]; m = n + 1, True, m++, If[PowerMod[k, m, n] == p, Return[m - n]]]; Flatten[Table[t[n - k + 1, k], {n, 1, 14}, {k, n, 1, -1}]] (* _Jean-François Alcover_, Jun 04 2012 *)"
			],
			"program": [
				"(PARI) a(n, k) = my(p=k^n%n); for(m=n+1, +oo, if(k^m%n==p, return(m-n))) \\\\ _Iain Fox_, Mar 12 2018"
			],
			"xref": [
				"Columns are A000012, A007733, A007734, A007735, A007736, A007737, A007738, A007739, A007740, A007732. A002322 is the highest value in each row and the least common multiple of each row, while the number of distinct values in each row is A066800."
			],
			"keyword": "nonn,tabl,base",
			"offset": "1,9",
			"author": "_Henry Bottomley_, Dec 20 2001",
			"references": 8,
			"revision": 25,
			"time": "2019-08-18T16:38:17-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}