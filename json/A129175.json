{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A129175",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 129175,
			"data": "1,1,1,0,1,1,0,1,1,1,0,1,1,0,1,1,2,1,2,1,2,1,1,0,1,1,0,1,1,2,2,3,2,4,3,4,3,4,2,3,2,2,1,1,0,1,1,0,1,1,2,2,4,3,5,5,7,6,9,7,9,8,9,7,9,6,7,5,5,3,4,2,2,1,1,0,1,1,0,1,1,2,2,4,4,6,6,9,9,13,12,16,16,19,18,22,20,23,21,23",
			"name": "Triangle read by rows: the q-analog of the Catalan numbers.",
			"comment": [
				"Previous name: T(n,k) is the number of Dyck words of length 2n having major index k (n \u003e= 0, k \u003e= 0). A Dyck word of length 2n is a word of n 0's and n 1's for which no initial segment contains more 1's than 0's. The major index of a Dyck word is the sum of the positions of those 1's that are followed by a 0.",
				"Representing a Dyck word p of length 2n as a Dyck path p', the major index of p is equal to the sum of the abscissae of the valleys of p'.",
				"Row n has 1+n*(n-1) terms.",
				"Row sums are the Catalan numbers (A000108).",
				"T(n,k) = T(n,n^2-n-k) (i.e., rows are palindromic).",
				"Alternating row sums are the central binomial coefficients binomial(n, floor(n/2)) = A001405(n).",
				"Sum_{k=0..n*(n-1)} k*T(n,k) = A002740(n+1).",
				"T(n,k) = A129174(n,n+k) (i.e., except for the initial 0's, rows of A129174 and A129175 are the same)."
			],
			"reference": [
				"G. E. Andrews, The Theory of Partitions, Addison-Wesley, 1976.",
				"R. P. Stanley, Enumerative Combinatorics, Cambridge, Vol. 2, 1999; see p. 236, Exercise 6.34 b. [From _Emeric Deutsch_, Nov 05 2008]"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A129175/b129175.txt\"\u003eRows n = 0..32, flattened\u003c/a\u003e",
				"FindStat - Combinatorial Statistic Finder, \u003ca href=\"http://www.findstat.org/StatisticsDatabase/St000027\"\u003eThe major index of a Dyck path.\u003c/a\u003e",
				"J. Furlinger and J. Hofbauer, \u003ca href=\"http://dx.doi.org/10.1016/0097-3165(85)90089-5\"\u003eq-Catalan numbers\u003c/a\u003e, J. Comb. Theory, A, 40, 248-264, 1985.",
				"M. Shattuck, \u003ca href=\"http://www.emis.de/journals/INTEGERS/papers/f7/f7.Abstract.html\"\u003eParity theorems for statistics on permutations and Catalan words\u003c/a\u003e, INTEGERS, Electronic J. of Combinatorial Number Theory, Vol. 5, Paper A07, 2005."
			],
			"formula": [
				"The generating polynomial for row n is P[n](t) = binomial[2n,n]/[n+1], where [n+1]=1+t+t^2+...+t^n and binomial[2n,n] is a Gaussian polynomial."
			],
			"example": [
				"T(4,8)=2 because we have 01001101 (with 10's starting at positions 2 and 6) and 00101011 (with 10's starting at positions 3 and 5).",
				"Triangle starts:",
				"  1;",
				"  1;",
				"  1,0,1;",
				"  1,0,1,1,1,0,1;",
				"  1,0,1,1,2,1,2,1,2,1,1,0,1;",
				"  1,0,1,1,2,2,3,2,4,3,4,3,4,2,3,2,2,1,1,0,1;"
			],
			"maple": [
				"br:=n-\u003esum(q^i,i=0..n-1): f:=n-\u003eproduct(br(j),j=1..n): cbr:=(n,k)-\u003ef(n)/f(k)/f(n-k): P:=n-\u003esort(expand(simplify(cbr(2*n,n)/br(n+1)))): for n from 0 to 7 do seq(coeff(P(n),q,k),k=0..n*(n-1)) od; # yields sequence in triangular form",
				"# second Maple program:",
				"b:= proc(x, y, t) option remember; `if`(y\u003c0 or y\u003ex, 0, `if`(x=0, 1,",
				"      expand(b(x-1, y-1, true)+b(x-1, y+1, false)*`if`(t, z^x, 1))))",
				"    end:",
				"T:= n-\u003e (p-\u003e seq(coeff(p, z, i), i=0..degree(p)))(b(2*n, 0, false)):",
				"seq(T(n), n=0..8);  # _Alois P. Heinz_, Sep 15 2014"
			],
			"mathematica": [
				"b[x_, y_, t_] := b[x, y, t] = If[y\u003c0 || y\u003ex, 0, If[x == 0, 1, Expand[b[x-1, y-1, True] + b[x-1, y+1, False]*If[t, z^x, 1]]]]; T[n_] := Function[{p}, Table[ Coefficient[p, z, i], {i, 0, Exponent[p, z]}]][b[2*n, 0, False]]; Table[T[n], {n, 0, 8}] // Flatten (* _Jean-François Alcover_, May 26 2015, after _Alois P. Heinz_ *)",
				"p[n_] := QBinomial[2n,n,q]/QBinomial[n+1,1,q]; Table[CoefficientList[p[n] // FunctionExpand, q], {n,0,9}] // Flatten (* _Peter Luschny_, Jul 22 2016 *)"
			],
			"program": [
				"(Sage)",
				"from sage.combinat.q_analogues import q_catalan_number",
				"def T(n): return list(q_catalan_number(n))",
				"for n in (0..6): print(T(n)) # _Peter Luschny_, Jul 19 2016"
			],
			"xref": [
				"Cf. A000108, A001405, A002740, A129174."
			],
			"keyword": "nonn,tabf",
			"offset": "0,17",
			"author": "_Emeric Deutsch_, Apr 20 2007",
			"ext": [
				"New name from _Peter Luschny_, Jul 24 2016"
			],
			"references": 6,
			"revision": 41,
			"time": "2020-06-22T16:40:37-04:00",
			"created": "2007-05-11T03:00:00-04:00"
		}
	]
}