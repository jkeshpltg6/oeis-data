{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A308584",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 308584,
			"data": "1,1,1,1,2,1,3,3,2,2,4,3,1,4,2,2,4,2,2,2,4,2,3,2,3,5,2,3,5,3,3,5,2,2,4,4,4,3,4,3,5,3,5,5,2,6,7,1,3,6,4,4,4,4,2,9,3,2,4,3,7,4,4,5,5,4,6,5,3,6,8,2,5,7,3,5,7,3,3,7,5,7,3,5,5,8,1,4,8,1,7,6,3,3,9,5,4,6,4,5",
			"name": "Number of ways to write n as a*(a+1)/2 + b*(b+1)/2 + 5^c*8^d, where a,b,c,d are nonnegative integers with a \u003c= b.",
			"comment": [
				"Conjecture: a(n) \u003e 0 for all n \u003e 0. Equivalently, each n = 1,2,3,... can be written as w^2 + x*(x+1) + 5^y*8^z with w,x,y,z nonnegative integers.",
				"We have verified a(n) \u003e 0 for all n = 1..4*10^8.",
				"See also A308566 for a similar conjecture.",
				"a(n) \u003e 0 for all 0 \u003c n \u003c 10^10. - _Giovanni Resta_, Jun 10 2019"
			],
			"link": [
				"Zhi-Wei Sun, \u003ca href=\"/A308584/b308584.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Zhi-Wei Sun, \u003ca href=\"http://dx.doi.org/10.4064/aa127-2-1\"\u003eMixed sums of squares and triangular numbers\u003c/a\u003e, Acta Arith. 127(2007), 103-113."
			],
			"example": [
				"a(13) = 1 with 13 = 3*4/2 + 3*4/2 + 5^0*8^0.",
				"a(48) = 1 with 48 = 5*6/2 + 7*8/2 + 5^1*8^0.",
				"a(87) = 1 with 87 = 1*2/2 + 12*13/2 + 5^0*8^1.",
				"a(90) = 1 with 90 = 4*5/2 + 10*11/2 + 5^2*8^0.",
				"a(423) = 1 with 423 = 9*10/2 + 22*23/2 + 5^3*8^0.",
				"a(517) = 1 with 517 = 17*18/2 + 24*25/2 + 5^0*8^2.",
				"a(985) = 1 with 985 = 19*20/2 + 34*35/2 + 5^2*8^1.",
				"a(2694) = 1 with 2694 = 7*8/2 + 68*69/2 + 5^1*8^2.",
				"a(42507) = 1 with 42507 = 178*179/2 + 223*224/2 + 5^2*8^2.",
				"a(544729) = 1 with 544729 = 551*552/2 + 857*858/2 + 5^5*8^1.",
				"a(913870) = 1 with 913870 = 559*560/2 + 700*701/2 + 5^3*8^4.",
				"a(1843782) = 1 with 1843782 = 808*809/2 + 1668*1669/2 + 5^6*8^1."
			],
			"mathematica": [
				"TQ[n_]:=TQ[n]=IntegerQ[Sqrt[8n+1]];",
				"tab={};Do[r=0;Do[If[TQ[n-5^k*8^m-x(x+1)/2],r=r+1],{k,0,Log[5,n]},{m,0,Log[8,n/5^k]},{x,0,(Sqrt[4(n-5^k*8^m)+1]-1)/2}];tab=Append[tab,r],{n,1,100}];Print[tab]"
			],
			"xref": [
				"Cf. A000217, A000351, A001018, A303656, A303637, A308411, A308547, A308566."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_Zhi-Wei Sun_, Jun 08 2019",
			"references": 12,
			"revision": 25,
			"time": "2019-06-10T10:20:59-04:00",
			"created": "2019-06-10T10:20:59-04:00"
		}
	]
}