{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006667",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6667,
			"id": "M0019",
			"data": "0,0,2,0,1,2,5,0,6,1,4,2,2,5,5,0,3,6,6,1,1,4,4,2,7,2,41,5,5,5,39,0,8,3,3,6,6,6,11,1,40,1,9,4,4,4,38,2,7,7,7,2,2,41,41,5,10,5,10,5,5,39,39,0,8,8,8,3,3,3,37,6,42,6,3,6,6,11,11,1,6,40,40,1,1,9,9,4,9,4,33,4,4,38",
			"name": "Number of tripling steps to reach 1 from n in '3x+1' problem, or -1 if 1 is never reached.",
			"comment": [
				"A075680, which gives the values for odd n, isolates the essential behavior of this sequence. - _T. D. Noe_, Jun 01 2006",
				"a(n) = A078719(n) - 1; a(A000079(n))=0; a(A062052(n))=1; a(A062053(n))=2; a(A062054(n))=3; a(A062055(n))=4; a(A062056(n))=5; a(A062057(n))=6; a(A062058(n))=7; a(A062059(n))=8; a(A062060(n))=9. - _Reinhard Zumkeller_, Oct 08 2011",
				"A033959 and A033958 give record values and where they occur. - _Reinhard Zumkeller_, Jan 08 2014",
				"a(n*2^k) = a(n), for all k \u003e= 0. - _L. Edson Jeffery_, Aug 11 2014"
			],
			"reference": [
				"J.-P. Allouche and J. Shallit, Automatic Sequences, Cambridge Univ. Press, 2003, p. 204, Problem 22.",
				"R. K. Guy, Unsolved Problems in Number Theory, E16.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"T. D. Noe, \u003ca href=\"/A006667/b006667.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"J. C. Lagarias, \u003ca href=\"http://www.cecm.sfu.ca/organics/papers/lagarias/paper/html/paper.html\"\u003eThe 3x+1 problem and its generalizations\u003c/a\u003e, Amer. Math. Monthly, 92 (1985), 3-23.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CollatzProblem.html\"\u003eCollatz Problem.\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"http://en.wikipedia.org/wiki/Collatz_conjecture\"\u003eCollatz conjecture\u003c/a\u003e",
				"\u003ca href=\"/index/3#3x1\"\u003eIndex entries for sequences related to 3x+1 (or Collatz) problem\u003c/a\u003e"
			],
			"formula": [
				"a(1) = 0, a(n) = a(n/2) if n is even, a(n) = a(3n+1)+1 if n\u003e1 is odd. The Collatz conjecture is that this defines a(n) for all n \u003e= 1.",
				"a(n) = floor(log(2^A006666(n)/n)/log(3)). - _Joe Slater_, Aug 30 2017"
			],
			"mathematica": [
				"Table[Count[Differences[NestWhileList[If[EvenQ[#],#/2,3#+1]\u0026,n,#\u003e1\u0026]], _?Positive], {n,100}] (* _Harvey P. Dale_, Nov 14 2011 *)"
			],
			"program": [
				"(PARI) for(n=2,100,s=n; t=0; while(s!=1,if(s%2==0,s=s/2,s=(3*s+1)/2; t++); if(s==1,print1(t,\",\"); ); ))",
				"(Haskell)",
				"a006667 = length . filter odd . takeWhile (\u003e 2) . (iterate a006370)",
				"a006667_list = map a006667 [1..]",
				"-- _Reinhard Zumkeller_, Oct 08 2011",
				"(Python)",
				"def a(n):",
				"    if n==1: return 0",
				"    x=0",
				"    while True:",
				"        if n%2==0: n/=2",
				"        else:",
				"            n = 3*n + 1",
				"            x+=1",
				"        if n\u003c2: break",
				"    return x",
				"print([a(n) for n in range(1, 101)]) # _Indranil Ghosh_, Apr 14 2017"
			],
			"xref": [
				"Equals A078719(n)-1.",
				"Cf. A006370, A006577, A006666 (halving steps), A092893, A127789."
			],
			"keyword": "nonn,nice,hear",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_, _Bill Gosper_",
			"ext": [
				"More terms from Larry Reeves (larryr(AT)acm.org), Apr 27 2001",
				"\"Escape clause\" added to definition by _N. J. A. Sloane_, Jun 06 2017"
			],
			"references": 55,
			"revision": 82,
			"time": "2021-12-22T10:17:54-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}