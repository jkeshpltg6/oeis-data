{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A085965",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 85965,
			"data": "0,3,5,7,5,5,0,1,7,4,8,3,9,2,4,2,5,7,1,3,2,8,1,8,2,4,2,5,3,8,8,5,5,7,1,1,1,3,1,6,9,7,2,7,6,7,2,6,6,5,1,3,3,1,6,9,0,0,9,2,6,7,4,8,2,3,9,7,5,8,3,4,2,7,4,7,2,7,9,3,1,3,6,6,0,7,2,8,0,6,4,7,0,3,7,6,7,9,5,0,8,9,6,3,9",
			"name": "Decimal expansion of the prime zeta function at 5.",
			"comment": [
				"Mathar's Table 1 (cited below) lists expansions of the prime zeta function at integers s in 10..39. - _Jason Kimberley_, Jan 05 2017"
			],
			"reference": [
				"Henri Cohen, Number Theory, Volume II: Analytic and Modern Tools, GTM Vol. 240, Springer, 2007; see pp. 208-209.",
				"J. W. L. Glaisher, On the Sums of Inverse Powers of the Prime Numbers, Quart. J. Math. 25, 347-362, 1891."
			],
			"link": [
				"Jason Kimberley, \u003ca href=\"/A085965/b085965.txt\"\u003eTable of n, a(n) for n = 0..1702\u003c/a\u003e",
				"Henri Cohen, \u003ca href=\"http://www.math.u-bordeaux.fr/~cohen/hardylw.dvi\"\u003eHigh Precision Computation of Hardy-Littlewood Constants\u003c/a\u003e, Preprint, 1998.",
				"Henri Cohen, \u003ca href=\"/A221712/a221712.pdf\"\u003eHigh-precision computation of Hardy-Littlewood constants\u003c/a\u003e. [pdf copy, with permission]",
				"X. Gourdon and P. Sebah, \u003ca href=\"http://numbers.computation.free.fr/Constants/Miscellaneous/constantsNumTheory.html\"\u003eSome Constants from Number theory\u003c/a\u003e",
				"R. J. Mathar, \u003ca href=\"http://arxiv.org/abs/0803.0900\"\u003eSeries of reciprocal powers of k-almost primes\u003c/a\u003e, arXiv:0803.0900 [math.NT], 2008-2009. Table 1.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/PrimeZetaFunction.html\"\u003ePrime Zeta Function\u003c/a\u003e"
			],
			"formula": [
				"P(5) = Sum_{p prime} 1/p^5 = Sum_{n\u003e=1} mobius(n)*log(zeta(5*n))/n.",
				"Equals 1/2^5 + A085994 + A086035. - _R. J. Mathar_, Jul 14 2012",
				"Equals Sum_{k\u003e=1} 1/A050997(k). - _Amiram Eldar_, Jul 27 2020"
			],
			"example": [
				"0.0357550174839242571328..."
			],
			"maple": [
				"A085965:= proc(i) print(evalf(add(1/ithprime(k)^5,k=1..i),100)); end:",
				"A085965(100000); # _Paolo P. Lava_, May 29 2012"
			],
			"mathematica": [
				"s[n_] := s[n] = Sum[ MoebiusMu[k]*Log[Zeta[5*k]]/k, {k, 1, n}] // RealDigits[#, 10, 104]\u0026 // First // Prepend[#, 0]\u0026; s[100]; s[n=200]; While[s[n] != s[n-100], n = n+100]; s[n] (* _Jean-François Alcover_, Feb 14 2013, from 1st formula *)",
				"RealDigits[ PrimeZetaP[ 5], 10, 111][[1]] (* _Robert G. Wilson v_, Sep 03 2014 *)"
			],
			"program": [
				"(MAGMA) R := RealField(106);",
				"PrimeZeta := func\u003ck,N | \u0026+[R|MoebiusMu(n)/n*Log(ZetaFunction(R,k*n)): n in[1..N]]\u003e;",
				"[0] cat Reverse(IntegerToSequence(Floor(PrimeZeta(5,69)*10^105)));",
				"// _Jason Kimberley_, Dec 30 2016",
				"(PARI) sumeulerrat(1/p,5) \\\\ _Hugo Pfoertner_, Feb 03 2020"
			],
			"xref": [
				"Decimal expansion of the prime zeta function: A085548 (at 2), A085541 (at 3), A085964 (at 4), this sequence (at 5), A085966 (at 6) to A085969 (at 9).",
				"Cf. A013663, A050997, A242304."
			],
			"keyword": "cons,easy,nonn",
			"offset": "0,2",
			"author": "Antonio G. Astudillo (afg_astudillo(AT)lycos.com), Jul 06 2003",
			"references": 17,
			"revision": 42,
			"time": "2020-07-27T07:57:18-04:00",
			"created": "2003-09-13T03:00:00-04:00"
		}
	]
}