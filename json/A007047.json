{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A007047",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 7047,
			"id": "M2903",
			"data": "1,3,11,51,299,2163,18731,189171,2183339,28349043,408990251,6490530291,112366270379,2107433393523,42565371881771,921132763911411,21262618727925419,521483068116543603,13542138653027381291,371206349277313644531",
			"name": "Number of chains in power set of n-set.",
			"comment": [
				"Stirling transform of A052849(n-1) = [1,2,4,12,48,...] is a(n-1) =[1,3,11,51,299,...]. - _Michael Somos_, Mar 04 2004",
				"It is interesting to note that a chain in the power set of a set X can be thought of as a fuzzy subset of X and conversely. Chains originating with empty set are fuzzy subsets with empty core and those chains not ending with the whole set are with support strictly contained in X. - Venkat Murali (v.murali(AT)ru.ac.za), May 18 2005",
				"Equals the binomial transform of A000629: (1, 2, 6, 26, 150, 1082, ...) and the double binomial transform of A000670: (1, 1, 3, 13, 75, 541, ...). - _Gary W. Adamson_, Aug 04 2009",
				"Row sums of A038719. - _Peter Bala_, Jul 09 2014",
				"Also the number of restricted barred preferential arrangements of an n-set having two bars, where one fixed section is a free section and the other two sections are restricted sections. - _Sithembele Nkonkobe_, Jun 16 2015"
			],
			"reference": [
				"V. Murali, Counting fuzzy subsets of a finite set, preprint, Rhodes University, Grahamstown 6140, South Africa, 2003.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A007047/b007047.txt\"\u003eTable of n, a(n) for n = 0..424\u003c/a\u003e (first 101 terms from T. D. Noe)",
				"P. Barry, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL16/Barry4/barry271.html\"\u003eGeneral Eulerian Polynomials as Moments Using Exponential Riordan Arrays\u003c/a\u003e, Journal of Integer Sequences, 16 (2013), #13.9.6.",
				"Lisa Berry, Stefan Forcey, Maria Ronco, and Patrick Showers, \u003ca href=\"http://www.math.uakron.edu/~sf34/stello_both.pdf\"\u003eSpecies substitution, graph suspension, and graded hopf algebras of painted tree polytopes\u003c/a\u003e.",
				"Jun Ma, S.-M. Ma, and Y.-N. Yeh, \u003ca href=\"https://arxiv.org/abs/1711.09016\"\u003eRecurrence relations for binomial-Eulerian polynomials\u003c/a\u003e, arXiv preprint arXiv:1711.09016 [math.CO], 2017.",
				"T. Manneville and V. Pilaud, \u003ca href=\"https://arxiv.org/abs/1501.07152\"\u003eCompatibility fans for graphical nested complexes\u003c/a\u003e, arXiv preprint arXiv:1501.07152 [math.CO], 2015-2016.",
				"V. Murali, \u003ca href=\"http://www.ru.ac.za/affiliates/fuzzysystems\"\u003eNumber of fuzzy subsets of a finite set\u003c/a\u003e, fuzzy systems research group, Universities of Rhodes and Fort Hare. [broken link]",
				"V. Murali and B. B. Makamba, \u003ca href=\"https://doi.org/10.1080/03081070512331318356\"\u003eFinite Fuzzy Sets\u003c/a\u003e, International Journal of General Systems, Vol. 34, No. 1 (2005), pp. 61-75.",
				"R. B. Nelsen, \u003ca href=\"/A007047/a007047.pdf\"\u003eLetter to N. J. A. Sloane, Aug. 1991\u003c/a\u003e",
				"R. B. Nelsen and H. Schmidt, Jr., \u003ca href=\"http://www.jstor.org/stable/2690450\"\u003eChains in power sets\u003c/a\u003e, Math. Mag., 64 (1991), 23-31.",
				"S. Nkonkobe and V. Murali, \u003ca href=\"https://arxiv.org/abs/1503.06172\"\u003eA study of a family of generating functions of Nelsen-Schmidt type and some identities on restricted barred preferential arrangements\u003c/a\u003e, arXiv:1503.06172 [math.CO], Apr 2015.",
				"\u003ca href=\"/index/Pos#posets\"\u003eIndex entries for sequences related to posets\u003c/a\u003e"
			],
			"formula": [
				"E.g.f.: exp(2*x)/(2-exp(x)).",
				"a(n) = Sum_{k\u003e=1} (k+1)^n/2^k = 2*A000629(n)-1. - _Benoit Cloitre_, Sep 08 2002",
				"a(n) = one less than sum of quotients with numerator 4 times (n!)((k_1 + k_2 + ... + k_n)!) and with denominator (k_1!k_2!...k_n!)(1!^k_1 2!^k_2...n!^k_n) where the sum is taken over all partitions 1*k_1 + 2*k_2 + ... + n*k_n = n. E.g. a(1) = 3 because the membership value of x to {x} is either 1, alpha with 0 \u003c alpha \u003c 1 or 0. a(2) = 11 since the membership values x and y to {x, y} are 1 \u003e= alpha \u003e= beta \u003e= 0 for {empty set, x, y} in that order or {empty set, y, x} exercising all possible \u003e or = . - Venkat Murali (v.murali(AT)ru.ac.za), May 18 2005",
				"G.f.: 1/Q(0), where Q(k) = 1 - 3*x*(k+1) - 2*x^2*(k+1)*(k+1)/Q(k+1); (continued fraction). - _Sergei N. Gladkovskii_, Oct 02 2013",
				"a(n) ~ 2*n! / (log(2))^(n+1). - _Vaclav Kotesovec_, Sep 27 2017",
				"a(n) = 4 * A000670(n) - 1 for n \u003e 0. - _Alois P. Heinz_, Feb 07 2020",
				"a(n) = -(-1)^n Phi(2,-n,-1), where Phi(z,s,a) is the Lerch Zeta function. - _Federico Provvedi_, Sep 05 2020",
				"a(n) = 1 + 2 * Sum_{k=0..n-1} (-1)^(n-k-1) * binomial(n,k) * a(k). - _Ilya Gutkovskiy_, Apr 28 2021"
			],
			"maple": [
				"P := proc(n,x) option remember; if n = 0 then 1 else",
				"   (n*x+2*(1-x))*P(n-1,x)+x*(1-x)*diff(P(n-1,x),x);",
				"   expand(%) fi end:",
				"A007047 := n -\u003e 2^n*subs(x=1/2, P(n,x)):",
				"seq(A007047(n), n=0..19);  # _Peter Luschny_, Mar 07 2014",
				"# second Maple program:",
				"b:= proc(n) option remember; `if`(n=0, 4,",
				"      add(b(n-j)*binomial(n, j), j=1..n))",
				"    end:",
				"a:= n-\u003e `if`(n=0, 1, b(n)-1):",
				"seq(a(n), n=0..21);  # _Alois P. Heinz_, Feb 07 2020"
			],
			"mathematica": [
				"Table[LerchPhi[1/2, -n, 2]/2, {n, 0, 10}] (* _Vladimir Reshetnikov_, Feb 16 2011 *)",
				"Table[2*PolyLog[-n, 1/2] - 1 , {n, 0, 19}] (* _Jean-François Alcover_, Aug 14 2013 *)",
				"With[{nn=20},CoefficientList[Series[Exp[2x]/(2-Exp[x]),{x,0,nn}],x] Range[ 0,nn]!] (* _Harvey P. Dale_, Dec 08 2015 *)",
				"Table[-(-1)^k HurwitzLerchPhi[2, -k, -1], {k, 0, 30}] (* _Federico Provvedi_,Sep 05 2020 *)"
			],
			"program": [
				"(PARI) a(n)=if(n\u003c0,0,n!*polcoeff(subst((y+1)^2/(1-y),y,exp(x+x*O(x^n))-1),n));",
				"(PARI) x='x+O('x^66); Vec(serlaplace(exp(2*x)/(2-exp(x)))) \\\\ _Joerg Arndt_, Aug 14 2013",
				"(Haskell)",
				"a007047 = sum . a038719_row  -- _Reinhard Zumkeller_, Feb 05 2014"
			],
			"xref": [
				"Cf. A000629, A000629, A000670. Row sums of A038719."
			],
			"keyword": "nonn,nice,easy",
			"offset": "0,2",
			"author": "_N. J. A. Sloane_, Roger B. Nelsen",
			"references": 28,
			"revision": 121,
			"time": "2021-06-04T22:50:15-04:00",
			"created": "1994-04-28T03:00:00-04:00"
		}
	]
}