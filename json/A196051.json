{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A196051",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 196051,
			"data": "0,1,4,4,10,10,9,9,20,20,20,18,18,18,35,16,18,31,16,32,32,35,31,28,56,31,48,29,32,50,35,25,56,32,52,44,28,28,50,46,31,46,29,52,72,48,50,40,48,75,52,46,25,64,84,42,46,50,32,67,44,56,67,36,76,76,28,48,72,70,46,59,46,44,102,42,79,68,52,62,88,50,48,62,79,46,75,71,40,92,71,67,84,72,71,54,75,65,104,96",
			"name": "The Wiener index of the rooted tree with Matula-Goebel number n.",
			"comment": [
				"The Wiener index of a connected graph is the sum of the distances between all unordered pairs of vertices in the graph.",
				"The Matula-Goebel number of a rooted tree can be defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A196051/b196051.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Emeric Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003eTree statistics from Matula numbers\u003c/a\u003e, arXiv preprint arXiv:1111.4288 [math.CO], 2011.",
				"F. Goebel, \u003ca href=\"http://dx.doi.org/10.1016/0095-8956(80)90049-0\"\u003eOn a 1-1-correspondence between rooted trees and natural numbers\u003c/a\u003e, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, \u003ca href=\"http://dx.doi.org/10.1016/0012-365X(95)00182-V\"\u003eOn Matula numbers\u003c/a\u003e, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, \u003ca href=\"http://www.emis.de/journals/PIMB/067/3.html\"\u003eDeducing properties of trees from their Matula numbers\u003c/a\u003e, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, \u003ca href=\"http://www.jstor.org/stable/2027327\"\u003eA natural rooted tree enumeration by prime factorization\u003c/a\u003e, SIAM Rev. 10 (1968) 273.",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1)=0; if n = prime(t) (the t-th prime), then a(n)=a(t)+PL(t)+E(t)+1; if n=rs (r,s\u003e=2), then a(n)=a(r)+a(s)+PL(r)E(s)+PL(s)E(r); PL(m) and E(m) denote the path length and the number of edges of the rooted tree with Matula number m (see A196047, A196050). The Maple program is based on this recursive formula."
			],
			"example": [
				"a(7)=9 because the rooted tree with Matula-Goebel number 7 is the rooted tree Y (1+1+1+2+2+2=9).",
				"a(2^m) = m^2 because the rooted tree with Matula-Goebel number 2^m is a star with m edges and we have m distances 1 and m(m-1)/2 distances 2; m + m(m-1)=m^2."
			],
			"maple": [
				"with(numtheory): a := proc (n) local r, s, E, PL: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: E := proc (n) if n = 1 then 0 elif bigomega(n) = 1 then 1+E(pi(n)) else E(r(n))+E(s(n)) end if end proc: PL := proc (n) if n = 1 then 0 elif bigomega(n) = 1 then 1+E(pi(n))+PL(pi(n)) else PL(r(n))+PL(s(n)) end if end proc: if n = 1 then 0 elif bigomega(n) = 1 then a(pi(n))+PL(pi(n))+1+E(pi(n)) else a(r(n))+a(s(n))+PL(r(n))*E(s(n))+PL(s(n))*E(r(n)) end if end proc: seq(a(n), n = 1 .. 100);"
			],
			"program": [
				"(Haskell)",
				"import Data.List (genericIndex)",
				"a196051 n = genericIndex a196051_list (n - 1)",
				"a196051_list = 0 : g 2 where",
				"   g x = y : g (x + 1) where",
				"     y | t \u003e 0     = a196051 t + a196047 t + a196050 t + 1",
				"       | otherwise = a196051 r + a196051 s +",
				"                     a196047 r * a196050 s + a196047 s * a196050 r",
				"       where t = a049084 x; r = a020639 x; s = x `div` r",
				"-- _Reinhard Zumkeller_, Sep 03 2013"
			],
			"xref": [
				"Cf. A196047, A196050.",
				"Cf. A049084, A020639.",
				"Terminal Wiener indices: A196055, A348959."
			],
			"keyword": "nonn",
			"offset": "1,3",
			"author": "_Emeric Deutsch_, Sep 27 2011",
			"references": 6,
			"revision": 21,
			"time": "2021-11-12T03:50:18-05:00",
			"created": "2011-09-27T13:40:20-04:00"
		}
	]
}