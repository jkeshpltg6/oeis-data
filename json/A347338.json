{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A347338",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 347338,
			"data": "2,3,35,7,4,12,39,20,146,30,52,32,175,88,693,9,99,108,188,847,1014,392,124,25,315,234,195,416,196,477,225,48,2262,1327,1330,252,368,160,1636,640,5067,168,441,884,1183,1064,1377,120,1328,112,4908,3872,891,396,512",
			"name": "a(n) is the smallest number k such that tau(k) = tau(k+n), and there is no number m, k \u003c m \u003c k+n such that tau(m) = tau(k).",
			"comment": [
				"The prohibition in the definition distinguishes this sequence from A065559. This sequence identifies the first occurrence of a gap between numbers with the same tau, where no intervening number has that tau. Each tau t \u003e 1 has a corresponding sequence of gaps (e.g., for t = 2, A001223, for t = 3, A069482), and a(n) is the smallest index of terms in A000005 corresponding to the first occurrence of a gap of length n in all of these (same tau) gap sequences.",
				"A number appearing in this sequence cannot appear again, and many numbers do not appear at all (1 is not in because it is the only number with 1 divisor; 5 6 and 8 are not in because 3 is already a term; 10 is not in because 7 is a term, etc."
			],
			"link": [
				"David A. Corneth, \u003ca href=\"/A347338/b347338.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e"
			],
			"example": [
				"a(1) = 2 because 2 is the smallest k such that tau(k) = tau(k+1); a(2) = 3 because it is the smallest k with tau(k) = tau(k+2) with no intervening same tau number; a(3) = 35 because d(35) = 4, d(36) = 9, d(37) = 2, d(38) = 4 = d(35) and this is the least case of a gap of 3. (Here d means tau, namely, A000005.)"
			],
			"mathematica": [
				"Block[{a = {}, k, d = DivisorSigma[0, Range[2^14]]}, Do[k = 1; While[Or[#[[1]] != #[[-1]], Count[#, #[[1]]] \u003e 2] \u0026@ d[[k ;; k + n]], k++]; AppendTo[a, k], {n, 55}]; a] (* _Michael De Vlieger_, Aug 27 2021 *)"
			],
			"program": [
				"(PARI) a(n) = {my(i); for(i = 1, oo, if(iscan(i, n), return(i) ) ) }",
				"iscan(k, n) = { my(c); c = numdiv(k); if(numdiv(k + n) != c, return(0) ); for(i = 1, n-1, if(numdiv(k + i) == c, return(0) ) ); 1 } \\\\ _David A. Corneth_, Aug 27 2021",
				"(Python)",
				"from sympy import divisor_count",
				"from functools import lru_cache",
				"@lru_cache(maxsize=None)",
				"def tau(n): return divisor_count(n)",
				"def a(n):",
				"    k = 2",
				"    while True:",
				"        while tau(k) != tau(k+n): k += 1",
				"        if not any(tau(m) == tau(k) for m in range(k+1, k+n)): return k",
				"        k += 1",
				"print([a(n) for n in range(1, 56)]) # _Michael S. Branicky_, Aug 27 2021"
			],
			"xref": [
				"Cf: A000005, A001223, A005237, A069482, A276963, A005179, A065559."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_David James Sycamore_, Aug 27 2021",
			"references": 2,
			"revision": 22,
			"time": "2021-08-30T21:50:22-04:00",
			"created": "2021-08-30T11:42:53-04:00"
		}
	]
}