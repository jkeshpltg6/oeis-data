{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A327083",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 327083,
			"data": "1,2,1,3,4,1,4,11,12,1,5,24,87,40,1,6,45,416,1197,184,1,7,76,1475,18592,42660,1296,1,8,119,4236,166885,3017600,4223313,17072,1,9,176,10437,1019880,85025050,1748176768,1139277096,424992",
			"name": "Array read by descending antidiagonals: A(n,k) is the number of oriented colorings of the edges of a regular n-dimensional simplex using up to k colors.",
			"comment": [
				"An n-dimensional simplex has n+1 vertices and (n+1)*n/2 edges. For n=1, the figure is a line segment with one edge. For n-2, the figure is a triangle with three edges. For n=3, the figure is a tetrahedron with six edges. The Schläfli symbol, {3,...,3}, of the regular n-dimensional simplex consists of n-1 threes. Two oriented colorings are the same if one is a rotation of the other; chiral pairs are counted as two.",
				"A(n,k) is also the number of oriented colorings of (n-2)-dimensional regular simplices in an n-dimensional simplex using up to k colors. Thus, A(2,k) is also the number of oriented colorings of the vertices (0-dimensional simplices) of an equilateral triangle."
			],
			"link": [
				"Robert A. Russell, \u003ca href=\"/A327083/b327083.txt\"\u003eTable of n, a(n) for n = 1..325\u003c/a\u003e First 25 antidiagonals.",
				"Harald Fripertinger, \u003ca href=\"http://www.mathe2.uni-bayreuth.de/frib/html/book/hyl00_42.html\"\u003eThe cycle type of the induced action on 2-subsets\u003c/a\u003e",
				"E. M. Palmer and R. W. Robinson, \u003ca href=\"https://projecteuclid.org/euclid.acta/1485889789\"\u003eEnumeration under two representations of the wreath product\u003c/a\u003e, Acta Math., 131 (1973), 123-143."
			],
			"formula": [
				"The algorithm used in the Mathematica program below assigns each permutation of the vertices to a partition of n+1. It then determines the number of permutations for each partition and the cycle index for each partition.",
				"A(n,k) = Sum_{j=1..(n+1)*n/2} A327087(n,j) * binomial(k,j).",
				"A(n,k) = A327084(n,k) + A327085(n,k) = 2*A327084(n,k) - A327086(n,k) = 2*A327085(n,k) + A327086(n,k)."
			],
			"example": [
				"Array begins with A(1,1):",
				"  1  2    3     4      5       6       7        8        9        10 ...",
				"  1  4   11    24     45      76     119      176      249       340 ...",
				"  1 12   87   416   1475    4236   10437    22912    45981     85900 ...",
				"  1 40 1197 18592 166885 1019880 4738153 17962624 58248153 166920040 ...",
				"  ...",
				"For A(2,3) = 11, the nine achiral colorings are AAA, AAB, AAC, ABB, ACC, BBB, BBC, BCC, and CCC. The chiral pair is ABC-ACB."
			],
			"mathematica": [
				"CycleX[{2}] = {{1,1}}; (* cycle index for permutation with given cycle structure *)",
				"CycleX[{n_Integer}] := CycleX[n] = If[EvenQ[n], {{n/2,1}, {n,(n-2)/2}}, {{n,(n-1)/2}}]",
				"compress[x : {{_, _} ...}] := (s = Sort[x]; For[i=Length[s], i\u003e1, i-=1, If[s[[i,1]] == s[[i-1,1]], s[[i-1,2]]+=s[[i,2]]; s=Delete[s,i], Null]]; s)",
				"CycleX[p_List] := CycleX[p] = compress[Join[CycleX[Drop[p,-1]], If[Last[p] \u003e 1, CycleX[{Last[p]}], ## \u0026[]], If[# == Last[p], {#, Last[p]}, {LCM[#, Last[p]], GCD[#, Last[p]]}] \u0026 /@ Drop[p,-1]]]",
				"pc[p_List] := Module[{ci, mb}, mb = DeleteDuplicates[p]; ci = Count[p, #] \u0026 /@ mb; Total[p]!/(Times @@ (ci!) Times @@ (mb^ci))] (*partition count*)",
				"row[n_Integer] := row[n] = Factor[Total[If[EvenQ[Total[1-Mod[#,2]]], pc[#] j^Total[CycleX[#]][[2]], 0] \u0026 /@ IntegerPartitions[n+1]]/((n+1)!/2)]",
				"array[n_, k_] := row[n] /. j -\u003e k",
				"Table[array[n,d-n+1], {d,1,10}, {n,1,d}] // Flatten",
				"(* Using Fripertinger's exponent per Andrew Howroyd's code in A063841: *)",
				"pc[p_] := Module[{ci, mb}, mb = DeleteDuplicates[p]; ci = Count[p, #] \u0026/@ mb; Total[p]!/(Times @@ (ci!) Times @@ (mb^ci))]",
				"ex[v_] := Sum[GCD[v[[i]], v[[j]]], {i,2,Length[v]}, {j,i-1}] + Total[Quotient[v,2]]",
				"array[n_,k_] := Total[If[EvenQ[Total[1-Mod[#,2]]], pc[#]k^ex[#], 0] \u0026/@ IntegerPartitions[n+1]]/((n+1)!/2)",
				"Table[array[n,d-n+1], {d,10}, {n,d}] // Flatten"
			],
			"xref": [
				"Cf. A327084 (unoriented), A327085 (chiral), A327086 (achiral), A327087 (exactly k colors), A324999 (vertices, facets), A337883 (faces, peaks), A337407 (orthotope edges, orthoplex ridges), A337411 (orthoplex edges, orthotope ridges).",
				"Rows 1-4 are A000027, A006527, A046023, A331350.",
				"Column 2 is A218144(n+1)."
			],
			"keyword": "nonn,tabl",
			"offset": "1,2",
			"author": "_Robert A. Russell_, Aug 19 2019",
			"references": 13,
			"revision": 22,
			"time": "2021-06-09T02:31:27-04:00",
			"created": "2019-08-19T16:56:57-04:00"
		}
	]
}