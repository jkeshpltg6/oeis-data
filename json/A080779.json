{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A080779",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 80779,
			"data": "1,1,1,1,3,2,0,6,12,6,-4,0,40,60,24,0,-60,0,300,360,120,120,0,-840,0,2520,2520,720,0,3360,0,-11760,0,23520,20160,5040,-12096,0,80640,0,-169344,0,241920,181440,40320,0,-544320,0,1814400,0,-2540160,0,2721600,1814400,362880",
			"name": "Triangle read by rows: n-th row gives expansion of the series for HarmonicNumber[n, -p].",
			"comment": [
				"Row sums are (n+1)!, last element in row n is n!",
				"Alternative description using Bernoulli polynomials: Let p[x,n]=Sum[k^n,{k,1,x}]; 1/x /. NSolve[p[x,n]-Zeta[n]==0,x] where n\u003e=2. Then t(n,m)=CoefficientList[Expand[n!*(BernoulliB[n + 1, x + 1] - BernoulliB[n + 1])/x], x]. - _Roger L. Bagula_ and _N. J. A. Sloane_, Feb 18 2008",
				"The row polynomials R(n, x) = (n+1)!*F(n, x)/x with F(n,x) = (Sum_{k=1..m} k^n)|_{m=x} satisfy the recurrence R(n, x) = n!*(((x + 1)^(n+1) - 1)/x - Sum_{k=0..n-1} (binomial(n+1, k)*R(k, x)/(k+1)!)), n \u003e= 1, and R(0, x) = 1. See the Silverman reference, pp. 370 - 371, for F(n, x). - _Wolfdieter Lang_, Feb 04 2016"
			],
			"reference": [
				"J. H. Silverman, A Friendly Introduction to Number Theory, 3rd ed., Pearson Education, Inc, 2006, pp. 370 - 371."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A080779/b080779.txt\"\u003eRows n=0..100 of triangle, flattened\u003c/a\u003e",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Faulhaber%27s_formula\"\u003eFaulhaber's formula\u003c/a\u003e"
			],
			"formula": [
				"t(n, m) = [x^m] ((Bernoulli(n+1, x+1) - Bernoulli(n+1, 1))/x), for m = 0..n. See a comment above. For these Bernoulli polynomials see A264388 and A264389.",
				"t(n, m) = t(n-1, m-1) * n/(m+1). - _Michael Somos_, Aug 18 2018"
			],
			"example": [
				"The triangle t(n, m) begins:",
				"n\\m  0    1    2      3    4      5     6    7 ...",
				"0:   1",
				"1:   1    1",
				"2:   1    3    2",
				"3:   0    6   12      6",
				"4:  -4    0   40     60   24",
				"5:   0  -60    0    300  360    120",
				"6: 120    0 -840      0 2520   2520   720",
				"7:   0 3360    0 -11760    0  23520 20160 5040",
				"...",
				"Row n=8: -12096    0 80640      0 -169344 0 241920 181440 40320;",
				"Row n=9: 0 -544320 0 1814400 0 -2540160 0 2721600 1814400 362880;",
				"Row n=10: 3024000 0 -19958400 0 39916800 0 -39916800 0 33264000 19958400 3628800.",
				"... Reformatted and extended. - _Wolfdieter Lang_, Feb 04 2016"
			],
			"mathematica": [
				"Table[(n+1)! CoefficientList[ Sum[k^n, {k, 0, m}]/m, m], {n, 1, 12}] and for n=0: 1.",
				"a = Join[{{1}}, Table[CoefficientList[Expand[n!*(BernoulliB[n + 1, x + 1] - BernoulliB[n + 1])/x], x], {n, 1, 10}]] Flatten[a] (* _Roger L. Bagula_ and _N. J. A. Sloane_, Feb 18 2008 *)",
				"T[n_, k_] := Coefficient[ 1/x Integrate[ BernoulliB[n, x + 1], x], x, k]; (* _Michael Somos_, Aug 18 2018 *)"
			],
			"xref": [
				"Cf. A264388, A264389."
			],
			"keyword": "easy,sign,tabl",
			"offset": "0,5",
			"author": "_Wouter Meeussen_, Mar 11 2003",
			"references": 2,
			"revision": 24,
			"time": "2018-09-01T17:43:22-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}