{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A247829",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 247829,
			"data": "0,-1,-3,2,3,5,6,15,21,12,35,45,20,63,77,30,99,117,42,143,165,56,195,221,72,255,285,90,323,357,110,399,437,132,483,525,156,575,621,182,675,725,210,783,837,240,899,957,272,1023,1085,306,1155,1221,342,1295",
			"name": "a(3*k) = k*(k+1), a(3*k+1) = (2*k-1)*(2*k+1), a(3*k+2) = (2*k-1)*(2*k+3).",
			"comment": [
				"A permutation of A061037(n) = -1, -3, 0, 5, 3, 21, 2, 45, 15, 77, 6, ... and of A214297(n) = -1, 0, -3, 2, 3, 6, 5, 12, ... .",
				"Among consequences: b(n) = 4*a(n) + (sequence of period 3:repeat 1, 4, 16) = 1, 0, 4, 9, 16, 36, 25, 64, 100, ... , is a permutation of the squares of the nonnegative integers A000290(n).",
				"And a(n)/b(n) = 0/1, -1/0, -3/4, 2/9, 3/16, 5/36, ... is a permutation of the Balmer series A061037(n)/A061038(n) = -1/0, -3/4, 0/1, 5/36, 3/16, ... .",
				"a(5n) is divisible by 5."
			],
			"link": [
				"Colin Barker, \u003ca href=\"/A247829/b247829.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_09\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,3,0,0,-3,0,0,1)."
			],
			"formula": [
				"a(n) = 3*a(n-3) - 3*a(n-6) + a(n-9).",
				"a(3*k) + a(3*k+1) + a(3*k+2) = 9*k^2 + 5*k - 4.",
				"G.f.: x*(3*x^7 - 3*x^6 - 14*x^4 - 6*x^3 - 2*x^2 + 3*x + 1)/((x-1)^3*(x^2 +x+1)^3). - _Robert Israel_, Dec 01 2014",
				"a(n) = -(n^2 + n + floor(n/3)*(27*floor(n/3)^3 - 18*(n+1)*floor(n/3)^2 + (3*n^2 + 21*n - 14)*floor(n/3) - (5*n^2 - n + 5)))/2. - _Luce ETIENNE_, Mar 13 2017"
			],
			"maple": [
				"seq(op([k*(k+1),(2*k-1)*(2*k+1),(2*k-1)*(2*k+3)]), k=0..100); # _Robert Israel_, Dec 01 2014"
			],
			"mathematica": [
				"Table[Sequence @@ {n*(n+1), (2*n-1)*(2*n+1), (2*n-1)*(2*n+3)}, {n, 0, 18}] (* _Jean-François Alcover_, Dec 16 2014 *)"
			],
			"program": [
				"(PARI) concat(0, Vec(x*(3*x^7-3*x^6-14*x^4-6*x^3-2*x^2+3*x+1)/((x-1)^3*(x^2+x+1)^3) + O(x^100))) \\\\ _Colin Barker_, Dec 02 2014",
				"(MAGMA) m:=50; R\u003cx\u003e:=PowerSeriesRing(Integers(), m); [0] cat Coefficients(R!(x*(3*x^7-3*x^6-14*x^4-6*x^3-2*x^2+3*x+1)/((x-1)^3*(x^2 +x+1)^3))); // _G. C. Greubel_, Sep 20 2018"
			],
			"xref": [
				"Cf. A142717 (2 terms out of 3), A000290, A000466, A002378, A061037/A061038, A078371, A214297."
			],
			"keyword": "sign,easy",
			"offset": "0,3",
			"author": "_Paul Curtz_, Dec 01 2014",
			"references": 2,
			"revision": 48,
			"time": "2018-09-20T20:29:05-04:00",
			"created": "2015-01-29T11:06:05-05:00"
		}
	]
}