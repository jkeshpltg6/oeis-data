{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A332382",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 332382,
			"data": "1,3,5,15,7,21,35,105,11,33,55,165,77,231,385,1155,13,39,65,195,91,273,455,1365,143,429,715,2145,1001,3003,5005,15015,17,51,85,255,119,357,595,1785,187,561,935,2805,1309,3927,6545,19635,221,663,1105,3315,1547,4641,7735,23205",
			"name": "If n = Sum (2^e_k) then a(n) = Product (prime(e_k + 2)).",
			"comment": [
				"Permutation of odd squarefree numbers (A056911).",
				"a(n) is the n-th power of 3 in the monoid defined in A331590. - _Peter Munn_, May 02 2020"
			],
			"link": [
				"\u003ca href=\"/index/Bi#binary\"\u003eIndex entries for sequences related to binary expansion of n\u003c/a\u003e"
			],
			"formula": [
				"G.f.: Product_{k\u003e=0} (1 + prime(k+2) * x^(2^k)).",
				"a(0) = 1; a(n) = prime(floor(log_2(n)) + 2) * a(n - 2^floor(log_2(n))).",
				"a(2^(k-1)-1) = A002110(k)/2 for k \u003e 0.",
				"From _Peter Munn_, May 02 2020: (Start)",
				"a(2n) = A003961(a(n)).",
				"a(2n+1) = 3 * a(2n).",
				"a(n) = A225546(4^n).",
				"a(n+k) = A331590(a(n), a(k)).",
				"a(n XOR k) = A059897(a(n), a(k)), where XOR denotes bitwise exclusive-or, A003987.",
				"A048675(a(n)) = 2n.",
				"(End)"
			],
			"example": [
				"21 = 2^0 + 2^2 + 2^4 so a(21) = prime(2) * prime(4) * prime(6) = 3 * 7 * 13 = 273."
			],
			"maple": [
				"a:= n-\u003e (l-\u003e mul(ithprime(i+1)^l[i], i=1..nops(l)))(convert(n, base, 2)):",
				"seq(a(n), n=0..55);  # _Alois P. Heinz_, Feb 10 2020"
			],
			"mathematica": [
				"nmax = 55; CoefficientList[Series[Product[(1 + Prime[k + 2] x^(2^k)), {k, 0, Floor[Log[2, nmax]]}], {x, 0, nmax}], x]",
				"a[0] = 1; a[n_] := Prime[Floor[Log[2, n]] + 2] a[n - 2^Floor[Log[2, n]]]; Table[a[n], {n, 0, 55}]"
			],
			"program": [
				"(PARI) a(n) = my(b=Vecrev(binary(n))); prod(k=1, #b, if (b[k], prime(k+1), 1)); \\\\ _Michel Marcus_, Feb 10 2020"
			],
			"xref": [
				"Bisection of A019565.",
				"Cf. A002110, A029930, A048675, A056911, A121663, A225546.",
				"A003961, A003987, A059897, A331590 are used to express relationship between terms of this sequence."
			],
			"keyword": "nonn",
			"offset": "0,2",
			"author": "_Ilya Gutkovskiy_, Feb 10 2020",
			"references": 2,
			"revision": 18,
			"time": "2020-05-06T12:13:22-04:00",
			"created": "2020-02-12T08:28:22-05:00"
		}
	]
}