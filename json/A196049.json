{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A196049",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 196049,
			"data": "0,0,0,0,0,0,1,1,0,0,0,1,1,1,0,1,1,1,1,1,1,0,1,1,0,1,1,2,1,1,0,1,0,1,1,1,1,1,1,1,1,2,2,1,1,1,1,1,2,1,1,2,1,1,0,2,1,1,1,1,1,0,2,1,1,1,1,2,1,2,1,1,2,1,1,2,1,2,1,1,1,1,1,2,1,2,1,1,1,1,2,2,0,1,1,1,1,3,1,1,2,2,1,2,2,1,2,1,1,1",
			"name": "Number of branching nodes of the rooted tree with Matula-Goebel number n.",
			"comment": [
				"A branching node of a tree is a vertex of degree at least 3.",
				"The Matula-Goebel number of a rooted tree is defined in the following recursive manner: to the one-vertex tree there corresponds the number 1; to a tree T with root degree 1 there corresponds the t-th prime number, where t is the Matula-Goebel number of the tree obtained from T by deleting the edge emanating from the root; to a tree T with root degree m\u003e=2 there corresponds the product of the Matula-Goebel numbers of the m branches of T."
			],
			"reference": [
				"F. Goebel, On a 1-1-correspondence between rooted trees and natural numbers, J. Combin. Theory, B 29 (1980), 141-143.",
				"I. Gutman and A. Ivic, On Matula numbers, Discrete Math., 150, 1996, 131-142.",
				"I. Gutman and Yeong-Nan Yeh, Deducing properties of trees from their Matula numbers, Publ. Inst. Math., 53 (67), 1993, 17-22.",
				"D. W. Matula, A natural rooted tree enumeration by prime factorization, SIAM Review, 10, 1968, 273."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A196049/b196049.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"E. Deutsch, \u003ca href=\"http://arxiv.org/abs/1111.4288\"\u003eTree statistics from Matula numbers\u003c/a\u003e, arXiv preprint arXiv:1111.4288, 2011",
				"\u003ca href=\"/index/Mat#matula\"\u003eIndex entries for sequences related to Matula-Goebel numbers\u003c/a\u003e"
			],
			"formula": [
				"a(1)=0; if n=p(t) (= the t-th prime) and t is not the product of 2 prime factors, then a(n)=a(t); if n=p(t) (= the t-th prime) and t is the product of 2 prime factors, then a(n)=a(t)+1; if n=rs (r prime, s\u003e=2) and s is not a product of 2 prime factors, then a(n)=a(r)+a(s); if n=rs (r prime, s\u003e=2) and s is a product of 2 prime factors, then a(n)=a(r)+a(s)+1. The Maple program is based on this recursive formula."
			],
			"example": [
				"a(7)=1 because the rooted tree with Matula-Goebel number 7 is the rooted tree Y.",
				"if m\u003e2 then a(2^m) = 1 because the rooted tree with Matula-Goebel number 2^m is a star with m edges."
			],
			"maple": [
				"with(numtheory): a := proc (n) local r, s: r := proc (n) options operator, arrow: op(1, factorset(n)) end proc: s := proc (n) options operator, arrow: n/r(n) end proc: if n = 1 then 0 elif bigomega(n) = 1 and bigomega(pi(n)) \u003c\u003e 2 then a(pi(n)) elif bigomega(n) = 1 then a(pi(n))+1 elif bigomega(s(n)) \u003c\u003e 2 then a(r(n))+a(s(n)) else a(r(n))+a(s(n))+1 end if end proc: seq(a(n), n = 1 .. 110);"
			],
			"program": [
				"(Haskell)",
				"import Data.List (genericIndex)",
				"a196049 n = genericIndex a196049_list (n - 1)",
				"a196049_list = 0 : g 2 where",
				"   g x = y : g (x + 1) where",
				"     y | t \u003e 0     = a196049 t + a064911 t",
				"       | otherwise = a196049 r + a196049 s + a064911 s",
				"       where t = a049084 x; r = a020639 x; s = x `div` r",
				"-- _Reinhard Zumkeller_, Sep 03 2013"
			],
			"xref": [
				"Cf. A049084, A020639, A064911."
			],
			"keyword": "nonn",
			"offset": "1,28",
			"author": "_Emeric Deutsch_, Sep 27 2011",
			"references": 2,
			"revision": 16,
			"time": "2017-03-07T06:19:12-05:00",
			"created": "2011-09-27T13:37:08-04:00"
		}
	]
}