{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A291265",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 291265,
			"data": "2,9,38,153,596,2268,8480,31275,114086,412443,1479926,5276664,18711758,66041901,232129190,812934621,2837740232,9877082004,34288573484,118752490863,410394698534,1415492232255,4873386985130,16750755602928,57487476629594,197013756414033",
			"name": "a(n) = (1/3)*A291232(n).",
			"comment": [
				"Suppose s = (c(0), c(1), c(2), ...) is a sequence and p(S) is a polynomial. Let S(x) = c(0)*x + c(1)*x^2 + c(2)*x^3 + ... and T(x) = (-p(0) + 1/p(S(x)))/x. The p-INVERT of s is the sequence t(s) of coefficients in the Maclaurin series for T(x). Taking p(S) = 1 - S gives the \"INVERT\" transform of s, so that p-INVERT is a generalization of the \"INVERT\" transform (e.g., A033453).",
				"See A291219 for a guide to related sequences."
			],
			"link": [
				"Clark Kimberling, \u003ca href=\"/A291265/b291265.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_04\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (6, -7, -6, -1)"
			],
			"formula": [
				"G.f.: (2 - 3 x - 2 x^2)/(-1 + 3 x + x^2)^2.",
				"a(n) = 6*a(n-1) - 7*a(n-2) -6*a(n-3) - a(n-4) for n \u003e= 5.",
				"a(n) = (((3-sqrt(13))/2)^n*(-3+sqrt(13))*(-39+17*sqrt(13)-39*n) + 2^(-n)*(3+sqrt(13))^(1+n)*(39+17*sqrt(13)+39*n)) / 338. - _Colin Barker_, Aug 26 2017"
			],
			"mathematica": [
				"z = 60; s = x/(1 - x^2); p = (1 - 3 s)^2;",
				"Drop[CoefficientList[Series[s, {x, 0, z}], x], 1]  (* A000035 *)",
				"u = Drop[CoefficientList[Series[1/p, {x, 0, z}], x], 1]  (* A291232 *)",
				"u/3   (* A291265 *)"
			],
			"program": [
				"(PARI) Vec((2 + x)*(1 - 2*x) / (1 - 3*x - x^2)^2 + O(x^30)) \\\\ _Colin Barker_, Aug 26 2017"
			],
			"xref": [
				"Cf. A000035, A291232, A291219."
			],
			"keyword": "nonn,easy",
			"offset": "0,1",
			"author": "_Clark Kimberling_, Aug 26 2017",
			"references": 2,
			"revision": 8,
			"time": "2017-08-26T21:37:12-04:00",
			"created": "2017-08-26T21:37:12-04:00"
		}
	]
}