{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A129759",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 129759,
			"data": "1,2,3,2,3,3,3,5,5,5,5,5,5,7,7,7,7,7,7,7,7,7,7,7,7,7,11,11,7,11,11,13,13,11,11,11,11,13,13,11,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,17,17,17,17,17,19,19,17,17,17,17,19,19,17,17,19,19,19,19,19,19,17,19",
			"name": "For the Landau function L(n), A000793, this sequence gives the largest prime which is a factor of L(n).",
			"comment": [
				"This function is not monotone increasing, for example a(33) = 13 while a(34) = 11.",
				"Nicolas showed that a(n) ~ sqrt(n log n) and Grantham showed that a(n) \u003c= 1.328 sqrt(n log n) for n \u003e 4. Massias, Nicolas, \u0026 Robin conjecture that a(n) \u003c= 1.265... sqrt(n log n) in this range with equality at n = 215. - _Charles R Greathouse IV_, Jun 02 2014"
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A129759/b129759.txt\"\u003eTable of n, a(n) for n = 1..10000\u003c/a\u003e",
				"Jon Grantham, \u003ca href=\"http://www.pseudoprime.com/maxord.html\"\u003eThe largest prime divisor of the maximal order of an element of S_n\u003c/a\u003e, Math. Comp. 64:209 (1995), pp. 407-410.",
				"J. P. Massias, J. L. Nicolas and G. Robin, \u003ca href=\"http://math.univ-lyon1.fr/~nicolas/gdenMathComp.pdf\"\u003eEffective bounds for the maximal order of an element in the symmetric group\u003c/a\u003e, Math. Comp. 53:188 (1989), pp. 665-678. [\u003ca href=\"https://doi.org/10.1090/S0025-5718-1989-0979940-4\"\u003ealternate link\u003c/a\u003e]",
				"Jean-Louis Nicolas, \u003ca href=\"https://doi.org/10.24033/bsmf.1676\"\u003eOrdre maximal d'un élément du groupe S_n des permutations et 'highly composite numbers'\u003c/a\u003e, Bull. Soc. Math. France 97 (1969), 129-191.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/LandausFunction.html\"\u003eLandau's Function\u003c/a\u003e"
			],
			"formula": [
				"a(n) = A006530(A000793(n)). - _R. J. Mathar_, May 17 2007"
			],
			"example": [
				"L(29) = 2520, whose largest prime factor is 7. So a(29) = 7."
			],
			"mathematica": [
				"b[n_, i_] := b[n, i] = Module[{p}, p = If[i \u003c 1, 1, Prime[i]]; If[n == 0 || i \u003c 1, 1, Max[b[n, i - 1], Table[p^j*b[n - p^j, i - 1], {j, 1, Log[p, n] // Floor}]]]];",
				"g[n_] := b[n, If[n\u003c8, 3, PrimePi[Ceiling[1.328*Sqrt[n*Log[n] // Floor]]]]];",
				"a[n_] := FactorInteger[g[n]][[-1, 1]];",
				"Array[a, 100] (* _Jean-François Alcover_, Feb 19 2020, after _Alois P. Heinz_ in A000793 *)"
			],
			"xref": [
				"Cf. A006530, A000793, A128305."
			],
			"keyword": "nonn,look",
			"offset": "1,2",
			"author": "_Anthony C Robin_, May 15 2007",
			"ext": [
				"More terms from _Klaus Brockhaus_ and _R. J. Mathar_, May 16 2007",
				"Corrected a(66) by _Alois P. Heinz_, Feb 16 2013"
			],
			"references": 2,
			"revision": 29,
			"time": "2020-02-19T09:55:16-05:00",
			"created": "2007-11-10T03:00:00-05:00"
		}
	]
}