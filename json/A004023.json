{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A004023",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 4023,
			"id": "M2114",
			"data": "2,19,23,317,1031,49081,86453,109297,270343,5794777,8177207",
			"name": "Indices of prime repunits: numbers n such that 11...111 (with n 1's) = (10^n - 1)/9 is prime.",
			"comment": [
				"People who search for repunit primes or repdigit primes may be looking for this entry.",
				"The indices of primes with digital product (i.e., product of digits) equal to 1.",
				"As of August 2014, only the first five repunits, through (10^1031-1)/9, have been proved prime. The last four repunits are known only to be probable primes and have not been proved to be prime. - _Robert Baillie_, Aug 17 2014",
				"These indices p must also be prime. If p is not prime, say p = m*n, then 10^(m*n) - 1 = ((10^m)^n) - 1 =\u003e 10^m - 1 divides 10^(m*n) - 1. Since 9 divides 10^m - 1 or (10^m - 1)/9 = q, it follows q divides (10^p - 1)/9. This is a result of the identity, a^n - b^n = (a - b)(a^(n-1) + a^(n-2)*b + ... + b^(n-1)). - _Cino Hilliard_, Dec 23 2008",
				"The numbers R_n = 11...111 = (10^n - 1)/9 with n in this sequence A004023, except for n = 2, are prime repunits in base ten, so they are prime Brazilian numbers belonging to A085104. [See Links: Les nombres brésiliens.] - _Bernard Schott_, Dec 24 2012",
				"Search limit is 10800000, currently. - _Serge Batalov_, July 01 2021"
			],
			"reference": [
				"J. Brillhart et al., Factorizations of b^n +- 1. Contemporary Mathematics, Vol. 22, Amer. Math. Soc., Providence, RI, 2nd edition, 1985; and later supplements.",
				"J.-M. De Koninck, Ces nombres qui nous fascinent, Entry 19, pp 6, Ellipses, Paris 2008.",
				"Graham, Knuth and Patashnik, Concrete Mathematics, Addison-Wesley, 1994; see p 146 problem 22.",
				"Clifford A. Pickover, A Passion for Mathematics, Wiley, 2005; see p. 60.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Paul Bourdelais, \u003ca href=\"https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;417ab0d6.0906\"\u003eA Generalized Repunit Conjecture\u003c/a\u003e, NMBRTHRY, 25 Jun 2009.",
				"John Brillhart, \u003ca href=\"/A001562/a001562.pdf\"\u003eLetter to N. J. A. Sloane, Aug 08 1970\u003c/a\u003e",
				"John Brillhart et al., \u003ca href=\"http://dx.doi.org/10.1090/conm/022\"\u003eFactorizations of b^n +- 1\u003c/a\u003e, Contemporary Mathematics, Vol. 22, Amer. Math. Soc., Providence, RI, 3rd edition, 2002.",
				"Chris K. Caldwell, The Prime Glossary, \u003ca href=\"http://primes.utm.edu/glossary/page.php?sort=Repunit\"\u003eRepunit\u003c/a\u003e",
				"Patrick De Geest, \u003ca href=\"http://www.worldofnumbers.com/circular.htm\"\u003eCircular Primes\u003c/a\u003e",
				"Giovanni Di Maria, \u003ca href=\"http://www.elektrosoft.it/matematica/repunit/repunit.htm\"\u003eRepunit Primes Project\u003c/a\u003e",
				"Harvey Dubner, \u003ca href=\"/A028491/a028491.pdf\"\u003eGeneralized repunit primes\u003c/a\u003e, Math. Comp., 61 (1993), 927-930. [Annotated scanned copy]",
				"Harvey Dubner, \u003ca href=\"https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;1d525256.9909\"\u003eNew probable prime repunit, R(49081)\u003c/a\u003e, Number Theory List, Sep 09 1999.",
				"Harvey Dubner, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-01-01319-9\"\u003eRepunit R49081 is a probable prime\u003c/a\u003e, Math. Comp., 71 (2001), 833-835.",
				"Harvey Dubner, \u003ca href=\"https://listserv.nodak.edu/cgi-bin/wa.exe?A2=NMBRTHRY;a9325f9e.0704\"\u003ePosting to Number Theory List : Apr 03 2007\u003c/a\u003e",
				"Martianus Frederic Ezerman, Bertrand Meyer, and Patrick Solé, \u003ca href=\"http://arxiv.org/abs/1210.7593\"\u003eOn Polynomial Pairs of Integers\u003c/a\u003e, arXiv:1210.7593 [math.NT], 2012-2014.",
				"Martianus Frederic Ezerman, Bertrand Meyer and Patrick Solé, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL18/Ezerman/eze3.html\"\u003eOn Polynomial Pairs of Integers\u003c/a\u003e, Journal of Integer Sequences, Vol. 18 (2015), Article 15.3.5.",
				"Makoto Kamada, \u003ca href=\"https://stdkmd.net/nrr/repunit\"\u003eFactorizations of 11...11 (Repunit)\u003c/a\u003e.",
				"Henri Lifchitz, \u003ca href=\"http://www.primenumbers.net/Henri/us/MersFermus.htm\"\u003eMersenne and Fermat primes field\u003c/a\u003e",
				"T. Muller, \u003ca href=\"https://doi.org/10.4171/EM/183\"\u003eIst die Folge der Primzahl-quersummen beschrankt?\u003c/a\u003e, Elem. Math. 66 (2011) 146-154; doi:10.4171/EM/183.",
				"Bernard Schott, \u003ca href=\"/A125134/a125134.pdf\"\u003eLes nombres brésiliens\u003c/a\u003e, Quadrature, no. 76, avril-juin 2010, pages 30-38; included here with permission from the editors of Quadrature.",
				"Andy Steward, \u003ca href=\"http://www.users.globalnet.co.uk/~aads/primes.html\"\u003ePrime Generalized Repunits\u003c/a\u003e",
				"Sam Wagstaff, Jr., \u003ca href=\"http://www.cerias.purdue.edu/homes/ssw/cun/index.html\"\u003eThe Cunningham Project\u003c/a\u003e",
				"E. Wegrzynowski, \u003ca href=\"http://www.lifl.fr/~wegrzyno/RepunitBase10/repbase101.html\"\u003eNombres 1_[n] premiers\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/IntegerSequencePrimes.html\"\u003eInteger Sequence Primes\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/Repunit.html\"\u003eRepunit\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RepunitPrime.html\"\u003eRepunit Prime\u003c/a\u003e",
				"H. C. Williams and Harvey Dubner, \u003ca href=\"http://dx.doi.org/10.1090/S0025-5718-1986-0856714-3\"\u003eThe primality of R1031\u003c/a\u003e, Math. Comp., 47(176), Oct 1986, 703-711.",
				"\u003ca href=\"/index/Pri#Pri_rep\"\u003eIndex entries for primes involving repunits\u003c/a\u003e"
			],
			"example": [
				"2 appears because the 2-digit repunit 11 is prime.",
				"3 does not appear because 111 = 3 * 37 is not prime.",
				"19 appears because the 19-digit repunit 1111111111111111111 is prime."
			],
			"mathematica": [
				"Select[Range[271000], PrimeQ[FromDigits[PadRight[{}, #, 1]]] \u0026] (* _Harvey P. Dale_, Nov 05 2011 *)",
				"repUnsUpTo[k_] := ParallelMap[If[PrimeQ[#] \u0026\u0026 PrimeQ[(10^# - 1)/9], #, Nothing] \u0026, Range[k]]; repUnsUpTo[5000] (* _Mikk Heidemaa_, Apr 24 2017 *)"
			],
			"program": [
				"(PARI) forprime(x=2,20000,if(ispseudoprime((10^x-1)/9),print1(x\",\"))) \\\\ _Cino Hilliard_, Dec 23 2008",
				"(MAGMA) [p: p in PrimesUpTo(500) | IsPrime((10^p - 1) div 9)]; // _Vincenzo Librandi_, Nov 06 2014",
				"(Python) from sympy import isprime; {print(n, end = ', ') for n in range(1, 10**7) if isprime(int('1'*n))} # _Ya-Ping Lu_, Dec 20 2021"
			],
			"xref": [
				"See A004022 for the actual primes.",
				"Cf. A055557, A002275, A085104."
			],
			"keyword": "hard,nonn,nice,more",
			"offset": "1,1",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"49081 found by Harvey Dubner - posting to Number Theory List (NMBRTHRY(AT)LISTSERV.NODAK.EDU) Sep 09, 1999.",
				"86453 found using pfgw (a faster version of PrimeForm) on Oct 26 2000 by Lew Baxter (ldenverb(AT)hotmail.com) - posting to Number Theory List (NMBRTHRY(AT)LISTSERV.NODAK.EDU), Oct 26, 2000.",
				"a(8) = 109297 was apparently discovered independently by (in alphabetical order) _Paul Bourdelais_ and Harvey Dubner (harvey(AT)dubner.com) around Mar 26-28 2007.",
				"A new probable prime repunit, R(270343), was found Jul 11 2007 by Maksym Voznyy (mvoznyy0526(AT)ROGERS.COM) and Anton Budnyy.",
				"270343 subsequently confirmed as a(9) (see Repunit Primes Project link) by _Robert Price_, Dec 14 2010",
				"Link to Repunit Primes Project site updated by _Felix Fröhlich_, Oct 19 2014",
				"a(10) = 5794777 was found Apr 20 2021 by _Ryan Propper_ and _Serge Batalov_",
				"a(11) = 8177207 was found May 08 2021 by _Ryan Propper_ and _Serge Batalov_"
			],
			"references": 131,
			"revision": 211,
			"time": "2021-12-21T07:42:41-05:00",
			"created": "1991-04-30T03:00:00-04:00"
		}
	]
}