{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A224360",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 224360,
			"data": "0,1,1,4,2,4,2,0,5,9,4,3,7,10,4,5,3,6,11,5,8,4,1,0,11,1,9,14,3,6,8,13,6,8,15,4,11,4,9,12,3,10,5,5,17,4,4,7,0,2,11,16,4,18,36,6,4,14,12,4,9,16,6,9,37,13,6,5,1,16,7,13,6,1,19,16,14,7,9",
			"name": "Triangle read by rows: T(n,k) = -1 + length of the Collatz sequence of -(n-k)/(2k+1) for n \u003e= 1 and k \u003e= 0.",
			"comment": [
				"This sequence is an extension of A210516 with negative values.",
				"We consider the triangle T(n,k) = -(n-k)/(2k+1) for n = 1,2,... and k = 0..n-1.",
				"The example shown below gives a general idea of this regular triangle. This contains all negative fractions whose denominator is odd and all integers. Now, from T(n,k) we could introduce a 3D triangle in order to produce a complete Collatz sequence starting from each rational T(n,k).",
				"The initial triangle T(n,k) begins",
				"  -1,",
				"  -2, -1/3;",
				"  -3, -2/3, -1/5;",
				"  -4, -3/3, -2/5, -1/7;",
				"  -5, -4/3, -3/5, -2/7, -1/9;",
				"  -6, -5/3, -4/5, -3/7, -2/9, -1/11;",
				"  ..."
			],
			"example": [
				"The triangle of lengths begins",
				"  1;",
				"  2, 2;",
				"  5, 3, 5;",
				"  3, 1, 6, 10;",
				"  5, 4, 8, 11, 5;",
				"  ...",
				"Individual numbers have the following Collatz sequences (the first term is not counted):",
				"[-1] =\u003e [1] because: -1 -\u003e -1 with 0 iterations;",
				"[-2 -1/3] =\u003e [1, 1] because: -2 -\u003e -1 =\u003e 1 iteration; -1/3 -\u003e 0 =\u003e 1 iteration;",
				"[-3 -2/3 -1/5] =\u003e [4, 2, 4] because: -3 -\u003e -8 -\u003e -4 -\u003e -2 -\u003e -1 =\u003e 4 iterations; -2/3 -\u003e -1/3 -\u003e 0 =\u003e 2 iterations; -1/5 -\u003e 2/5 -\u003e 1/5 -\u003e 8/5 -\u003e 4/5 =\u003e 4 iterations."
			],
			"mathematica": [
				"Collatz2[n_] := Module[{lst = NestWhileList[If[EvenQ[Numerator[#]], #/2, 3 # + 1] \u0026, n, Unequal, All]}, If[lst[[-1]] == -1, lst = Drop[lst, -2], If[lst[[-1]] == 2, lst = Drop[lst, -2], If[lst[[-1]] == 4, lst = Drop[lst, -1], If[MemberQ[Rest[lst], lst[[-1]]], lst = Drop[lst, -1]]]]]]; t = Table[s = Collatz2[-(n - k)/(2*k + 1)]; Length[s]-1 , {n, 13}, {k, 0, n - 1}]; Flatten[t] (* program from _T. D. Noe_, adapted for this sequence - see A210516 *)."
			],
			"xref": [
				"Cf. A210516, A210688, A224299, A224300, A224361."
			],
			"keyword": "nonn,tabl",
			"offset": "1,4",
			"author": "_Michel Lagneau_, Apr 04 2013",
			"ext": [
				"Better definition from _Michel Marcus_, Sep 14 2017"
			],
			"references": 2,
			"revision": 22,
			"time": "2020-03-04T16:20:57-05:00",
			"created": "2013-04-04T19:03:40-04:00"
		}
	]
}