{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A295380",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 295380,
			"data": "1,1,1,2,3,1,3,8,5,1,6,20,22,8,1,11,49,73,46,11,1,23,119,233,206,87,15,1,46,288,689,807,485,147,19,1,98,696,1988,2891,2320,1021,236,24,1,207,1681,5561,9737,9800,5795,1960,356,29,1,451,4062,15322,31350,38216,28586,13088,3525,520,35,1,983,9821,41558,97552,139901,127465,74280,27224,5989,730,41,1",
			"name": "Number of canonical forms for separation coordinates on hyperspheres S_n, ordered by increasing number of independent continuous parameters.",
			"comment": [
				"Table 1 of the Schöbel and Veselov paper with initial 1 added. Reverse of Table 2 of the Devadoss and Read paper.",
				"Apparently A032132 contains the row sums.",
				"From _Petros Hadjicostas_, Jan 28 2018: (Start)",
				"In this triangle, which is read by rows, for 0 \u003c= k \u003c= n-1 and n\u003e=1, let T(n,k) be the number of inequivalent canonical forms for separation coordinates of the hypersphere S^n with k independent continuous parameters. It is the mirror image of sequence A232206, that is, T(n, k) = A232206(n+1, n-k) for 0 \u003c= k \u003c= n-1 and n\u003e=1. (Triangular array A232206(N, K) is defined for N \u003e= 2 and 1 \u003c= K \u003c= N-1.)",
				"If B(x,y) = Sum_{n,k\u003e=0} T(n,k)*x^n*y^k (with T(0,0) = 1, T(0,k) = 0 for k\u003e=1, and T(n,k) = 0 for 1 \u003c= n \u003c= k), then B(x,y) = 1 + (x/2)*(B(x,y)^2/(1-x*y*B(x,y)) + (1 + x*y*B(x,y))*B(x^2,y^2)/(1-x^2*y^2*B(x^2,y^2))). This can be derived from the bivariate g.f. of A232206. See the comments for that sequence.",
				"Let S(n) := Sum_{k\u003e=0} T(n,k). The g.f. of S(n) is B(x, y=1). If we let y=1 in the above functional equation, we get x*B(x,1) = x + (1/2)*((x*B(x,1))^2/(1-x*B(x,1)) + (1 + x*B(x,1))*x^2*B(x^2,1)/(1-x^2*B(x^2,1))). After some algebra, we get 2*x*B(x,1) = x + (1/2)(x*B(x,1)/(1-x*B(x,1)) + (x*B(x,1) + x^2*B(x^2,1))/(1-x^2*B(x,1))), i.e., 2*x*B(x,1) = x + BIK(x*B(x,1)), where we have the \"BIK\" (reversible, indistinct, unlabeled) transform of C. G. Bower. This proves that S(n) = A032132(n+1) for n\u003e=0, which is Copeland's claim above.",
				"Note that for the second column we have T(n,k=2) = A048739(n-2) for 2 \u003c= n \u003c = 10, but T(11,2) = 4062 \u003c\u003e 4059 = A048739(9). In any case, they have different g.f.s (see the formula section below).",
				"(End)"
			],
			"link": [
				"C. G. Bower, \u003ca href=\"/transforms2.html\"\u003eTransforms (2)\u003c/a\u003e",
				"S. Devadoss and R. C. Read, \u003ca href=\"https://arxiv.org/abs/math/0008145\"\u003eCellular structures determined by polygons and trees\u003c/a\u003e, arXiv/0008145 [math.CO], 2000.",
				"S. L. Devadoss and R. C. Read, \u003ca href=\"https://doi.org/10.1007/PL00001293\"\u003eCellular structures determined by polygons and trees\u003c/a\u003e, Ann. Combin., 5 (2001), 71-98.",
				"K. Schöbel and A. Veselov, \u003ca href=\"https://arxiv.org/abs/1307.6132\"\u003eSeparation coordinates, moduli spaces, and Stasheff polytopes\u003c/a\u003e, arXiv:1307.6132 [math.DG], 2014.",
				"K. Schöbel and A. Veselov, \u003ca href=\"https://doi.org/10.1007/s00220-015-2332-x\"\u003eSeparation coordinates, moduli spaces and Stasheff polytopes\u003c/a\u003e, Commun. Math. Phys., 337 (2015), 1255-1274."
			],
			"formula": [
				"From _Petros Hadjicostas_, Jan 28 2018: (Start)",
				"G.f.: If B(x,y) = Sum_{n,k\u003e=0} T(n,k)*x^n*y^k (with T(0,0) = 1, T(0,k) = 0 for k\u003e=1, and T(n,k) = 0 for 1 \u003c= n \u003c= k), then B(x,y) = 1 + (x/2)*(B(x,y)^2/(1-x*y*B(x,y)) + (1 + x*y*B(x,y))*B(x^2,y^2)/(1-x^2*y^2*B(x^2,y^2))).",
				"If c(N,K) = A232206(N,K) and C(x,y) = Sum_{N,K\u003e=0} c(N,K)*x^N*y^K (with c(1,0) = 1 and c(N,K) = 0 for 0 \u003c= N \u003c= K), then C(x,y) = x*B(x*y, 1/y) and B(x,y) = C(x*y, 1/y)/(x*y).",
				"Setting y=0 in the above functional equation, we get x*B(x,0) = x + (1/2)*((x*B(x,0))^2 + x^2*B(x^2,0)), which is the functional equation for the g.f. of the first column. This proves that T(n,k=0) = A001190(n+1) for n\u003e=0 (assuming T(0,0) = 1).",
				"The g.f. of the second column is B_1(x,0) = Sum_{n\u003e=0} T(n,2)*x^n = lim_{y-\u003e0} (B(x,y)-B(x,0))/y, where B(x,0) = 1 + x + x^2 + ... is the g.f. of the first column. We get B_1(x,0) = x*B(x,0)*(B(x,0) - 1)/(1 - x*B(x,0)).",
				"(End)"
			],
			"example": [
				"From _Petros Hadjicostas_, Jan 27 2018: (Start)",
				"Triangle T(n,k) begins:",
				"n\\k      0     1     2     3     4     5     6    7   8  9",
				"----------------------------------------------------------------",
				"(S^1)    1,",
				"(S^2)    1,    1,",
				"(S^3)    2,    3,    1,",
				"(S^4)    3,    8,    5,    1,",
				"(S^5)    6,   20,   22,    8,    1,",
				"(S^6)   11,   49,   73,   46,   11,    1,",
				"(S^7)   23,  119,  233,  206,   87,   15,    1,",
				"(S^8)   46,  288,  689,  807,  485,  147,   19,   1,",
				"(S^9)   98,  696, 1988, 2891, 2320, 1021,  236,  24,  1,",
				"(S^10) 207, 1681, 5561, 9737, 9800, 5795, 1960, 356, 29, 1,",
				"...",
				"(End)"
			],
			"xref": [
				"Cf. A001190, A024206, A032132, A232206."
			],
			"keyword": "nonn,tabl",
			"offset": "1,4",
			"author": "_Tom Copeland_, Nov 21 2017",
			"ext": [
				"Typo for T(11,3)=15322 corrected by _Petros Hadjicostas_, Jan 28 2018"
			],
			"references": 1,
			"revision": 66,
			"time": "2018-01-31T04:02:30-05:00",
			"created": "2017-11-22T02:31:20-05:00"
		}
	]
}