{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A331905",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 331905,
			"data": "1,4,12,128,605,3072,16807,82944,412164,2035220,9864899,47579136,227902597,1084320412,5134860060,24207040512,113664879137,531895993344,2481300851179,11543181696640,53565699079956,248005494380204,1145875775104967,5284358088818688",
			"name": "Number of spanning trees in the multigraph cube of an n-cycle.",
			"comment": [
				"The multigraph cube of an n-cycle has n nodes V1, V2, ... Vn, with one edge Vi to Vj for each pair (i,j) such that j = i+1, i+2 or i+3 modulo n. It is a multigraph when n \u003c= 6 because this produces instances of multiple edges between the same two vertices, and it also produces loops if n \u003c= 3.",
				"Baron et al. (1985) describes the corresponding sequence A169630 for the multigraph square of a cycle.",
				"I conjecture that a(n) = gcd(n,2) * n * (A005822(n))^2. [This is correct - see the Formula section. - _N. J. A. Sloane_, Feb 06 2020)",
				"Terms a(7) to a(18) calculated by _Brendan McKay_, and terms a(1) to a(6) by _David J. Seal_, in both cases using Kirchhoff's matrix tree theorem."
			],
			"link": [
				"Alois P. Heinz, \u003ca href=\"/A331905/b331905.txt\"\u003eTable of n, a(n) for n = 1..1546\u003c/a\u003e",
				"G. Baron et al., \u003ca href=\"http://www.fq.math.ca/Scanned/23-3/baron.pdf\"\u003eThe number of spanning trees in the square of a cycle\u003c/a\u003e, Fib. Quart., 23 (1985), 258-264.",
				"Min Li, Zhibing Chen, Xiaoqing Ruan, Xuerong Yong, \u003ca href=\"https://doi.org/10.1016/j.disc.2015.04.025\"\u003eThe formulas for the number of spanning trees in circulant graphs\u003c/a\u003e, Disc. Math. 338 (11) (2015) 1883-1906, Lemma 1.",
				"Tsuyoshi Miezaki, \u003ca href=\"/A331905/a331905.pdf\"\u003eA note on spanning trees.\u003c/a\u003e"
			],
			"formula": [
				"The following formulas were provided by Tsuyoshi Miezaki on Feb 05 2020 (see link). Let z1=(-3+sqrt(-7))/4, z2=(-3-sqrt(-7))/4; T(n,z) = cos(n*arccos(z)). Then a(n) = (2*n/7)*(T(n,z1)-1)*(T(n,z2)-1). Furthermore a(n) = 2*n*A005822(n)^2 if n is even, or n*A005822(n)^2 if n is odd. - _N. J. A. Sloane_, Feb 06 2020"
			],
			"example": [
				"The multigraph cube of a 4-cycle has four vertices, with two edges between each pair of distinct vertices - i.e., it is a doubled-edge cover of the complete graph on 4 vertices. The complete graph on 4 vertices has 4^2 = 16 spanning trees, and each of those spanning trees corresponds to 8 spanning trees of the multigraph tree because there are independent choices of 2 multigraph edges to be made for each of the three edges in the graph's spanning tree. So a(4) = 16 * 8 = 128."
			],
			"maple": [
				"a:= n-\u003e ((\u003c\u003c0|1|0|0\u003e, \u003c0|0|1|0\u003e, \u003c0|0|0|1\u003e, \u003c-1|4|1|4\u003e\u003e^iquo(n, 2, 'd').",
				"       \u003c[\u003c0, 1, 4, 16\u003e, \u003c1, 2, 11, 49\u003e][d+1]\u003e)[1, 1])^2*n*(2-irem(n, 2)):",
				"seq(a(n), n=1..30);  # _Alois P. Heinz_, Feb 06 2020"
			],
			"xref": [
				"Cf. A005822, A169630 (corresponding sequence for the multigraph square of an n-cycle)."
			],
			"keyword": "nonn",
			"offset": "1,2",
			"author": "_David J. Seal_, Jan 31 2020",
			"ext": [
				"More terms from _Alois P. Heinz_, Feb 06 2020"
			],
			"references": 2,
			"revision": 34,
			"time": "2020-02-06T12:50:40-05:00",
			"created": "2020-02-06T08:24:39-05:00"
		}
	]
}