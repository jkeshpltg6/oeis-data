{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A081658",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 81658,
			"data": "1,1,0,1,0,-1,1,0,-3,0,1,0,-6,0,5,1,0,1,-10,0,25,0,1,0,-15,0,75,0,-61,1,0,-21,0,175,0,-427,0,1,0,-28,0,350,0,-1708,0,1385,1,0,-36,0,630,0,-5124,0,12465,0,1,0,-45,0,1050,0,-12810,0,62325,0,-50521,1,0,-55,0,1650,0,-28182,0,228525,0,-555731,0,1,0,-66,0,2475,0",
			"name": "Triangle read by rows, coefficients of polynomials related to the Euler numbers ordered by falling powers.",
			"comment": [
				"These are the coefficients of the Swiss-Knife polynomials A153641. - _Peter Luschny_, Jul 21 2012",
				"Nonzero diagonals of the triangle are of the form A000364(k)*binomial(n+2k,2k)*(-1)^k."
			],
			"formula": [
				"Coefficients of the polynomials in k in the binomial transform of the expansion of 2/(exp(kx)+exp(-kx)).",
				"From _Peter Luschny_, Jul 20 2012: (Start)",
				"p{n}(0) = Signed Euler secant numbers A122045.",
				"p{n}(1) = Signed Euler tangent numbers A155585.",
				"p{n}(2) has e.g.f. 2*exp(x)/(exp(-2*x)+1) A119880.",
				"2^n*p{n}(1/2) = Signed Springer numbers A188458.",
				"3^n*p{n}(1/3) has e.g.f. 2*exp(4*x)/(exp(6*x)+1)",
				"4^n*p{n}(1/4) has e.g.f. 2*exp(5*x)/(exp(8*x)+1).",
				"Row sum: A155585 (cf. A009006). Absolute row sum: A003701.",
				"The GCD of the rows without the first column: A155457. (End)"
			],
			"example": [
				"The triangle begins",
				"[0] 1;",
				"[1] 1, 0;",
				"[2] 1, 0,  -1;",
				"[3] 1, 0,  -3, 0;",
				"[4] 1, 0,  -6, 0,   5;",
				"[5] 1, 0, -10, 0,  25, 0;",
				"[6] 1, 0, -15, 0,  75, 0,  -61;",
				"[7] 1, 0, -21, 0, 175, 0, -427, 0;",
				"...",
				"From _Peter Luschny_, Sep 17 2021: (Start)",
				"The triangle shows the coefficients of the following polynomials:",
				"[1] 1;",
				"[2] 1 -    x^2;",
				"[3] 1 -  3*x^2;",
				"[4] 1 -  6*x^2 +   5*x^4;",
				"[5] 1 - 10*x^2 +  25*x^4;",
				"[6] 1 - 15*x^2 +  75*x^4 -  61*x^6;",
				"[7] 1 - 21*x^2 + 175*x^4 - 427*x^6;",
				"...",
				"These polynomials are the permanents of the n X n matrices with all entries above the main antidiagonal set to 'x' and all entries below the main antidiagonal set to '-x'. The main antidiagonals consist only of ones. Substituting x \u003c- 1 generates the Euler tangent numbers A155585. (Compare with A046739.)",
				"(End)"
			],
			"mathematica": [
				"sk[n_, x_] := Sum[Binomial[n, k]*EulerE[k]*x^(n - k), {k, 0, n}];",
				"Table[CoefficientList[sk[n, x], x] // Reverse, {n, 0, 12}] // Flatten (* _Jean-François Alcover_, Jun 04 2019 *)"
			],
			"program": [
				"(Sage)",
				"R = PolynomialRing(ZZ, 'x')",
				"@CachedFunction",
				"def p(n, x) :",
				"    if n == 0 : return 1",
				"    return add(p(k, 0)*binomial(n, k)*(x^(n-k)-(n+1)%2) for k in range(n)[::2])",
				"def A081658_row(n) : return [R(p(n,x)).reverse()[i] for i in (0..n)]",
				"for n in (0..8) : print(A081658_row(n)) # _Peter Luschny_, Jul 20 2012"
			],
			"xref": [
				"Row reversed: A119879.",
				"Cf. A000364, A046739, A155585."
			],
			"keyword": "easy,sign,tabl",
			"offset": "0,9",
			"author": "_Paul Barry_, Mar 26 2003",
			"ext": [
				"Typo in data corrected by _Peter Luschny_, Jul 20 2012"
			],
			"references": 4,
			"revision": 30,
			"time": "2021-09-18T10:51:01-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}