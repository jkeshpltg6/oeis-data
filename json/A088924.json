{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A088924",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 88924,
			"data": "1,18,252,3168,37512,427608,4748472,51736248,555626232,5900636088,62105724792,648951523128,6740563708152,69665073373368,716985660360312,7352870943242808,75175838489185272,766582546402667448",
			"name": "Number of \"9ish numbers\" with n digits.",
			"comment": [
				"First difference of A016189. (\"9\" can be replaced by any other nonzero digit, however only the 9ish numbers are closed under lunar multiplication.)",
				"See A257285 - A257289 for first differences of 5^n-4^n, ..., 9^n-8^n. These also give the number of n-digit numbers whose largest digit is 5, 6, 7, 8, respectively. - _M. F. Hasler_, May 04 2015"
			],
			"link": [
				"D. Applegate, \u003ca href=\"/A087061/a087061.txt\"\u003eC program for lunar arithmetic and number theory\u003c/a\u003e [Note: we have now changed the name from \"dismal arithmetic\" to \"lunar arithmetic\" - the old name was too depressing]",
				"D. Applegate, M. LeBrun and N. J. A. Sloane, \u003ca href=\"http://arxiv.org/abs/1107.1130\"\u003eDismal Arithmetic\u003c/a\u003e [Note: we have now changed the name from \"dismal arithmetic\" to \"lunar arithmetic\" - the old name was too depressing]",
				"\u003ca href=\"/index/Di#dismal\"\u003eIndex entries for sequences related to dismal (or lunar) arithmetic\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_02\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (19,-90)."
			],
			"formula": [
				"a(n) = 9*10^(n-1) - 8*9^(n-1).",
				"G.f.: (x*(1 - x))/(1 - 19*x + 90*x^2). - _Bobby Milazzo_, May 02 2014",
				"a(n) = 19*a(n-1) - 90*a(n-2). - _Vincenzo Librandi_, May 04 2015"
			],
			"example": [
				"a(2) = 18 because 19, 29, 39, 49, 59, 69, 79, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98 and 99 are the eighteen two-digit 9ish numbers."
			],
			"maple": [
				"A088924:=n-\u003e9*10^(n-1) - 8*9^(n-1); seq(A088924(n), n=1..30); # _Wesley Ivan Hurt_, May 15 2014"
			],
			"mathematica": [
				"Series[(x (1 - x))/(1 - 19 x + 90 x^2), {x, 0, 10}] (* _Bobby Milazzo_, May 02 2014 *)",
				"Table[9*10^(n - 1) - 8*9^(n - 1), {n, 30}] (* _Wesley Ivan Hurt_, May 15 2014 *)"
			],
			"program": [
				"(PARI) a(n)=9*10^n-8*9^n \\\\ _M. F. Hasler_, May 04 2015",
				"(MAGMA) [9*10^(n-1) - 8*9^(n-1): n in [1..30]]; // _Vincenzo Librandi_, May 04 2015"
			],
			"xref": [
				"Cf. A016189, A011539."
			],
			"keyword": "base,easy,nonn",
			"offset": "1,2",
			"author": "_Marc LeBrun_, Oct 23 2003",
			"references": 7,
			"revision": 37,
			"time": "2015-06-13T00:51:15-04:00",
			"created": "2004-02-19T03:00:00-05:00"
		}
	]
}