{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A226556",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 226556,
			"data": "1,1,1,0,-1,-1,-1,-1,0,1,2,3,2,0,-2,-4,-4,-3,-1,3,6,7,5,0,-5,-9,-10,-7,-1,7,14,16,11,1,-11,-20,-22,-16,-2,15,29,33,23,2,-23,-41,-45,-32,-4,30,57,64,45,4,-43,-78,-86,-60,-7,57,107,119,83,8,-79",
			"name": "Expansion of f(x, -x^4) / f(-x^2, x^3) in powers of x where f(,) is Ramanujan's general theta function.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"Given the g.f. A(x), then S(q) := q^(1/5) * A(q) notation is used by Berndt."
			],
			"reference": [
				"G. E. Andrews and B. C. Berndt, Ramanujan's Lost Notebook, Part I, Springer, 2005, see p. 57.",
				"B. C. Berndt, Ramanujan's Notebooks Part V, Springer-Verlag, see p. 9."
			],
			"link": [
				"Seiichi Manyama, \u003ca href=\"/A226556/b226556.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e",
				"O. Marichev and M. Trott, \u003ca href=\"http://blog.wolfram.com/2013/05/01/after-100-years-ramanujan-gap-filled/\"\u003eAfter 100 Years, Ramanujan Gap Filled\u003c/a\u003e"
			],
			"formula": [
				"Given g.f. A(x), then B(x) = x * A(x^5) satisfies 0 = f(B(x), B(x^3)) where f(u, v) = (u^3 - v) * (1 + u*v^3) - 3 * u^2*v^2.",
				"Given g.f. A(x), then B(x) = x * A(x^5) satisfies 0 = f(B(x), B(x^5)) where f(u, v) = u^5 * (1 - 3*v + 4*v^2 - 2*v^3 + v^4) - v * (1 + 2*v + 4*v^2 + 3*v^3 + v^4).",
				"Euler transform of period 20 sequence [1, 0, -1, -1, 0, 0, -1, 1, 1, 0, 1, 1, -1, 0, 0, -1, -1, 0, 1, 0, ...].",
				"G.f.: (Sum_{k in Z} (-1)^k * (-x)^((5*k + 3)*k / 2)) / (Sum_{k in Z} (-1)^k * (-x)^((5*k + 1)*k / 2)).",
				"G.f.: 1 / (1 - x / (1 + x^2 / (1 - x^3 / ...))). [continued fraction]",
				"a(n) = (-1)^n * A007325(n).",
				"G.f.: 1/G(0), where G(k)= 1 + (-x)^(k+1)/G(k+1) ; (continued fraction). - _Sergei N. Gladkovskii_, Jun 29 2013"
			],
			"example": [
				"G.f. = 1 - x + x^2 - x^4 + x^5 - x^6 + x^7 - x^9 + 2*x^10 - 3*x^11 + 2*x^12 + ...",
				"G.f. = q - q^6 + q^11 - q^21 + q^26 - q^31 + q^36 - q^46 + 2*q^51 - 3*q^56 + 2*q^61 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ ContinuedFractionK[ (-q)^k, 1, {k, 0, n}], {q, 0, n}];",
				"a[ n_] := SeriesCoefficient[ QPochhammer[ -q, -q^5] QPochhammer[ q^4, -q^5] / (QPochhammer[ q^2, -q^5] QPochhammer[-q^3, -q^5]), {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = my(k); if( n\u003c0, 0, k = (3 + sqrtint(9 + 40*n)) \\ 10; polcoeff( sum( n=-k, k, (-1)^n * (-x)^( (5*n^2 + 3*n) / 2), x * O(x^n)) / sum( n=-k, k, (-1)^n * (-x)^( (5*n^2 + n) / 2), x * O(x^n)), n))};"
			],
			"xref": [
				"Cf. A007325."
			],
			"keyword": "sign",
			"offset": "0,11",
			"author": "_Michael Somos_, Jun 10 2013",
			"references": 2,
			"revision": 20,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2013-06-11T10:34:42-04:00"
		}
	]
}