{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A172375",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 172375,
			"data": "1,1,1,1,6,1,1,48,48,1,1,352,2816,352,1,1,2640,154880,154880,2640,1,1,19680,8659200,63500800,8659200,19680,1,1,146944,481976320,26508697600,26508697600,481976320,146944,1,1,1096704,26859012096,11012194959360,82591462195200,11012194959360,26859012096,1096704,1",
			"name": "Triangle T(n, k, q) = c(n-1, q)*c(n, q)/(c(k-1, q)^2*c(n-k, q)*c(n-k+1, q)*f(k, q)), where c(n, q) = Product_{j=1..n} f(j, q), f(n, q) = q*(f(n-1, q) + f(n-2, q)), f(0, q) = 0, f(1, q) = 1, and q = 2, read by rows.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A172375/b172375.txt\"\u003eRows n = 1..30 of the triangle, flattened\u003c/a\u003e"
			],
			"formula": [
				"T(n, k, q) = c(n-1, q)*c(n, q)/(c(k-1, q)^2*c(n-k, q)*c(n-k+1, q)*f(k, q)), where c(n, q) = Product_{j=1..n} f(j, q), f(n, q) = q*(f(n-1, q) + f(n-2, q)), f(0, q) = 0, f(1, q) = 1, and q = 2.",
				"T(n, k, q) = c(n-1, q)*c(n, q)/(c(k-1, q)^2*c(n-k, q)*c(n-k+1, q)*f(k,q)), where c(n, q) = Product_{j=1..n} f(j, q), and f(n, q) = (-I*sqrt(q))^(n-1)*ChebyshevU(n-1, i*sqrt(q)/2). - _G. C. Greubel_, May 07 2021"
			],
			"example": [
				"Triangle begins as:",
				"  1;",
				"  1,      1;",
				"  1,      6,         1;",
				"  1,     48,        48,           1;",
				"  1,    352,      2816,         352,           1;",
				"  1,   2640,    154880,      154880,        2640,         1;",
				"  1,  19680,   8659200,    63500800,     8659200,     19680,      1;",
				"  1, 146944, 481976320, 26508697600, 26508697600, 481976320, 146944, 1;"
			],
			"mathematica": [
				"f[n_, q_]:= (-I*Sqrt[q])^(n-1)*ChebyshevU[n-1, I*Sqrt[q]/2];",
				"c[n_, q_]:= Product[f[j, q], {j,n}];",
				"T[n_, k_, q_]:= c[n-1, q]*c[n, q]/(c[k-1, q]^2*c[n-k, q]*c[n-k+1, q]*f[k, q]);",
				"Table[T[n, k, 2], {n,12}, {k, n}]//Flatten (* modified by _G. C. Greubel_, May 07 2021 *)"
			],
			"program": [
				"(Sage)",
				"@CachedFunction",
				"def f(n,q): return (-i*sqrt(q))^(n-1)*chebyshev_U(n-1, i*sqrt(q)/2)",
				"def c(n,q): return product( f(j,q) for j in (1..n) )",
				"def T(n,k,q): return c(n-1, q)*c(n, q)/(c(k-1, q)^2*c(n-k, q)*c(n-k+1, q)*f(k, q))",
				"flatten([[T(n,k,2) for k in (1..n)] for n in (1..12)]) # _G. C. Greubel_, May 07 2021"
			],
			"xref": [
				"Cf. A002605, A030195, this sequence (q=2), A172376 (q=3)."
			],
			"keyword": "nonn",
			"offset": "1,5",
			"author": "_Roger L. Bagula_, Feb 01 2010",
			"ext": [
				"Edited by _G. C. Greubel_, May 07 2021"
			],
			"references": 2,
			"revision": 8,
			"time": "2021-05-08T01:41:22-04:00",
			"created": "2010-06-01T03:00:00-04:00"
		}
	]
}