{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A005436",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 5436,
			"id": "M1778",
			"data": "1,2,7,28,120,528,2344,10416,46160,203680,894312,3907056,16986352,73512288,316786960,1359763168,5815457184,24788842304,105340982248,446389242480,1886695382192,7955156287456,33468262290096,140516110684832,588832418973280,2463133441338048",
			"name": "Number of convex polygons of perimeter 2n on square lattice.",
			"comment": [
				"Or, a(n) = number of convex polyominoes of perimeter 2n. - _David Callan_, Jul 25 2008"
			],
			"reference": [
				"Boussicault, Adrien, Simone Rinaldi, and Samanta Socci. \"The number of directed k-convex polyominoes.\" arXiv preprint arXiv:1501.00872 (2015). Discrete Math., 343 (2020), #111731, 22 pages.",
				"K. Y. Lin and S. J. Chang, J. Phys. A: Math. Gen., 21 (1988), 2635-2642.",
				"N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence)."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A005436/b005436.txt\"\u003eTable of n, a(n) for n = 2..1650\u003c/a\u003e (n = 2..105 from I. Jensen)",
				"Peter Balazs, \u003ca href=\"http://dx.doi.org/10.1007/978-3-540-73040-8_35\"\u003eGeneration and Empirical Investigation of hv-Convex Discrete Sets\u003c/a\u003e, in Image Analysis, Lecture Notes in Computer Science, Volume 4522/2007, Springer-Verlag. [From _N. J. A. Sloane_, Jul 09 2009]",
				"D. Battaglino, J. M. Fedou, S. Rinaldi and S. Socci, \u003ca href=\"http://www.liafa.univ-paris-diderot.fr/fpsac13/pdfAbstracts/dmAS0203.pdf\"\u003eThe number of k-parallelogram polyominoes\u003c/a\u003e, FPSAC 2013 Paris, France DMTCS Proc. AS, 2013, 1143-1154.",
				"A. Bernini, F. Disanto, R. Pinzani and S. Rinaldi, \u003ca href=\"http://www.cs.uwaterloo.ca/journals/JIS/VOL10/Rinaldi/rinaldi5.html\"\u003ePermutations defining convex permutominoes\u003c/a\u003e, J. Int. Seq. 10 (2007) # 07.9.7.",
				"Adrien Boussicault, P. Laborde-Zubieta, \u003ca href=\"https://arxiv.org/abs/1611.03766\"\u003ePeriodic Parallelogram Polyominoes\u003c/a\u003e, arXiv preprint arXiv:1611.03766 [math.CO], 2016.",
				"Kevin Buchin, Man-Kwun Chiu, Stefan Felsner, Günter Rote, André Schulz, \u003ca href=\"https://arxiv.org/abs/1903.01095\"\u003eThe Number of Convex Polyominoes with Given Height and Width\u003c/a\u003e, arXiv:1903.01095 [math.CO], 2019.",
				"M.-P. Delest and G. Viennot, \u003ca href=\"http://dx.doi.org/10.1016/0304-3975(84)90116-6\"\u003eAlgebraic languages and polyominoes enumeration\u003c/a\u003e, Theoretical Computer Sci., 34 (1984), 169-206.",
				"F. Disanto, A. Frosini, R. Pinzani and S. Rinaldi, \u003ca href=\"http://arxiv.org/abs/math/0702550\"\u003eA closed formula for the number of convex permutominoes\u003c/a\u003e, arXiv:math/0702550 [math.CO], 2007.",
				"Filippo Disanto, Andrea Frosini, Simone Rinaldi, Renzo Pinzani, \u003ca href=\"http://www.seams-bull-math.ynu.edu.cn/downloadfile.jsp?filemenu=_200805\u0026amp;filename=The%20Combinatorics%20of%20Convex%20Permutominoes.pdf\"\u003eThe Combinatorics of Convex Permutominoes\u003c/a\u003e, Southeast Asian Bulletin of Mathematics (2008) 32: 883-912.",
				"E. Duchi, S. Rinaldi and G. Schaeffer, \u003ca href=\"http://arXiv.org/abs/math.CO/0602124\"\u003eThe number of Z-convex polyominoes\u003c/a\u003e, arXiv:math/0602124 [math.CO], 2006.",
				"I. G. Enting and A. J. Guttmann, \u003ca href=\"http://dx.doi.org/10.1088/0305-4470/22/14/013\"\u003eArea-weighted moments of convex polygons on the square lattice\u003c/a\u003e, J. Phys. A 22 (1989), 2639-2642. See Eq. (4).",
				"I. G. Enting and A. J. Guttmann, \u003ca href=\"http://dx.doi.org/10.1007/BF01112757\"\u003eOn the area of square lattice polygons\u003c/a\u003e, J. Statist. Phys., 58 (1990), 475-484. See p. 477.",
				"A. J. Guttmann and I. G. Enting, \u003ca href=\"http://dx.doi.org/10.1088/0305-4470/21/8/007\"\u003eThe number of convex polygons on the square and honeycomb lattices\u003c/a\u003e, J. Phys. A 21 (1988), L467-L474.",
				"I. Jensen, \u003ca href=\"http://www.ms.unimelb.edu.au/~iwan/polygons/Polygons_ser.html\"\u003eMore terms\u003c/a\u003e",
				"Anne Micheli and Dominique Rossin, \u003ca href=\"http://www.combinatorics.org/ojs/index.php/eljc/article/view/v20i2p56/0\"\u003eCounting k-Convex Polyominoes\u003c/a\u003e, Electron. J. Combin., Volume 20, Issue 2 (2013), #P56.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/ConvexPolyomino.html\"\u003eConvex Polyomino\u003c/a\u003e",
				"V. M. Zhuravlev, \u003ca href=\"http://www.mccme.ru/free-books/matpros/mph.pdf\"\u003eHorizontally-convex polyiamonds and their generating functions\u003c/a\u003e, Mat. Pros. 17 (2013), 107-129 (in Russian)."
			],
			"formula": [
				"a(n) = (3+2*n)*4^n/256 - (4*n-12)*C(2n-7,n-4) for n \u003e= 4.",
				"(2*n+11)*4^n-4*(2*n+1)*binomial(2*n,n) produces the terms (except the first two) with a different offset. - _N. J. A. Sloane_, Oct 14 2017",
				"G.f.: x^2*(1-6*x+11*x^2-4*x^3)/(1-4*x)^2-4*x^4*(1-4*x)^(-3/2). - Markus Voege (voege(AT)blagny.inria.fr), Nov 28 2003"
			],
			"maple": [
				"t1:=x^2*( (1-6*x+11*x^2-4*x^3)/(1-4*x)^2 - 4*x^2/(1-4*x)^(3/2));",
				"series(t1,x,40);",
				"gfun:-seriestolist(%); # _N. J. A. Sloane_, Aug 02 2015"
			],
			"mathematica": [
				"Join[{1, 2}, Table[(2 n + 11) 4^n - 4 (2 n + 1) Binomial[2 n, n], {n, 0, 25}]] (* _Vincenzo Librandi_, Jun 25 2015 *)"
			],
			"program": [
				"(MAGMA) [1,2] cat [4^n*(2*n+11)-4*(2*n+1)*Binomial(2*n,n): n in [0..25]]; // _Vincenzo Librandi_, Jun 25 2015"
			],
			"xref": [
				"a(n) = A005768(n) + A005769(n) + A005770(n).",
				"Cf. A260346."
			],
			"keyword": "nonn",
			"offset": "2,2",
			"author": "_Simon Plouffe_ and _N. J. A. Sloane_.",
			"ext": [
				"First formula corrected by _Robert Israel_, Apr 04 2016"
			],
			"references": 11,
			"revision": 74,
			"time": "2020-03-06T17:14:34-05:00",
			"created": "1991-07-11T03:00:00-04:00"
		}
	]
}