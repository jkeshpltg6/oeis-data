{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A245432",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 245432,
			"data": "1,1,-1,-2,3,4,-6,-8,11,15,-20,-26,34,44,-56,-72,91,114,-143,-178,220,272,-334,-408,498,605,-732,-884,1064,1276,-1528,-1824,2171,2580,-3058,-3616,4269,5028,-5910,-6936,8124,9498,-11088,-12922,15034,17468,-20264",
			"name": "Expansion of f(-q^3, -q^5)^2 / (psi(-q) * phi(q^2)) in powers of q where phi(), psi(), f() are Ramanujan theta functions.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700)."
			],
			"link": [
				"G. C. Greubel, \u003ca href=\"/A245432/b245432.txt\"\u003eTable of n, a(n) for n = 0..1000\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Expansion of (f(-q^3, -q^5) / f(-q^1, -q^7)) * (psi(q^4) / phi(q^2)) in powers of q where phi(), psi(), f() are Ramanujan theta functions.",
				"Euler transform of period 8 sequence [ 1, -2, -1, 4, -1, -2, 1, 0, ...].",
				"Convolution quotient of A244526 and A226192.",
				"a(n) = (-1)^floor(n/2) * A115671(n).",
				"a(n) = A224216(n) unless n=0. a(2*n+1) = A210063(n)."
			],
			"example": [
				"G.f. = 1 + q - q^2 - 2*q^3 + 3*q^4 + 4*q^5 - 6*q^6 - 8*q^7 + 11*q^8 + ..."
			],
			"mathematica": [
				"a[ n_] := SeriesCoefficient[ (QPochhammer[ q^2, q^4] / QPochhammer[ q^4, q^8]^2)^2 QPochhammer[ q^3, q^8] QPochhammer[ q^5, q^8] / (QPochhammer[ q^1, q^8] QPochhammer[ q^7, q^8]), {q, 0, n}];"
			],
			"program": [
				"(PARI) {a(n) = if( n\u003c0, 0, polcoeff( prod(k=1, n, (1 - x^k + x * O(x^n))^[0, -1, 2, 1, -4, 1, 2, -1][k%8 + 1]), n))};",
				"(PARI) {a(n) = local(A); if( n\u003c0, 0, A = x * O(x^n); (-1)^(n \\ 2) * polcoeff( (1 + eta(x^2 + A)^3 / (eta(x + A)^2 * eta(x^4 + A))) / 2, n))};"
			],
			"xref": [
				"Cf. A115671, A210063, A226192, A224216, A244526."
			],
			"keyword": "sign",
			"offset": "0,4",
			"author": "_Michael Somos_, Jul 21 2014",
			"references": 2,
			"revision": 10,
			"time": "2021-03-12T22:24:47-05:00",
			"created": "2014-07-21T17:12:25-04:00"
		}
	]
}