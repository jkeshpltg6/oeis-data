{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A006463",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 6463,
			"data": "0,0,1,2,4,6,8,11,14,17,20,24,28,32,36,40,45,50,55,60,65,70,76,82,88,94,100,106,112,119,126,133,140,147,154,161,168,176,184,192,200,208,216,224,232,240,249,258,267,276,285,294,303,312,321,330,340,350,360",
			"name": "Convolve natural numbers with characteristic function of triangular numbers.",
			"comment": [
				"Ramanujan theta functions: f(q) (see A121373), phi(q) (A000122), psi(q) (A010054), chi(q) (A000700).",
				"a(n) = length (i.e., number of elements minus 1) of longest chain in partition lattice Par(n). Par(n) is the set of partitions of n under \"dominance order\": partition P is \u003c= partition Q iff the sum of the largest k parts of P is \u003c= the corresponding sum for Q for all k.",
				"If C_n(q, t) are the (q, t)-Catalan polynomials, then p_n(x) := C_n(x, x) is a polynomial in x such that a(n) is the degree of the lowest degree term. The sequence of polynomials p_n(x) = 1, 1, 2*x, x^2 + 4*x^3, 3*x^4 + 4*x^5 + 7*x^6 + ... while the coefficient of the lowest degree term is A074909(n). - _Michael Somos_, Jan 09 2019"
			],
			"reference": [
				"R. P. Stanley, Enumerative Combinatorics, Cambridge, Vol. 2, 1999; see Problem 7.2(f)."
			],
			"link": [
				"Reinhard Zumkeller, \u003ca href=\"/A006463/b006463.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"Jeffrey Shallit, \u003ca href=\"/A006463/a006463.pdf\"\u003eLetter to N. J. A. Sloane with attachment, Aug. 1979\u003c/a\u003e",
				"Michael Somos, \u003ca href=\"/A010815/a010815.txt\"\u003eIntroduction to Ramanujan theta functions\u003c/a\u003e",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/RamanujanThetaFunctions.html\"\u003eRamanujan Theta Functions\u003c/a\u003e"
			],
			"formula": [
				"Let n=binomial(m+1, 2)+r, 0\u003c=r\u003c=m; then a(n) = (1/3)*m*(m^2+3*r-1).",
				"G.f.: (psi(x) - 1) * x / (1 - x)^2 where psi() is a Ramanujan theta function. - _Michael Somos_, Mar 06 2006",
				"a(n) = sum_(k=0..n-1) A003056(k). - Daniele Parisse (daniele.parisse(AT)eads.com), Jul 10 2007",
				"a(n+1) - 2*a(n) + a(n-1) = A010054(n) if n\u003e0. - _Michael Somos_, May 07 2016"
			],
			"example": [
				"a(6)=8; one longest chain consists of these 9 partitions: 6, 5+1, 4+2, 3+3, 3+2+1, 2+2+2, 2+2+1+1, 2+1+1+1+1, 1+1+1+1+1+1. Others are obtained by changing 3+3 to 4+1+1 or 2+2+2 to 3+1+1+1.",
				"G.f. = x^2 + 2*x^3 + 4*x^4 + 6*x^5 + 8*x^6 + 11*x^7 + 14*x^8 + 17*x^9 + ..."
			],
			"mathematica": [
				"a[n_] := (x = Quotient[ Sqrt[1+8*n]-1, 2]; x*(x^2-1+3*(n-x*(x+1)/2))/3); Table[a[n], {n, 0, 58}] (* _Jean-François Alcover_, Apr 11 2013, after _Michael Somos_ *)",
				"t = {0}; Do[Do[AppendTo[t, t[[-1]]+n], {k, 0, n}], {n, 0, 11}]; t (* _Jean-François Alcover_, May 10 2016, after _Vladimir Joseph Stephan Orlovsky_ *)",
				"Join[{0},Table[ListConvolve[Range[x],Table[If[OddQ[Sqrt[8n+1]],1,0],{n,x}]],{x,0,60}]//Flatten] (* _Harvey P. Dale_, Jan 14 2019 *)"
			],
			"program": [
				"(PARI) {a(n) = my(x); if( n\u003c0, 0, x = (sqrtint(8*n + 1) - 1)\\2; x * (x^2 - 1 + 3 * (n - x*(x+1)/2)) / 3)}; /* _Michael Somos_, Mar 06 2006 */",
				"(Haskell)",
				"a006463 n = a006463_list !! n",
				"a006463_list = 0 : scanl1 (+) a003056_list",
				"-- _Reinhard Zumkeller_, Dec 17 2011"
			],
			"xref": [
				"Cf. A076269, A074909, A010054, A060432.",
				"0 together with the partial sums of A003056."
			],
			"keyword": "nonn,easy,nice",
			"offset": "0,4",
			"author": "_N. J. A. Sloane_",
			"ext": [
				"Edited by _Dean Hickerson_, Nov 09 2002"
			],
			"references": 12,
			"revision": 57,
			"time": "2021-09-11T22:58:35-04:00",
			"created": "1999-12-11T03:00:00-05:00"
		}
	]
}