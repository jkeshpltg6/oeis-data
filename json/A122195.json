{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A122195",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 122195,
			"data": "8,11,13,14,18,19,22,23,30,31,36,38,49,51,59,62,80,83,96,101,130,135,156,164,211,219,253,266,342,355,410,431,554,575,664,698,897,931,1075,1130,1452,1507,1740,1829,2350,2439,2816,2960,3803,3947,4557,4790,6154",
			"name": "Numbers that are the sum of exactly 3 sets of Fibonacci numbers.",
			"link": [
				"G. C. Greubel, \u003ca href=\"/A122195/b122195.txt\"\u003eTable of n, a(n) for n = 1..1000\u003c/a\u003e",
				"M. Bicknell-Johnson \u0026 D. C. Fielder, \u003ca href=\"http://www.fq.math.ca/Scanned/37-1/bicknell.pdf\"\u003eThe number of Representations of N Using Distinct Fibonacci Numbers, Counted by Recursive Formulas\u003c/a\u003e, Fibonacci Quart. 37.1 (1999) pp. 47 ff.",
				"Ron Knott, \u003ca href=\"http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Fibonacci/fibrep.html#sumoffib\"\u003eSumthing about Fibonacci Numbers\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_09\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (1,0,0,1,-1,0,0,1,-1)."
			],
			"formula": [
				"G.f.: (8+3*x+2*x^2+x^3-4*x^4-2*x^5+x^6-5*x^8-3*x^9)/(1-x-x^4+x^5-x^8+x^9).",
				"a(n) = a(n-4) + a(n-8) + 1.",
				"a(0)=8, a(1)=11, a(2)=13, a(3)=18, then: a(4n) = A022318(n+3) = 2*A000045(n+5) + A000045(n+3) - 1, a(4n+1) = A022406(n+2) = 4*A000045(n+4) - 1, a(4n+2) = A022308(n+4) = 2*A000045(n+4) + A000045(n+6) - 1, a(4n+3) = 3*A000045(n+4) - 1, for n\u003e=1.",
				"a(n) = a(n-1) +a(n-4) -a(n-5) +a(n-8) -a(n-9). - _G. C. Greubel_, Jul 13 2019"
			],
			"example": [
				"8 is the sum of only 3 sets of Fibonacci numbers: {8}, {3,5} and {1,2,5};",
				"11 is the sum of only {3,8}, {1,2,8}, {1,2,3,5}."
			],
			"maple": [
				"# first N terms:",
				"series((8+3*x+2*x^2+x^3-4*x^4-2*x^5+x^6-5*x^8-3*x^9)/(x^9-x^8+x^5-x^4-x+1),x,N+1);"
			],
			"mathematica": [
				"CoefficientList[Series[(8+3*x+2*x^2+x^3-4*x^4-2*x^5+x^6-5*x^8-3*x^9)/(1 - x-x^4+x^5-x^8+x^9), {x, 0, 60}], x] (* _G. C. Greubel_, Jul 13 2019 *)"
			],
			"program": [
				"(PARI) my(x='x+O('x^60)); Vec((8+3*x+2*x^2+x^3-4*x^4-2*x^5+x^6-5*x^8 -3*x^9)/(1-x-x^4+x^5-x^8+x^9)) \\\\ _G. C. Greubel_, Jul 13 2019",
				"(MAGMA) R\u003cx\u003e:=PowerSeriesRing(Integers(), 60); Coefficients(R!( (8+3*x+2*x^2+x^3-4*x^4-2*x^5+x^6-5*x^8-3*x^9)/(1 - x-x^4+x^5-x^8+x^9) )); // _G. C. Greubel_, Jul 13 2019",
				"(Sage) ((8+3*x+2*x^2+x^3-4*x^4-2*x^5+x^6-5*x^8-3*x^9)/(1 - x-x^4+x^5-x^8+x^9)).series(x, 60).coefficients(x, sparse=False) # _G. C. Greubel_, Jul 13 2019",
				"(GAP) a:=[11,13,14,18,19,22,23,30];; for n in [9..60] do a[n]:=a[n-4]+a[n-8]+1; od; Concatenation([8], a); # _G. C. Greubel_, Jul 13 2019"
			],
			"xref": [
				"Cf. A000045, A000071, A013583, A000119, A122194."
			],
			"keyword": "nonn",
			"offset": "1,1",
			"author": "_Ron Knott_, Aug 25 2006, corrected Aug 29 2006",
			"references": 5,
			"revision": 21,
			"time": "2019-07-18T08:12:27-04:00",
			"created": "2006-09-29T03:00:00-04:00"
		}
	]
}