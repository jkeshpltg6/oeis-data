{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A076839",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 76839,
			"data": "1,1,2,3,2,1,1,2,3,2,1,1,2,3,2,1,1,2,3,2,1,1,2,3,2,1,1,2,3,2,1,1,2,3,2,1,1,2,3,2,1,1,2,3,2,1,1,2,3,2,1,1,2,3,2,1,1,2,3,2,1,1,2,3,2,1,1,2,3,2,1,1,2,3,2,1,1,2,3,2,1,1,2,3,2,1,1,2,3,2,1,1,2,3,2,1,1,2,3,2,1,1,2,3,2",
			"name": "A simple example of the Lyness 5-cycle: a(1) = a(2) = 1; a(n) = (a(n-1)+1)/a(n-2) (for n\u003e2).",
			"comment": [
				"Any sequence a(1),a(2),a(3),... defined by the recurrence a(n) = (a(n-1)+1)/a(n-2) (for n\u003e2) has period 5. The theory of cluster algebras currently being developed by Fomin and Zelevinsky gives a context for these facts, but it doesn't really explain them in an elementary way. - _James Propp_, Nov 20 2002",
				"Terms of the simple continued fraction of 34/[sqrt(2405)-29]. Decimal expansion of 1248/11111. - _Paolo P. Lava_, Aug 05 2009"
			],
			"reference": [
				"J. H. Conway and R. L. Graham, On Periodic Sequences Defined by Recurrences, unpublished, date?",
				"Martin Gardner, The Magic Numbers of Dr Matrix, Prometheus Books, 1985, pages 198 and 305."
			],
			"link": [
				"Sergey Fomin and Andrei Zelevinsky, \u003ca href=\"https://arxiv.org/abs/math/0208229\"\u003eCluster algebras II: Finite type classification\u003c/a\u003e, arXiv:math/0208229 [math.RA], 2002-2003.",
				"Jonny Griffiths, \u003ca href=\"http://www.s253053503.websitehome.co.uk/jg-msc-uea/thesis-final-11-2-2012.pdf\"\u003eLyness cycles, elliptic curves, and Hikorsky triangles\u003c/a\u003e, MSc Thesis, University of East Anglia, Norwich, UK, Department of Mathematics, Feb 2012.",
				"V. L. Kocic, G. Ladas, and I. W. Rodrigues, \u003ca href=\"https://doi.org/10.1006/jmaa.1993.1057\"\u003eOn Rational Recursive Sequences\u003c/a\u003e, J. Math. Anal. Appl., 173 (1993), 127-157.",
				"R. C. Lyness, \u003ca href=\"https://www.jstor.org/stable/3606036\"\u003eNote 1581. Cycles\u003c/a\u003e, Math. Gazette, 26 (1942), 62.",
				"R. C. Lyness, \u003ca href=\"https://www.jstor.org/stable/3609268\"\u003eNote 1847. Cycles\u003c/a\u003e, Math. Gaz., 29 (1945), 231-233.",
				"R. C. Lyness, \u003ca href=\"https://www.jstor.org/stable/3612778\"\u003eNote 2952. Cycles\u003c/a\u003e, Math. Gaz., 45 (1961), 207-209.",
				"S. Morier-Genoud, V. Ovsienko and S. Tabachnikov, \u003ca href=\"http://arxiv.org/abs/1008.3359\"\u003e2-frieze patterns and the cluster structure of the space of polygons\u003c/a\u003e, arXiv:1008.3359 [math.AG], 2010-2011.",
				"S. Morier-Genoud, V. Ovsienko and S. Tabachnikov, \u003ca href=\"https://doi.org/10.5802/aif.2713\"\u003e2-frieze patterns and the cluster structure of the space of polygons\u003c/a\u003e, Annales de l'institut Fourier, 62 no. 3 (2012), 937-987.",
				"\u003ca href=\"/index/Rec#order_05\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (0,0,0,0,1)."
			],
			"formula": [
				"Periodic with period 5.",
				"a(1)=1, a(2)=1, a(3)=2, a(4)=3, a(5)=2, a(n)=a(n-5). - _Harvey P. Dale_, Jan 17 2013"
			],
			"maple": [
				"a := 1; b := 1; f := proc(n) option remember; global a,b; if n=1 then a elif n=2 then b else (f(n-1)+1)/f(n-2); fi; end;"
			],
			"mathematica": [
				"RecurrenceTable[{a[1]==a[2]==1,a[n]==(a[n-1]+1)/a[n-2]},a,{n,110}] (* or *) LinearRecurrence[{0,0,0,0,1},{1,1,2,3,2},110] (* _Harvey P. Dale_, Jan 17 2013 *)"
			],
			"xref": [
				"Cf. A076840, A076841, A076844, A076823.",
				"See A335688/A335689 for a very similar nonperiodic sequence."
			],
			"keyword": "nonn,easy",
			"offset": "1,3",
			"author": "_N. J. A. Sloane_, Nov 21 2002",
			"ext": [
				"Thanks to _Michael Somos_ for pointing out the Kocic et al. (1973) reference. Also I deleted some useless comments. - _N. J. A. Sloane_, Jul 19 2020"
			],
			"references": 10,
			"revision": 79,
			"time": "2020-09-27T14:48:52-04:00",
			"created": "2003-05-16T03:00:00-04:00"
		}
	]
}