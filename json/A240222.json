{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A240222",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 240222,
			"data": "1,3,1,5,9,1,7,17,33,1,9,25,65,129,1,11,33,97,257,513,1,13,41,129,385,1025,2049,1,15,49,161,513,1537,4097,8193,1,17,57,193,641,2049,6145,16385,32769,1,19,65,225,769,2561,8193,24577,65537,131073,1,21,73,257,897,3073,10241,32769,98305",
			"name": "Rectangular array giving all start values M(n, k), k \u003e= 1, for Collatz sequences following the pattern (udd)^(n-1) ud, n \u003e= 1, read by antidiagonals.",
			"comment": [
				"The companion array and triangle for the end numbers N(n, k) is given in A240223.",
				"The two operations on natural numbers m used in the Collatz 3x+1 conjecture are here (following the M. Trümper paper given in the link) denoted by u for 'up' and d for 'down': u m = 3*m+1, if m is odd, and d m = m/2 if m is even. The present array gives all start numbers M(n, k) for Collatz sequences realizing the Collatz word (udd)^n ud = (sd)^n s (s = ud is useful because, except for the one letter word u, at least one d follows a letter u), with n \u003e= 1, and k \u003e= 1. The length of these Collatz sequences 3*n. For these Collatz sequences M(n, 0) = M(1, 0) = 1 and N(n, 0) = N(1, 0) = 2."
			],
			"link": [
				"Wolfdieter Lang, \u003ca href=\"/A240222/a240222.pdf\"\u003eRectangular array and triangle.\u003c/a\u003e",
				"Wolfdieter Lang, \u003ca href=\"https://cs.uwaterloo.ca/journals/JIS/VOL17/Lang/lang6.html\"\u003eOn Collatz' Words, Sequences and Trees\u003c/a\u003e, J. of Integer Sequences, Vol. 17 (2014), Article 14.11.7.",
				"Manfred Trümper, \u003ca href=\"http://dx.doi.org/10.1155/2014/756917\"\u003eThe Collatz Problem in the Light of an Infinite Free Semigroup\u003c/a\u003e, Chinese Journal of Mathematics, Vol. 2014, Article ID 756917, 21 pages.",
				"Eric Weisstein's World of Mathematics, \u003ca href=\"http://mathworld.wolfram.com/CollatzProblem.html\"\u003eCollatz Problem\u003c/a\u003e.",
				"Wikipedia, \u003ca href=\"https://en.wikipedia.org/wiki/Collatz_conjecture\"\u003eCollatz Conjecture\u003c/a\u003e"
			],
			"formula": [
				"The array: M(n, k) = 1 + 2^(2*n-1)*k for n \u003e= 1 and k \u003e= 0.",
				"The triangle: TM(m, n) = M(n,m-n+1) = 1 + 2^(2*n-1)*(m-n+1) for m+1 \u003e= n \u003e= 1 and 0 for m+1 \u003c n."
			],
			"example": [
				"The rectangular array M(n, k) begins:",
				"n\\k 0       1       2       3       4       5 ...",
				"1:  1       3       5       7       9      11",
				"2:  1       9      17      25      33      41",
				"3:  1      33      65      97     129     161",
				"4:  1     129     257     385     513     641",
				"5:  1     513    1025    1537    2049    2561",
				"6:  1    2049    4097    6145    8193   10241",
				"7:  1    8193   16385   24577   32769   40961",
				"8:  1   32769   65537   98305  131073  163841",
				"9:  1  131073  262145  393217  524289  655361",
				"10: 1  524289 1048577 1572865 2097153 2621441",
				"...",
				"For more columns see the link.",
				"The triangle TM(m, n) begins (zeros are not shown):",
				"k\\n  1  2   3   4    5     6      7 ...",
				"0:   1",
				"1:   3  1",
				"2:   5  9   1",
				"3:   7 17  33   1",
				"4:   9 25  65 129    1",
				"5:  11 33  97 257  513     1",
				"6:  13 41 129 385 1025  2049      1",
				"...",
				"For more rows see the link.",
				"n=1, ud, k=0: M(1, 0) = 1 = TM(0, 1), N(1, 0) = 2 with the Collatz sequence [1, 4, 2] of",
				"length 3.",
				"n=1, ud, k=2: M(1, 2) = 5 = TM(2, 1), N(1,  2) = 8 with the Collatz sequence [5, 16, 8] of length 3.",
				"n=2, uddud, k=0: M(2, 0) = 1 = TM(1, 2), Ne(2, 0) = 2 with the Collatz sequence [1, 4, 2, 1, 4, 2, 1, 4, 2] of length 9."
			],
			"xref": [
				"Cf. A238475, A238476, A239126, A239127."
			],
			"keyword": "nonn,easy,tabl",
			"offset": "1,2",
			"author": "_Wolfdieter Lang_, Apr 02 2014",
			"references": 2,
			"revision": 27,
			"time": "2018-09-01T21:44:51-04:00",
			"created": "2014-04-04T04:56:56-04:00"
		}
	]
}