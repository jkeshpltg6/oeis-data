{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A261032",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 261032,
			"data": "0,-1,255,-6306,59230,-331395,1348221,-4416580,12360636,-30686085,69313915,-145044966,284936730,-530793991,944995065,-1617895560,2677071736,-4298685705,6721274871,-10262288170,15337711830,-22485147531,32390726005,-45920259276,64155054900,-88432835725",
			"name": "a(n) = (-1)^n*(n^8 + 4*n^7 - 14*n^5 + 28*n^3 - 17*n)/2.",
			"comment": [
				"Alternating sum of eighth powers (A001016).",
				"For n\u003e0, a(n) is divisible by A000217(n)."
			],
			"link": [
				"Robert Israel, \u003ca href=\"/A261032/b261032.txt\"\u003eTable of n, a(n) for n = 0..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Rec#order_09\"\u003eIndex entries for linear recurrences with constant coefficients\u003c/a\u003e, signature (-9,-36,-84,-126,-126,-84,-36,-9,-1 )."
			],
			"formula": [
				"G.f.: -x*(1 - 246*x + 4047*x^2 - 11572*x^3 + 4047*x^4 - 246*x^5 + x^6)/(1 + x)^9.",
				"a(n) = Sum_{k = 0..n} (-1)^k*k^8.",
				"a(n) = (-1)^n*n*(n + 1)*(n^6 + 3*n^5 - 3*n^4 - 11*n^3 + 11*n^2 + 17*n - 17)/2.",
				"Sum_{n\u003e0} 1/a(n) = -0.9962225712723456482...",
				"Sum_{j=0..9} binomial(9,j)*a(n-j) = 0. - _Robert Israel_, Nov 18 2015",
				"E.g.f.: (x/2)*(-2 +253*x -1848*x^2 +2961*x^3 -1596*x^4 +350*x^5 -32*x^6 +x^7)*exp(-x). - _G. C. Greubel_, Apr 02 2021"
			],
			"example": [
				"a(0) = 0^8 = 0,",
				"a(1) = 0^8 -1^8 = -1,",
				"a(2) = 0^8 -1^8 + 2^8 = 255,",
				"a(3) = 0^8 -1^8 + 2^8 - 3^8 = -6306,",
				"a(4) = 0^8 -1^8 + 2^8 - 3^8 + 4^8 = 59230,",
				"a(5) = 0^8 -1^8 + 2^8 - 3^8 + 4^8 - 5^8 = -331395, etc."
			],
			"maple": [
				"seq((-1)^n*(n^8 + 4*n^7 - 14*n^5 + 28*n^3 - 17*n)/2, n = 0 .. 100); # _Robert Israel_, Nov 18 2015"
			],
			"mathematica": [
				"Table[(1/2) (-1)^n n (n + 1) (n^6 + 3 n^5 - 3 n^4 - 11 n^3 + 11 n^2 + 17 n - 17), {n, 0, 25}]"
			],
			"program": [
				"(PARI) vector(100, n, n--; (-1)^n*(n^8+4*n^7-14*n^5+28*n^3-17*n)/2) \\\\ _Altug Alkan_, Nov 18 2015",
				"(MAGMA) [(-1)^n*(n^8+4*n^7-14*n^5+28*n^3-17*n)/2: n in [0..30]]; // _Vincenzo Librandi_, Nov 20 2015",
				"(Sage) [(-1)^n*(n^8 +4*n^7 -14*n^5 +28*n^3 -17*n)/2 for n in (0..40)] # _G. C. Greubel_, Apr 02 2021"
			],
			"xref": [
				"Cf. A000217, A001016, A000542, A089594, A232599, A062392, A062393, A152725, A152726."
			],
			"keyword": "sign,easy",
			"offset": "0,3",
			"author": "_Ilya Gutkovskiy_, Nov 18 2015",
			"references": 1,
			"revision": 33,
			"time": "2021-04-02T21:21:45-04:00",
			"created": "2015-11-20T05:00:48-05:00"
		}
	]
}