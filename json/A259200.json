{
	"greeting": "Greetings from The On-Line Encyclopedia of Integer Sequences! http://oeis.org/",
	"query": "id:A259200",
	"count": 1,
	"start": 0,
	"results": [
		{
			"number": 259200,
			"data": "1,1,1,2,2,3,4,4,5,7,7,9,10,11,12,16,16,20,21,24,26,33,31,39,39,47,46,59,53,69,65,80,77,98,85,114,104,131,118,154,133,179,155,200,177,236,196,268,227,300,256",
			"name": "Number of partitions of n into nine primes.",
			"link": [
				"Robert Israel, \u003ca href=\"/A259200/b259200.txt\"\u003eTable of n, a(n) for n = 18..10000\u003c/a\u003e",
				"\u003ca href=\"/index/Par#part\"\u003eIndex entries for sequences related to partitions\u003c/a\u003e"
			],
			"formula": [
				"a(n) = [x^n y^9] Product_{k\u003e=1} 1/(1 - y*x^prime(k)). - _Ilya Gutkovskiy_, Apr 18 2019",
				"a(n) = Sum_{q=1..floor(n/9)} Sum_{p=q..floor((n-q)/8)} Sum_{o=p..floor((n-p-q)/7)} Sum_{m=o..floor((n-o-p-q)/6)} Sum_{l=m..floor((n-m-o-p-q)/5)} Sum_{k=l..floor((n-l-m-o-p-q)/4)} Sum_{j=k..floor((n-k-l-m-o-p-q)/3)} Sum_{i=j..floor((n-j-k-l-m-o-p-q)/2)} A010051(q) * A010051(p) * A010051(o) * A010051(m) * A010051(l) * A010051(k) * A010051(j) * A010051(i) * A010051(n-i-j-k-l-m-o-p-q). - _Wesley Ivan Hurt_, Jul 13 2019"
			],
			"example": [
				"a(23) = 3 because there are 3 partitions of 23 into nine primes: [2,2,2,2,2,2,2,2,7], [2,2,2,2,2,2,3,3,5] and [2,2,2,2,3,3,3,3,3]."
			],
			"maple": [
				"N:= 100: # to get a(0) to a(N)",
				"Primes:= select(isprime,[$1..N]):",
				"np:= nops(Primes):",
				"for j from 0 to np do g[0,j]:= 1 od:",
				"for n from 1 to 9 do",
				"  g[n,0]:= 0:",
				"  for j from 1 to np do",
				"     g[n,j]:= convert(series(add(g[k,j-1]",
				"          *x^((n-k)*Primes[j]),k=0..n),x,N+1),polynom)",
				"  od",
				"od:",
				"seq(coeff(g[9,np],x,i),i=18..N) # _Robert Israel_, Jun 21 2015"
			],
			"mathematica": [
				"Table[Length[Select[IntegerPartitions[n],Length[#]==9\u0026\u0026AllTrue[ #, PrimeQ]\u0026]], {n,18,70}] (* The program uses the AllTrue function from Mathematica version 10 *) (* _Harvey P. Dale_, Jul 31 2016 *)"
			],
			"program": [
				"(PARI) a(n) = {nb = 0; forpart(p=n, if (#p \u0026\u0026 (#select(x-\u003eisprime(x), Vec(p)) == #p), nb+=1), , [9,9]); nb;} \\\\ _Michel Marcus_, Jun 21 2015",
				"(MAGMA) [#RestrictedPartitions(k,9,Set(PrimesUpTo(1000))):k in [18..70]] ; // _Marius A. Burtea_, Jul 13 2019"
			],
			"xref": [
				"Column k=9 of A117278.",
				"Number of partitions of n into r primes for r = 1..10: A010051, A061358, A068307, A259194, A259195, A259196, A259197, A259198, this sequence, A259201.",
				"Cf. A000040."
			],
			"keyword": "nonn,easy",
			"offset": "18,4",
			"author": "_Doug Bell_, Jun 20 2015",
			"references": 22,
			"revision": 45,
			"time": "2019-10-29T21:03:46-04:00",
			"created": "2015-06-22T12:40:51-04:00"
		}
	]
}