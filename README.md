OEIS Data
=========

Copy of [OEIS](https://oeis.org/).  Fetched with a crawler.  Because I
didn't find a way to download all the sequences.

Source URL is like `https://oeis.org/search?q=id:A000001&fmt=json`.

The OEIS is made available under the Creative Commons Attribution
Non-Commercial 3.0 license.
